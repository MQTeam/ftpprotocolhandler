package com.atomiton.sff.imp.ftp.parser;

import java.text.ParseException;
import java.util.List;
import java.util.ListIterator;

import com.atomiton.sff.imp.ftp.FTPClientConfig;
import com.atomiton.sff.imp.ftp.FTPFile;

public class UnixFTPEntryParser
extends
    ConfigurableFTPFileEntryParserImpl
{
static final String DEFAULT_DATE_FORMAT = "MMM d yyyy";

static final String DEFAULT_RECENT_DATE_FORMAT = "MMM d HH:mm";

static final String NUMERIC_DATE_FORMAT = "yyyy-MM-dd HH:mm";

private static final String JA_MONTH = "\u6708";
private static final String JA_DAY = "\u65e5";
private static final String JA_YEAR = "\u5e74";

private static final String DEFAULT_DATE_FORMAT_JA = "M'" + JA_MONTH + "' d'" + JA_DAY + "' yyyy'" + JA_YEAR + "'";

private static final String DEFAULT_RECENT_DATE_FORMAT_JA = "M'" + JA_MONTH + "' d'" + JA_DAY + "' HH:mm";
public static final FTPClientConfig NUMERIC_DATE_CONFIG = new FTPClientConfig(
                                                                              FTPClientConfig.SYST_UNIX,
                                                                              NUMERIC_DATE_FORMAT,
                                                                              null);
private static final String REGEX = "([bcdelfmpSs-])"
                                    + "(((r|-)(w|-)([xsStTL-]))((r|-)(w|-)([xsStTL-]))((r|-)(w|-)([xsStTL-])))\\+?"

                                    + "\\s*"

                                    + "(\\d+)"

                                    + "\\s+"

                                    + "(?:(\\S+(?:\\s\\S+)*?)\\s+)?"
                                    + "(?:(\\S+(?:\\s\\S+)*)\\s+)?"
                                    + "(\\d+(?:,\\s*\\d+)?)"

                                    + "\\s+"

                                    + "(" +
                                    "(?:\\d+[-/]\\d+[-/]\\d+)" +
                                    "|(?:\\S{3}\\s+\\d{1,2})" +
                                    "|(?:\\d{1,2}\\s+\\S{3})" +
                                    "|(?:\\d{1,2}" + JA_MONTH + "\\s+\\d{1,2}" + JA_DAY + ")" +
                                    ")"

                                    + "\\s+"

                                    + "((?:\\d+(?::\\d+)?)|(?:\\d{4}" + JA_YEAR + "))"

                                    + "\\s"

                                    + "(.*)";
final boolean trimLeadingSpaces;
public UnixFTPEntryParser()
{
    this(null);
}
public UnixFTPEntryParser(FTPClientConfig config)
{
    this(config, false);
}
public UnixFTPEntryParser(FTPClientConfig config, boolean trimLeadingSpaces)
{
    super(REGEX);
    configure(config);
    this.trimLeadingSpaces = trimLeadingSpaces;
}
@Override
public List<String>
    preParse(List<String> original)
{
    ListIterator<String> iter = original.listIterator();
    while(iter.hasNext())
    {
        String entry = iter.next();
        if(entry.matches("^total \\d+$"))
        {
            iter.remove();
        }
    }
    return original;
}
@Override
public FTPFile
    parseFTPEntry(String entry)
{
    FTPFile file = new FTPFile();
    file.setRawListing(entry);
    int type;
    boolean isDevice = false;

    if(matches(entry))
    {
        String typeStr = group(1);
        String hardLinkCount = group(15);
        String usr = group(16);
        String grp = group(17);
        String filesize = group(18);
        String datestr = group(19) + " " + group(20);
        String name = group(21);
        if(trimLeadingSpaces)
        {
            name = name.replaceFirst("^\\s+", "");
        }

        try
        {
            if(group(19).contains(JA_MONTH))
            {
                FTPTimestampParserImpl jaParser = new FTPTimestampParserImpl();
                jaParser.configure(new FTPClientConfig(
                                                       FTPClientConfig.SYST_UNIX, DEFAULT_DATE_FORMAT_JA, DEFAULT_RECENT_DATE_FORMAT_JA));
                file.setTimestamp(jaParser.parseTimestamp(datestr));
            }
            else
            {
                file.setTimestamp(super.parseTimestamp(datestr));
            }
        }
        catch(ParseException e)
        {
        }
        switch(typeStr.charAt(0))
        {
        case 'd':
            type = FTPFile.DIRECTORY_TYPE;
            break;
        case 'e':
            type = FTPFile.SYMBOLIC_LINK_TYPE;
            break;
        case 'l':
            type = FTPFile.SYMBOLIC_LINK_TYPE;
            break;
        case 'b':
        case 'c':
            isDevice = true;
            type = FTPFile.FILE_TYPE;
            break;
        case 'f':
        case '-':
            type = FTPFile.FILE_TYPE;
            break;
        default:
            type = FTPFile.UNKNOWN_TYPE;
        }

        file.setType(type);

        int g = 4;
        for(int access = 0; access < 3; access++, g += 4)
        {
            file.setPermission(access, FTPFile.READ_PERMISSION,
                               (!group(g).equals("-")));
            file.setPermission(access, FTPFile.WRITE_PERMISSION,
                               (!group(g + 1).equals("-")));

            String execPerm = group(g + 2);
            if(!execPerm.equals("-") && !Character.isUpperCase(execPerm.charAt(0)))
            {
                file.setPermission(access, FTPFile.EXECUTE_PERMISSION, true);
            }
            else
            {
                file.setPermission(access, FTPFile.EXECUTE_PERMISSION, false);
            }
        }

        if(!isDevice)
        {
            try
            {
                file.setHardLinkCount(Integer.parseInt(hardLinkCount));
            }
            catch(NumberFormatException e)
            {
            }
        }

        file.setUser(usr);
        file.setGroup(grp);

        try
        {
            file.setSize(Long.parseLong(filesize));
        }
        catch(NumberFormatException e)
        {
        }

        if(type == FTPFile.SYMBOLIC_LINK_TYPE)
        {

            int end = name.indexOf(" -> ");
            if(end == -1)
            {
                file.setName(name);
            }
            else
            {
                file.setName(name.substring(0, end));
                file.setLink(name.substring(end + 4));
            }

        }
        else
        {
            file.setName(name);
        }
        return file;
    }
    return null;
}
@Override
protected FTPClientConfig
    getDefaultConfiguration()
{
    return new FTPClientConfig(
                               FTPClientConfig.SYST_UNIX,
                               DEFAULT_DATE_FORMAT,
                               DEFAULT_RECENT_DATE_FORMAT);
}
}
