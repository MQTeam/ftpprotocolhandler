package com.atomiton.sff.imp.tftp;

import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import com.atomiton.sff.imp.ftp.io.FromNetASCIIOutputStream;
import com.atomiton.sff.imp.ftp.io.ToNetASCIIInputStream;

public class TFTPClient
extends
    TFTP
{
public static final int DEFAULT_MAX_TIMEOUTS = 5;
private int __maxTimeouts;
private long totalBytesReceived = 0;
private long totalBytesSent = 0;
public TFTPClient()
{
    __maxTimeouts = DEFAULT_MAX_TIMEOUTS;
}
public void
    setMaxTimeouts(int numTimeouts)
{
    if(numTimeouts < 1)
    {
        __maxTimeouts = 1;
    }
    else
    {
        __maxTimeouts = numTimeouts;
    }
}
public int
    getMaxTimeouts()
{
    return __maxTimeouts;
}
public long
    getTotalBytesReceived()
{
    return totalBytesReceived;
}
public long
    getTotalBytesSent()
{
    return totalBytesSent;
}
public int
    receiveFile(String filename, int mode, OutputStream output,
                InetAddress host, int port)
    throws IOException
{
    int bytesRead = 0;
    int lastBlock = 0;
    int block = 1;
    int hostPort = 0;
    int dataLength = 0;

    totalBytesReceived = 0;

    if(mode == TFTP.ASCII_MODE)
    {
        output = new FromNetASCIIOutputStream(output);
    }

    TFTPPacket sent = new TFTPReadRequestPacket(host, port, filename, mode);
    TFTPAckPacket ack = new TFTPAckPacket(host, port, 0);

    beginBufferedOps();

    boolean justStarted = true;
    try
    {
        do
        {
            bufferedSend(sent);
            boolean wantReply = true;
            int timeouts = 0;
            do
            {
                try
                {
                    TFTPPacket received = bufferedReceive();
                    final int recdPort = received.getPort();
                    final InetAddress recdAddress = received.getAddress();
                    if(justStarted)
                    {
                        justStarted = false;
                        if(recdPort == port)
                        {
                            TFTPErrorPacket error = new TFTPErrorPacket(recdAddress,
                                                                        recdPort, TFTPErrorPacket.UNKNOWN_TID,
                                                                        "INCORRECT SOURCE PORT");
                            bufferedSend(error);
                            throw new IOException("Incorrect source port (" + recdPort + ") in request reply.");
                        }
                        hostPort = recdPort;
                        ack.setPort(hostPort);
                        if(!host.equals(recdAddress))
                        {
                            host = recdAddress;
                            ack.setAddress(host);
                            sent.setAddress(host);
                        }
                    }
                    if(host.equals(recdAddress) && recdPort == hostPort)
                    {
                        switch(received.getType())
                        {

                        case TFTPPacket.ERROR:
                            TFTPErrorPacket error = (TFTPErrorPacket)received;
                            throw new IOException("Error code " + error.getError() +
                                                  " received: " + error.getMessage());
                        case TFTPPacket.DATA:
                            TFTPDataPacket data = (TFTPDataPacket)received;
                            dataLength = data.getDataLength();
                            lastBlock = data.getBlockNumber();

                            if(lastBlock == block)
                            {
                                try
                                {
                                    output.write(data.getData(), data.getDataOffset(), dataLength);
                                }
                                catch(IOException e)
                                {
                                    error = new TFTPErrorPacket(host, hostPort,
                                                                TFTPErrorPacket.OUT_OF_SPACE,
                                                                "File write failed.");
                                    bufferedSend(error);
                                    throw e;
                                }
                                ++block;
                                if(block > 65535)
                                {
                                    block = 0;
                                }
                                wantReply = false;
                            }
                            else
                            {
                                discardPackets();
                                if(lastBlock == (block == 0 ? 65535 : (block - 1)))
                                {
                                    wantReply = false;
                                }
                            }
                            break;

                        default:
                            throw new IOException("Received unexpected packet type (" + received.getType() + ")");
                        }
                    }
                    else
                    {
                        TFTPErrorPacket error = new TFTPErrorPacket(recdAddress, recdPort,
                                                                    TFTPErrorPacket.UNKNOWN_TID,
                                                                    "Unexpected host or port.");
                        bufferedSend(error);
                    }
                }
                catch(SocketException e)
                {
                    if(++timeouts >= __maxTimeouts)
                    {
                        throw new IOException("Connection timed out.");
                    }
                }
                catch(InterruptedIOException e)
                {
                    if(++timeouts >= __maxTimeouts)
                    {
                        throw new IOException("Connection timed out.");
                    }
                }
                catch(TFTPPacketException e)
                {
                    throw new IOException("Bad packet: " + e.getMessage());
                }
            }
            while(wantReply);

            ack.setBlockNumber(lastBlock);
            sent = ack;
            bytesRead += dataLength;
            totalBytesReceived += dataLength;
        }
        while(dataLength == TFTPPacket.SEGMENT_SIZE);
        bufferedSend(sent);
    }
    finally
    {
        endBufferedOps();
    }
    return bytesRead;
}
public int
    receiveFile(String filename, int mode, OutputStream output,
                String hostname, int port)
    throws UnknownHostException,
    IOException
{
    return receiveFile(filename, mode, output, InetAddress.getByName(hostname),
                       port);
}
public int
    receiveFile(String filename, int mode, OutputStream output,
                InetAddress host)
    throws IOException
{
    return receiveFile(filename, mode, output, host, DEFAULT_PORT);
}
public int
    receiveFile(String filename, int mode, OutputStream output,
                String hostname)
    throws UnknownHostException,
    IOException
{
    return receiveFile(filename, mode, output, InetAddress.getByName(hostname),
                       DEFAULT_PORT);
}
public void
    sendFile(String filename, int mode, InputStream input,
             InetAddress host, int port)
    throws IOException
{
    int block = 0;
    int hostPort = 0;
    boolean justStarted = true;
    boolean lastAckWait = false;

    totalBytesSent = 0L;

    if(mode == TFTP.ASCII_MODE)
    {
        input = new ToNetASCIIInputStream(input);
    }

    TFTPPacket sent = new TFTPWriteRequestPacket(host, port, filename, mode);
    TFTPDataPacket data = new TFTPDataPacket(host, port, 0, _sendBuffer, 4, 0);

    beginBufferedOps();

    try
    {
        do
        {
            bufferedSend(sent);
            boolean wantReply = true;
            int timeouts = 0;
            do
            {
                try
                {
                    TFTPPacket received = bufferedReceive();
                    final InetAddress recdAddress = received.getAddress();
                    final int recdPort = received.getPort();
                    if(justStarted)
                    {
                        justStarted = false;
                        if(recdPort == port)
                        {
                            TFTPErrorPacket error = new TFTPErrorPacket(recdAddress,
                                                                        recdPort, TFTPErrorPacket.UNKNOWN_TID,
                                                                        "INCORRECT SOURCE PORT");
                            bufferedSend(error);
                            throw new IOException("Incorrect source port (" + recdPort + ") in request reply.");
                        }
                        hostPort = recdPort;
                        data.setPort(hostPort);
                        if(!host.equals(recdAddress))
                        {
                            host = recdAddress;
                            data.setAddress(host);
                            sent.setAddress(host);
                        }
                    }
                    if(host.equals(recdAddress) && recdPort == hostPort)
                    {

                        switch(received.getType())
                        {
                        case TFTPPacket.ERROR:
                            TFTPErrorPacket error = (TFTPErrorPacket)received;
                            throw new IOException("Error code " + error.getError() +
                                                  " received: " + error.getMessage());
                        case TFTPPacket.ACKNOWLEDGEMENT:

                            int lastBlock = ((TFTPAckPacket)received).getBlockNumber();

                            if(lastBlock == block)
                            {
                                ++block;
                                if(block > 65535)
                                {
                                    block = 0;
                                }
                                wantReply = false;
                            }
                            else
                            {
                                discardPackets();
                            }
                            break;
                        default:
                            throw new IOException("Received unexpected packet type.");
                        }
                    }
                    else
                    {
                        TFTPErrorPacket error = new TFTPErrorPacket(recdAddress,
                                                                    recdPort,
                                                                    TFTPErrorPacket.UNKNOWN_TID,
                                                                    "Unexpected host or port.");
                        bufferedSend(error);
                    }
                }
                catch(SocketException e)
                {
                    if(++timeouts >= __maxTimeouts)
                    {
                        throw new IOException("Connection timed out.");
                    }
                }
                catch(InterruptedIOException e)
                {
                    if(++timeouts >= __maxTimeouts)
                    {
                        throw new IOException("Connection timed out.");
                    }
                }
                catch(TFTPPacketException e)
                {
                    throw new IOException("Bad packet: " + e.getMessage());
                }
            }
            while(wantReply);

            if(lastAckWait)
            {
                break;
            }
            int dataLength = TFTPPacket.SEGMENT_SIZE;
            int offset = 4;
            int totalThisPacket = 0;
            int bytesRead = 0;
            while(dataLength > 0 &&
                  (bytesRead = input.read(_sendBuffer, offset, dataLength)) > 0)
            {
                offset += bytesRead;
                dataLength -= bytesRead;
                totalThisPacket += bytesRead;
            }
            if(totalThisPacket < TFTPPacket.SEGMENT_SIZE)
            {
                lastAckWait = true;
            }
            data.setBlockNumber(block);
            data.setData(_sendBuffer, 4, totalThisPacket);
            sent = data;
            totalBytesSent += totalThisPacket;
        }
        while(true);
    }
    finally
    {
        endBufferedOps();
    }
}
public void
    sendFile(String filename, int mode, InputStream input,
             String hostname, int port)
    throws UnknownHostException,
    IOException
{
    sendFile(filename, mode, input, InetAddress.getByName(hostname), port);
}
public void
    sendFile(String filename, int mode, InputStream input,
             InetAddress host)
    throws IOException
{
    sendFile(filename, mode, input, host, DEFAULT_PORT);
}
public void
    sendFile(String filename, int mode, InputStream input,
             String hostname)
    throws UnknownHostException,
    IOException
{
    sendFile(filename, mode, input, InetAddress.getByName(hostname),
             DEFAULT_PORT);
}
}
