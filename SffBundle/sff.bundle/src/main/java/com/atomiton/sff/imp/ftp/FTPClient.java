package com.atomiton.sff.imp.ftp;

import java.io.*;
import java.net.*;
import java.util.*;

import com.atomiton.sff.imp.commons.MalformedServerReplyException;
import com.atomiton.sff.imp.ftp.io.*;
import com.atomiton.sff.imp.ftp.io.SocketInputStream;
import com.atomiton.sff.imp.ftp.io.SocketOutputStream;
import com.atomiton.sff.imp.ftp.parser.DefaultFTPFileEntryParserFactory;
import com.atomiton.sff.imp.ftp.parser.FTPFileEntryParserFactory;
import com.atomiton.sff.imp.ftp.parser.MLSxEntryParser;

public class FTPClient
extends
    FTP
implements
    Configurable
{
public static final String FTP_SYSTEM_TYPE = "com.atomiton.sff.imp.ftp.systemType";
public static final String FTP_SYSTEM_TYPE_DEFAULT = "com.atomiton.sff.imp.ftp.systemType.default";
public static final String SYSTEM_TYPE_PROPERTIES = "/systemType.properties";
public static final int ACTIVE_LOCAL_DATA_CONNECTION_MODE = 0;
public static final int ACTIVE_REMOTE_DATA_CONNECTION_MODE = 1;
public static final int PASSIVE_LOCAL_DATA_CONNECTION_MODE = 2;
public static final int PASSIVE_REMOTE_DATA_CONNECTION_MODE = 3;
private InputParams params;
private int __dataConnectionMode;
private int __dataTimeout;
private int __passivePort;
private String __passiveHost;
private final Random __random;
private int __activeMinPort;
private int __activeMaxPort;
private InetAddress __activeExternalHost;
private InetAddress __reportActiveExternalHost;
private InetAddress __passiveLocalHost;
private int __fileType;
@SuppressWarnings("unused")
private int __fileFormat;
@SuppressWarnings("unused")
private int __fileStructure;
@SuppressWarnings("unused")
private int __fileTransferMode;
private boolean __remoteVerificationEnabled;
private long __restartOffset;
private FTPFileEntryParserFactory __parserFactory;
private int __bufferSize;
private int __sendDataSocketBufferSize;
private int __receiveDataSocketBufferSize;
private boolean __listHiddenFiles;
private boolean __useEPSVwithIPv4;
private String __systemName;
private FTPFileEntryParser __entryParser;
private String __entryParserKey;
private FTPClientConfig __configuration;
private CopyStreamListener __copyStreamListener;
private long __controlKeepAliveTimeout;
private int __controlKeepAliveReplyTimeout = 1000;
private HostnameResolver __passiveNatWorkaroundStrategy = new NatServerResolverImpl(this);
private static final java.util.regex.Pattern __PARMS_PAT;
static
{
    __PARMS_PAT = java.util.regex.Pattern.compile(
                                                  "(\\d{1,3},\\d{1,3},\\d{1,3},\\d{1,3}),(\\d{1,3}),(\\d{1,3})");
}
private boolean __autodetectEncoding = false;
private HashMap<String, Set<String>> __featuresMap;
private static class PropertiesSingleton
{

static final Properties PROPERTIES;

static
{
    InputStream resourceAsStream = FTPClient.class.getResourceAsStream(SYSTEM_TYPE_PROPERTIES);
    Properties p = null;
    if(resourceAsStream != null)
    {
        p = new Properties();
        try
        {
            p.load(resourceAsStream);
        }
        catch(IOException e)
        {
        }
        finally
        {
            try
            {
                resourceAsStream.close();
            }
            catch(IOException e)
            {
            }
        }
    }
    PROPERTIES = p;
}
}
private static Properties
    getOverrideProperties()
{
    return PropertiesSingleton.PROPERTIES;
}
public FTPClient()
{
    __initDefaults();
    __dataTimeout = -1;
    __remoteVerificationEnabled = true;
    __parserFactory = new DefaultFTPFileEntryParserFactory();
    __configuration = null;
    __listHiddenFiles = false;
    __useEPSVwithIPv4 = false;
    __random = new Random();
    __passiveLocalHost = null;
    params = new InputParams();
}
private void
    __initDefaults()
{
    __dataConnectionMode = ACTIVE_LOCAL_DATA_CONNECTION_MODE;
    __passiveHost = null;
    __passivePort = -1;
    __activeExternalHost = null;
    __reportActiveExternalHost = null;
    __activeMinPort = 0;
    __activeMaxPort = 0;
    __fileType = FTP.ASCII_FILE_TYPE;
    __fileStructure = FTP.FILE_STRUCTURE;
    __fileFormat = FTP.NON_PRINT_TEXT_FORMAT;
    __fileTransferMode = FTP.STREAM_TRANSFER_MODE;
    __restartOffset = 0;
    __systemName = null;
    __entryParser = null;
    __entryParserKey = "";
    __featuresMap = null;
}
static String
    __parsePathname(String reply)
{
    String param = reply.substring(REPLY_CODE_LEN + 1);
    if(param.startsWith("\""))
    {
        StringBuilder sb = new StringBuilder();
        boolean quoteSeen = false;
        for(int i = 1; i < param.length(); i++)
        {
            char ch = param.charAt(i);
            if(ch == '"')
            {
                if(quoteSeen)
                {
                    sb.append(ch);
                    quoteSeen = false;
                }
                else
                {
                    quoteSeen = true;
                }
            }
            else
            {
                if(quoteSeen)
                {
                    return sb.toString();
                }
                sb.append(ch);
            }
        }
        if(quoteSeen)
        {
            return sb.toString();
        }
    }
    return param;
}

protected void
    _parsePassiveModeReply(String reply)
    throws MalformedServerReplyException
{
    java.util.regex.Matcher m = __PARMS_PAT.matcher(reply);
    if(!m.find())
    {
        throw new MalformedServerReplyException(
                                                "Could not parse passive host information.\nServer Reply: " + reply);
    }
    __passiveHost = m.group(1).replace(',', '.');
    try
    {
        int oct1 = Integer.parseInt(m.group(2));
        int oct2 = Integer.parseInt(m.group(3));
        __passivePort = (oct1 << 8) | oct2;
    }
    catch(NumberFormatException e)
    {
        throw new MalformedServerReplyException(
                                                "Could not parse passive port information.\nServer Reply: " + reply);
    }
    if(__passiveNatWorkaroundStrategy != null)
    {
        try
        {
            String passiveHost = __passiveNatWorkaroundStrategy.resolve(__passiveHost);
            if(!__passiveHost.equals(passiveHost))
            {
                fireReplyReceived(0,
                                  "[Replacing PASV mode reply address " + __passiveHost + " with " + passiveHost + "]\n");
                __passiveHost = passiveHost;
            }
        }
        catch(UnknownHostException e)
        {
            throw new MalformedServerReplyException(
                                                    "Could not parse passive host information.\nServer Reply: " + reply);
        }
    }
}

protected void
    _parseExtendedPassiveModeReply(String reply)
    throws MalformedServerReplyException
{
    reply = reply.substring(reply.indexOf('(') + 1,
                            reply.indexOf(')'))
                 .trim();

    char delim1, delim2, delim3, delim4;
    delim1 = reply.charAt(0);
    delim2 = reply.charAt(1);
    delim3 = reply.charAt(2);
    delim4 = reply.charAt(reply.length() - 1);

    if(!(delim1 == delim2) || !(delim2 == delim3)
       || !(delim3 == delim4))
    {
        throw new MalformedServerReplyException(
                                                "Could not parse extended passive host information.\nServer Reply: " + reply);
    }
    int port;
    try
    {
        port = Integer.parseInt(reply.substring(3, reply.length() - 1));
    }
    catch(NumberFormatException e)
    {
        throw new MalformedServerReplyException(
                                                "Could not parse extended passive host information.\nServer Reply: " + reply);
    }
    __passiveHost = getRemoteAddress().getHostAddress();
    __passivePort = port;
}

private boolean
    __storeFile(FTPCmd command, String remote, InputStream local)
    throws IOException
{
    return _storeFile(command.getCommand(), remote, local);
}
protected boolean
    _storeFile(String command, String remote, InputStream local)
    throws IOException
{
    Socket socket = _openDataConnection_(command, remote);

    if(socket == null)
    {
        return false;
    }
    final OutputStream output;
    if(__fileType == ASCII_FILE_TYPE)
    {
        output = new ToNetASCIIOutputStream(getBufferedOutputStream(socket.getOutputStream()));
    }
    else
    {
        output = getBufferedOutputStream(socket.getOutputStream());
    }
    CSL csl = null;
    if(__controlKeepAliveTimeout > 0)
    {
        csl = new CSL(this, __controlKeepAliveTimeout, __controlKeepAliveReplyTimeout);
    }

    try
    {
        Util.copyStream(local, output, getBufferSize(),
                        CopyStreamEvent.UNKNOWN_STREAM_SIZE, __mergeListeners(csl),
                        false);
    }
    catch(IOException e)
    {
        Util.closeQuietly(socket);
        if(csl != null)
        {
            csl.cleanUp();
        }
        throw e;
    }

    output.close();
    socket.close();
    if(csl != null)
    {
        csl.cleanUp();
    }
    boolean ok = completePendingCommand();
    return ok;
}

private OutputStream
    __storeFileStream(FTPCmd command, String remote)
    throws IOException
{
    return _storeFileStream(command.getCommand(), remote);
}
protected OutputStream
    _storeFileStream(String command, String remote)
    throws IOException
{
    Socket socket = _openDataConnection_(command, remote);

    if(socket == null)
    {
        return null;
    }

    final OutputStream output;
    if(__fileType == ASCII_FILE_TYPE)
    {
        output = new ToNetASCIIOutputStream(getBufferedOutputStream(socket.getOutputStream()));
    }
    else
    {
        output = socket.getOutputStream();
    }
    return new SocketOutputStream(socket, output);
}
protected Socket
    _openDataConnection_(FTPCmd command, String arg)
    throws IOException
{
    return _openDataConnection_(command.getCommand(), arg);
}
protected Socket
    _openDataConnection_(String command, String arg)
    throws IOException
{
    if(__dataConnectionMode != ACTIVE_LOCAL_DATA_CONNECTION_MODE &&
       __dataConnectionMode != PASSIVE_LOCAL_DATA_CONNECTION_MODE)
    {
        return null;
    }

    final boolean isInet6Address = getRemoteAddress() instanceof Inet6Address;

    Socket socket;

    if(__dataConnectionMode == ACTIVE_LOCAL_DATA_CONNECTION_MODE)
    {
        ServerSocket server = _serverSocketFactory_.createServerSocket(getActivePort(), 1, getHostAddress());

        try
        {
            if(isInet6Address)
            {
                if(!FTPReply.isPositiveCompletion(eprt(getReportHostAddress(), server.getLocalPort())))
                {
                    return null;
                }
            }
            else
            {
                if(!FTPReply.isPositiveCompletion(port(getReportHostAddress(), server.getLocalPort())))
                {
                    return null;
                }
            }

            if((__restartOffset > 0) && !restart(__restartOffset))
            {
                return null;
            }

            if(!FTPReply.isPositivePreliminary(sendCommand(command, arg)))
            {
                return null;
            }
            if(__dataTimeout >= 0)
            {
                server.setSoTimeout(__dataTimeout);
            }
            socket = server.accept();
            if(__dataTimeout >= 0)
            {
                socket.setSoTimeout(__dataTimeout);
            }
            if(__receiveDataSocketBufferSize > 0)
            {
                socket.setReceiveBufferSize(__receiveDataSocketBufferSize);
            }
            if(__sendDataSocketBufferSize > 0)
            {
                socket.setSendBufferSize(__sendDataSocketBufferSize);
            }
        }
        finally
        {
            server.close();
        }
    }
    else
    {
        boolean attemptEPSV = isUseEPSVwithIPv4() || isInet6Address;
        if(attemptEPSV && epsv() == FTPReply.ENTERING_EPSV_MODE)
        {
            _parseExtendedPassiveModeReply(_replyLines.get(0));
        }
        else
        {
            if(isInet6Address)
            {
                return null;
            }
            if(pasv() != FTPReply.ENTERING_PASSIVE_MODE)
            {
                return null;
            }
            _parsePassiveModeReply(_replyLines.get(0));
        }

        socket = _socketFactory_.createSocket();
        if(__receiveDataSocketBufferSize > 0)
        {
            socket.setReceiveBufferSize(__receiveDataSocketBufferSize);
        }
        if(__sendDataSocketBufferSize > 0)
        {
            socket.setSendBufferSize(__sendDataSocketBufferSize);
        }
        if(__passiveLocalHost != null)
        {
            socket.bind(new InetSocketAddress(__passiveLocalHost, 0));
        }
        if(__dataTimeout >= 0)
        {
            socket.setSoTimeout(__dataTimeout);
        }

        socket.connect(new InetSocketAddress(__passiveHost, __passivePort), connectTimeout);
        if((__restartOffset > 0) && !restart(__restartOffset))
        {
            socket.close();
            return null;
        }

        if(!FTPReply.isPositivePreliminary(sendCommand(command, arg)))
        {
            socket.close();
            return null;
        }
    }

    if(__remoteVerificationEnabled && !verifyRemote(socket))
    {
        socket.close();

        throw new IOException(
                              "Host attempting data connection " + socket.getInetAddress().getHostAddress() +
                              " is not same as server " + getRemoteAddress().getHostAddress());
    }

    return socket;
}
@Override
protected void
    _connectAction_()
    throws IOException
{
    _connectAction_(null);
}
@Override
protected void
    _connectAction_(Reader socketIsReader)
    throws IOException
{
    super._connectAction_(socketIsReader);
    __initDefaults();
    if(__autodetectEncoding)
    {
        ArrayList<String> oldReplyLines = new ArrayList<String>(_replyLines);
        int oldReplyCode = _replyCode;
        if(hasFeature("UTF8") || hasFeature("UTF-8"))
        {
            setControlEncoding("UTF-8");
            _controlInput_ = new CRLFLineReader(new InputStreamReader(_input_, getControlEncoding()));
            _controlOutput_ = new BufferedWriter(new OutputStreamWriter(_output_, getControlEncoding()));
        }
        _replyLines.clear();
        _replyLines.addAll(oldReplyLines);
        _replyCode = oldReplyCode;
        _newReplyString = true;
    }
}
public void
    setDataTimeout(int timeout)
{
    __dataTimeout = timeout;
}
public void
    setParserFactory(FTPFileEntryParserFactory parserFactory)
{
    __parserFactory = parserFactory;
}
public void
    disconnect()
    throws IOException
{
    super.disconnect();
    __initDefaults();
}
public void
    setRemoteVerificationEnabled(boolean enable)
{
    __remoteVerificationEnabled = enable;
}
public boolean
    isRemoteVerificationEnabled()
{
    return __remoteVerificationEnabled;
}
public boolean
    login(String username, String password)
    throws IOException
{
    user(username);

    if(FTPReply.isPositiveCompletion(_replyCode))
    {
        return true;
    }
    if(!FTPReply.isPositiveIntermediate(_replyCode))
    {
        return false;
    }

    return FTPReply.isPositiveCompletion(pass(password));
}
public boolean
    login(String username, String password, String account)
    throws IOException
{
    user(username);
    if(FTPReply.isPositiveCompletion(_replyCode))
    {
        return true;
    }
    if(!FTPReply.isPositiveIntermediate(_replyCode))
    {
        return false;
    }
    pass(password);

    if(FTPReply.isPositiveCompletion(_replyCode))
    {
        return true;
    }

    if(!FTPReply.isPositiveIntermediate(_replyCode))
    {
        return false;
    }

    return FTPReply.isPositiveCompletion(acct(account));
}
public boolean
    logout()
    throws IOException
{
    return FTPReply.isPositiveCompletion(quit());
}
public boolean
    changeWorkingDirectory(String pathname)
    throws IOException
{
    return FTPReply.isPositiveCompletion(cwd(pathname));
}
public boolean
    changeToParentDirectory()
    throws IOException
{
    return FTPReply.isPositiveCompletion(cdup());
}
public boolean
    structureMount(String pathname)
    throws IOException
{
    return FTPReply.isPositiveCompletion(smnt(pathname));
}
public boolean
    reinitialize()
    throws IOException
{
    rein();

    if(FTPReply.isPositiveCompletion(_replyCode) ||
       (FTPReply.isPositivePreliminary(_replyCode) &&
        FTPReply.isPositiveCompletion(getReply())))
    {

        __initDefaults();

        return true;
    }

    return false;
}
public void
    enterLocalActiveMode()
{
    __dataConnectionMode = ACTIVE_LOCAL_DATA_CONNECTION_MODE;
    __passiveHost = null;
    __passivePort = -1;
}
public void
    enterLocalPassiveMode()
{
    __dataConnectionMode = PASSIVE_LOCAL_DATA_CONNECTION_MODE;
    __passiveHost = null;
    __passivePort = -1;
}
public boolean
    enterRemoteActiveMode(InetAddress host, int port)
    throws IOException
{
    if(FTPReply.isPositiveCompletion(port(host, port)))
    {
        __dataConnectionMode = ACTIVE_REMOTE_DATA_CONNECTION_MODE;
        __passiveHost = null;
        __passivePort = -1;
        return true;
    }
    return false;
}
public boolean
    enterRemotePassiveMode()
    throws IOException
{
    if(pasv() != FTPReply.ENTERING_PASSIVE_MODE)
    {
        return false;
    }

    __dataConnectionMode = PASSIVE_REMOTE_DATA_CONNECTION_MODE;
    _parsePassiveModeReply(_replyLines.get(0));

    return true;
}
public String
    getPassiveHost()
{
    return __passiveHost;
}
public int
    getPassivePort()
{
    return __passivePort;
}
public int
    getDataConnectionMode()
{
    return __dataConnectionMode;
}
private int
    getActivePort()
{
    if(__activeMinPort > 0 && __activeMaxPort >= __activeMinPort)
    {
        if(__activeMaxPort == __activeMinPort)
        {
            return __activeMaxPort;
        }
        return __random.nextInt(__activeMaxPort - __activeMinPort + 1) + __activeMinPort;
    }
    else
    {
        return 0;
    }
}
private InetAddress
    getHostAddress()
{
    if(__activeExternalHost != null)
    {
        return __activeExternalHost;
    }
    else
    {
        return getLocalAddress();
    }
}
private InetAddress
    getReportHostAddress()
{
    if(__reportActiveExternalHost != null)
    {
        return __reportActiveExternalHost;
    }
    else
    {
        return getHostAddress();
    }
}
public void
    setActivePortRange(int minPort, int maxPort)
{
    this.__activeMinPort = minPort;
    this.__activeMaxPort = maxPort;
}
public void
    setActiveExternalIPAddress(String ipAddress)
    throws UnknownHostException
{
    this.__activeExternalHost = InetAddress.getByName(ipAddress);
}
public void
    setPassiveLocalIPAddress(String ipAddress)
    throws UnknownHostException
{
    this.__passiveLocalHost = InetAddress.getByName(ipAddress);
}
public void
    setPassiveLocalIPAddress(InetAddress inetAddress)
{
    this.__passiveLocalHost = inetAddress;
}
public InetAddress
    getPassiveLocalIPAddress()
{
    return this.__passiveLocalHost;
}
public void
    setReportActiveExternalIPAddress(String ipAddress)
    throws UnknownHostException
{
    this.__reportActiveExternalHost = InetAddress.getByName(ipAddress);
}
public boolean
    setFileType(int fileType)
    throws IOException
{
    if(FTPReply.isPositiveCompletion(type(fileType)))
    {
        __fileType = fileType;
        __fileFormat = FTP.NON_PRINT_TEXT_FORMAT;
        return true;
    }
    return false;
}
public boolean
    setFileType(int fileType, int formatOrByteSize)
    throws IOException
{
    if(FTPReply.isPositiveCompletion(type(fileType, formatOrByteSize)))
    {
        __fileType = fileType;
        __fileFormat = formatOrByteSize;
        return true;
    }
    return false;
}
public boolean
    setFileStructure(int structure)
    throws IOException
{
    if(FTPReply.isPositiveCompletion(stru(structure)))
    {
        __fileStructure = structure;
        return true;
    }
    return false;
}
public boolean
    setFileTransferMode(int mode)
    throws IOException
{
    if(FTPReply.isPositiveCompletion(mode(mode)))
    {
        __fileTransferMode = mode;
        return true;
    }
    return false;
}
public boolean
    remoteRetrieve(String filename)
    throws IOException
{
    if(__dataConnectionMode == ACTIVE_REMOTE_DATA_CONNECTION_MODE ||
       __dataConnectionMode == PASSIVE_REMOTE_DATA_CONNECTION_MODE)
    {
        return FTPReply.isPositivePreliminary(retr(filename));
    }
    return false;
}
public boolean
    remoteStore(String filename)
    throws IOException
{
    if(__dataConnectionMode == ACTIVE_REMOTE_DATA_CONNECTION_MODE ||
       __dataConnectionMode == PASSIVE_REMOTE_DATA_CONNECTION_MODE)
    {
        return FTPReply.isPositivePreliminary(stor(filename));
    }
    return false;
}
public boolean
    remoteStoreUnique(String filename)
    throws IOException
{
    if(__dataConnectionMode == ACTIVE_REMOTE_DATA_CONNECTION_MODE ||
       __dataConnectionMode == PASSIVE_REMOTE_DATA_CONNECTION_MODE)
    {
        return FTPReply.isPositivePreliminary(stou(filename));
    }
    return false;
}
public boolean
    remoteStoreUnique()
    throws IOException
{
    if(__dataConnectionMode == ACTIVE_REMOTE_DATA_CONNECTION_MODE ||
       __dataConnectionMode == PASSIVE_REMOTE_DATA_CONNECTION_MODE)
    {
        return FTPReply.isPositivePreliminary(stou());
    }
    return false;
}
public boolean
    remoteAppend(String filename)
    throws IOException
{
    if(__dataConnectionMode == ACTIVE_REMOTE_DATA_CONNECTION_MODE ||
       __dataConnectionMode == PASSIVE_REMOTE_DATA_CONNECTION_MODE)
    {
        return FTPReply.isPositivePreliminary(appe(filename));
    }
    return false;
}
public boolean
    completePendingCommand()
    throws IOException
{
    return FTPReply.isPositiveCompletion(getReply());
}
public boolean
    retrieveFile(String remote, OutputStream local)
    throws IOException
{
    return _retrieveFile(FTPCmd.RETR.getCommand(), remote, local);
}
protected boolean
    _retrieveFile(String command, String remote, OutputStream local)
    throws IOException
{
    Socket socket = _openDataConnection_(command, remote);

    if(socket == null)
    {
        return false;
    }

    final InputStream input;
    if(__fileType == ASCII_FILE_TYPE)
    {
        input = new FromNetASCIIInputStream(getBufferedInputStream(socket.getInputStream()));
    }
    else
    {
        input = getBufferedInputStream(socket.getInputStream());
    }

    CSL csl = null;
    if(__controlKeepAliveTimeout > 0)
    {
        csl = new CSL(this, __controlKeepAliveTimeout, __controlKeepAliveReplyTimeout);
    }

    try
    {
        Util.copyStream(input, local, getBufferSize(),
                        CopyStreamEvent.UNKNOWN_STREAM_SIZE, __mergeListeners(csl),
                        false);
    }
    finally
    {
        Util.closeQuietly(input);
        Util.closeQuietly(socket);
        if(csl != null)
        {
            csl.cleanUp();
        }
    }

    boolean ok = completePendingCommand();
    return ok;
}
public InputStream
    retrieveFileStream(String remote)
    throws IOException
{
    return _retrieveFileStream(FTPCmd.RETR.getCommand(), remote);
}
protected InputStream
    _retrieveFileStream(String command, String remote)
    throws IOException
{
    Socket socket = _openDataConnection_(command, remote);

    if(socket == null)
    {
        return null;
    }

    final InputStream input;
    if(__fileType == ASCII_FILE_TYPE)
    {
        input = new FromNetASCIIInputStream(getBufferedInputStream(socket.getInputStream()));
    }
    else
    {
        input = socket.getInputStream();
    }
    return new SocketInputStream(socket, input);
}
public boolean
    storeFile(String remote, InputStream local)
    throws IOException
{
    return __storeFile(FTPCmd.STOR, remote, local);
}
public OutputStream
    storeFileStream(String remote)
    throws IOException
{
    return __storeFileStream(FTPCmd.STOR, remote);
}
public boolean
    appendFile(String remote, InputStream local)
    throws IOException
{
    return __storeFile(FTPCmd.APPE, remote, local);
}
public OutputStream
    appendFileStream(String remote)
    throws IOException
{
    return __storeFileStream(FTPCmd.APPE, remote);
}
public boolean
    storeUniqueFile(String remote, InputStream local)
    throws IOException
{
    return __storeFile(FTPCmd.STOU, remote, local);
}
public OutputStream
    storeUniqueFileStream(String remote)
    throws IOException
{
    return __storeFileStream(FTPCmd.STOU, remote);
}
public boolean
    storeUniqueFile(InputStream local)
    throws IOException
{
    return __storeFile(FTPCmd.STOU, null, local);
}
public OutputStream
    storeUniqueFileStream()
    throws IOException
{
    return __storeFileStream(FTPCmd.STOU, null);
}
public boolean
    allocate(int bytes)
    throws IOException
{
    return FTPReply.isPositiveCompletion(allo(bytes));
}
public boolean
    features()
    throws IOException
{
    return FTPReply.isPositiveCompletion(feat());
}
public String[]
    featureValues(String feature)
    throws IOException
{
    if(!initFeatureMap())
    {
        return null;
    }
    Set<String> entries = __featuresMap.get(feature.toUpperCase(Locale.ENGLISH));
    if(entries != null)
    {
        return entries.toArray(new String[entries.size()]);
    }
    return null;
}
public String
    featureValue(String feature)
    throws IOException
{
    String[] values = featureValues(feature);
    if(values != null)
    {
        return values[0];
    }
    return null;
}
public boolean
    hasFeature(String feature)
    throws IOException
{
    if(!initFeatureMap())
    {
        return false;
    }
    return __featuresMap.containsKey(feature.toUpperCase(Locale.ENGLISH));
}
public boolean
    hasFeature(String feature, String value)
    throws IOException
{
    if(!initFeatureMap())
    {
        return false;
    }
    Set<String> entries = __featuresMap.get(feature.toUpperCase(Locale.ENGLISH));
    if(entries != null)
    {
        return entries.contains(value);
    }
    return false;
}
private boolean
    initFeatureMap()
    throws IOException
{
    if(__featuresMap == null)
    {
        final int replyCode = feat();
        if(replyCode == FTPReply.NOT_LOGGED_IN)
        {
            return false;
        }
        boolean success = FTPReply.isPositiveCompletion(replyCode);
        __featuresMap = new HashMap<String, Set<String>>();
        if(!success)
        {
            return false;
        }
        for(String l : getReplyStrings())
        {
            if(l.startsWith(" "))
            {
                String key;
                String value = "";
                int varsep = l.indexOf(' ', 1);
                if(varsep > 0)
                {
                    key = l.substring(1, varsep);
                    value = l.substring(varsep + 1);
                }
                else
                {
                    key = l.substring(1);
                }
                key = key.toUpperCase(Locale.ENGLISH);
                Set<String> entries = __featuresMap.get(key);
                if(entries == null)
                {
                    entries = new HashSet<String>();
                    __featuresMap.put(key, entries);
                }
                entries.add(value);
            }
        }
    }
    return true;
}
public boolean
    allocate(int bytes, int recordSize)
    throws IOException
{
    return FTPReply.isPositiveCompletion(allo(bytes, recordSize));
}
public boolean
    doCommand(String command, String params)
    throws IOException
{
    return FTPReply.isPositiveCompletion(sendCommand(command, params));
}
public String[]
    doCommandAsStrings(String command, String params)
    throws IOException
{
    boolean success = FTPReply.isPositiveCompletion(sendCommand(command, params));
    if(success)
    {
        return getReplyStrings();
    }
    else
    {
        return null;
    }
}
public FTPFile
    mlistFile(String pathname)
    throws IOException
{
    boolean success = FTPReply.isPositiveCompletion(sendCommand(FTPCmd.MLST, pathname));
    if(success)
    {
        String reply = getReplyStrings()[1];
        if(reply.length() < 3 || reply.charAt(0) != ' ')
        {
            throw new MalformedServerReplyException("Invalid server reply (MLST): '" + reply + "'");
        }
        String entry = reply.substring(1);
        return MLSxEntryParser.parseEntry(entry);
    }
    else
    {
        return null;
    }
}
public FTPFile[]
    mlistDir()
    throws IOException
{
    return mlistDir(null);
}
public FTPFile[]
    mlistDir(String pathname)
    throws IOException
{
    FTPListParseEngine engine = initiateMListParsing(pathname);
    return engine.getFiles();
}
public FTPFile[]
    mlistDir(String pathname, FTPFileFilter filter)
    throws IOException
{
    FTPListParseEngine engine = initiateMListParsing(pathname);
    return engine.getFiles(filter);
}
protected boolean
    restart(long offset)
    throws IOException
{
    __restartOffset = 0;
    return FTPReply.isPositiveIntermediate(rest(Long.toString(offset)));
}
public void
    setRestartOffset(long offset)
{
    if(offset >= 0)
    {
        __restartOffset = offset;
    }
}
public long
    getRestartOffset()
{
    return __restartOffset;
}
public boolean
    rename(String from, String to)
    throws IOException
{
    if(!FTPReply.isPositiveIntermediate(rnfr(from)))
    {
        return false;
    }

    return FTPReply.isPositiveCompletion(rnto(to));
}
public boolean
    abort()
    throws IOException
{
    return FTPReply.isPositiveCompletion(abor());
}
public boolean
    deleteFile(String pathname)
    throws IOException
{
    return FTPReply.isPositiveCompletion(dele(pathname));
}
public boolean
    removeDirectory(String pathname)
    throws IOException
{
    return FTPReply.isPositiveCompletion(rmd(pathname));
}
public boolean
    makeDirectory(String pathname)
    throws IOException
{
    return FTPReply.isPositiveCompletion(mkd(pathname));
}
public String
    printWorkingDirectory()
    throws IOException
{
    if(pwd() != FTPReply.PATHNAME_CREATED)
    {
        return null;
    }

    return __parsePathname(_replyLines.get(_replyLines.size() - 1));
}
public boolean
    sendSiteCommand(String arguments)
    throws IOException
{
    return FTPReply.isPositiveCompletion(site(arguments));
}
public String
    getSystemType()
    throws IOException
{
    if(__systemName == null)
    {
        if(FTPReply.isPositiveCompletion(syst()))
        {
            __systemName = _replyLines.get(_replyLines.size() - 1).substring(4);
        }
        else
        {
            String systDefault = System.getProperty(FTP_SYSTEM_TYPE_DEFAULT);
            if(systDefault != null)
            {
                __systemName = systDefault;
            }
            else
            {
                throw new IOException("Unable to determine system type - response: " + getReplyString());
            }
        }
    }
    return __systemName;
}
public String
    listHelp()
    throws IOException
{
    if(FTPReply.isPositiveCompletion(help()))
    {
        return getReplyString();
    }
    return null;
}
public String
    listHelp(String command)
    throws IOException
{
    if(FTPReply.isPositiveCompletion(help(command)))
    {
        return getReplyString();
    }
    return null;
}
public boolean
    sendNoOp()
    throws IOException
{
    return FTPReply.isPositiveCompletion(noop());
}
public String[]
    listNames(String pathname)
    throws IOException
{
    Socket socket = _openDataConnection_(FTPCmd.NLST, getListArguments(pathname));

    if(socket == null)
    {
        return null;
    }

    BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), getControlEncoding()));

    ArrayList<String> results = new ArrayList<String>();
    String line;
    while((line = reader.readLine()) != null)
    {
        results.add(line);
    }

    reader.close();
    socket.close();

    if(completePendingCommand())
    {
        String[] names = new String[results.size()];
        return results.toArray(names);
    }

    return null;
}
public String[]
    listNames()
    throws IOException
{
    return listNames(null);
}
public FTPFile[]
    listFiles(String pathname)
    throws IOException
{
    FTPListParseEngine engine = initiateListParsing((String)null, pathname);
    return engine.getFiles();

}
public FTPFile[]
    listFiles()
    throws IOException
{
    return listFiles((String)null);
}
public FTPFile[]
    listFiles(String pathname, FTPFileFilter filter)
    throws IOException
{
    FTPListParseEngine engine = initiateListParsing((String)null, pathname);
    return engine.getFiles(filter);

}
public FTPFile[]
    listDirectories()
    throws IOException
{
    return listDirectories((String)null);
}
public FTPFile[]
    listDirectories(String parent)
    throws IOException
{
    return listFiles(parent, FTPFileFilters.DIRECTORIES);
}
public FTPListParseEngine
    initiateListParsing()
    throws IOException
{
    return initiateListParsing((String)null);
}
public FTPListParseEngine
    initiateListParsing(String pathname)
    throws IOException
{
    return initiateListParsing((String)null, pathname);
}
public FTPListParseEngine
    initiateListParsing(
                        String parserKey, String pathname)
    throws IOException
{
    __createParser(parserKey);
    return initiateListParsing(__entryParser, pathname);
}

void __createParser(String parserKey)
throws IOException
{
    if(__entryParser == null || (parserKey != null && !__entryParserKey.equals(parserKey)))
    {
        if(null != parserKey)
        {
            __entryParser = __parserFactory.createFileEntryParser(parserKey);
            __entryParserKey = parserKey;

        }
        else
        {
            if(null != __configuration && __configuration.getServerSystemKey().length() > 0)
            {
                __entryParser = __parserFactory.createFileEntryParser(__configuration);
                __entryParserKey = __configuration.getServerSystemKey();
            }
            else
            {
                String systemType = System.getProperty(FTP_SYSTEM_TYPE);
                if(systemType == null)
                {
                    systemType = getSystemType();
                    Properties override = getOverrideProperties();
                    if(override != null)
                    {
                        String newType = override.getProperty(systemType);
                        if(newType != null)
                        {
                            systemType = newType;
                        }
                    }
                }
                if(null != __configuration)
                {
                    __entryParser = __parserFactory.createFileEntryParser(new FTPClientConfig(systemType, __configuration));
                }
                else
                {
                    __entryParser = __parserFactory.createFileEntryParser(systemType);
                }
                __entryParserKey = systemType;
            }
        }
    }

}
private FTPListParseEngine
    initiateListParsing(
                        FTPFileEntryParser parser, String pathname)
    throws IOException
{
    Socket socket = _openDataConnection_(FTPCmd.LIST, getListArguments(pathname));

    FTPListParseEngine engine = new FTPListParseEngine(parser, __configuration);
    if(socket == null)
    {
        return engine;
    }

    try
    {
        engine.readServerList(socket.getInputStream(), getControlEncoding());
    }
    finally
    {
        Util.closeQuietly(socket);
    }

    completePendingCommand();
    return engine;
}
private FTPListParseEngine
    initiateMListParsing(String pathname)
    throws IOException
{
    Socket socket = _openDataConnection_(FTPCmd.MLSD, pathname);
    FTPListParseEngine engine = new FTPListParseEngine(MLSxEntryParser.getInstance(), __configuration);
    if(socket == null)
    {
        return engine;
    }

    try
    {
        engine.readServerList(socket.getInputStream(), getControlEncoding());
    }
    finally
    {
        Util.closeQuietly(socket);
        completePendingCommand();
    }
    return engine;
}
protected String
    getListArguments(String pathname)
{
    if(getListHiddenFiles())
    {
        if(pathname != null)
        {
            StringBuilder sb = new StringBuilder(pathname.length() + 3);
            sb.append("-a ");
            sb.append(pathname);
            return sb.toString();
        }
        else
        {
            return "-a";
        }
    }

    return pathname;
}
public String
    getStatus()
    throws IOException
{
    if(FTPReply.isPositiveCompletion(stat()))
    {
        return getReplyString();
    }
    return null;
}
public String
    getStatus(String pathname)
    throws IOException
{
    if(FTPReply.isPositiveCompletion(stat(pathname)))
    {
        return getReplyString();
    }
    return null;
}
public String
    getModificationTime(String pathname)
    throws IOException
{
    if(FTPReply.isPositiveCompletion(mdtm(pathname)))
    {
        return getReplyStrings()[0].substring(4);
    }
    return null;
}
public FTPFile
    mdtmFile(String pathname)
    throws IOException
{
    if(FTPReply.isPositiveCompletion(mdtm(pathname)))
    {
        String reply = getReplyStrings()[0].substring(4);
        FTPFile file = new FTPFile();
        file.setName(pathname);
        file.setRawListing(reply);
        file.setTimestamp(MLSxEntryParser.parseGMTdateTime(reply));
        return file;
    }
    return null;
}
public boolean
    setModificationTime(String pathname, String timeval)
    throws IOException
{
    return(FTPReply.isPositiveCompletion(mfmt(pathname, timeval)));
}
public void
    setBufferSize(int bufSize)
{
    __bufferSize = bufSize;
}
public int
    getBufferSize()
{
    return __bufferSize;
}
public void
    setSendDataSocketBufferSize(int bufSize)
{
    __sendDataSocketBufferSize = bufSize;
}
public int
    getSendDataSocketBufferSize()
{
    return __sendDataSocketBufferSize;
}
public void
    setReceieveDataSocketBufferSize(int bufSize)
{
    __receiveDataSocketBufferSize = bufSize;
}
public int
    getReceiveDataSocketBufferSize()
{
    return __receiveDataSocketBufferSize;
}
@Override
public void
    configure(FTPClientConfig config)
{
    this.__configuration = config;
}
public void
    setListHiddenFiles(boolean listHiddenFiles)
{
    this.__listHiddenFiles = listHiddenFiles;
}
public boolean
    getListHiddenFiles()
{
    return this.__listHiddenFiles;
}
public boolean
    isUseEPSVwithIPv4()
{
    return __useEPSVwithIPv4;
}
public void
    setUseEPSVwithIPv4(boolean selected)
{
    this.__useEPSVwithIPv4 = selected;
}
public void
    setCopyStreamListener(CopyStreamListener listener)
{
    __copyStreamListener = listener;
}
public CopyStreamListener
    getCopyStreamListener()
{
    return __copyStreamListener;
}
public void
    setControlKeepAliveTimeout(long controlIdle)
{
    __controlKeepAliveTimeout = controlIdle * 1000;
}
public long
    getControlKeepAliveTimeout()
{
    return __controlKeepAliveTimeout / 1000;
}
public void
    setControlKeepAliveReplyTimeout(int timeout)
{
    __controlKeepAliveReplyTimeout = timeout;
}
public int
    getControlKeepAliveReplyTimeout()
{
    return __controlKeepAliveReplyTimeout;
}
public void
    setPassiveNatWorkaroundStrategy(HostnameResolver resolver)
{
    this.__passiveNatWorkaroundStrategy = resolver;
}
public static interface HostnameResolver
{
String resolve(String hostname)
throws UnknownHostException;
}
public static class NatServerResolverImpl
implements
    HostnameResolver
{
private FTPClient client;

public NatServerResolverImpl(FTPClient client)
{
    this.client = client;
}
@Override
public String
    resolve(String hostname)
    throws UnknownHostException
{
    String newHostname = hostname;
    InetAddress host = InetAddress.getByName(newHostname);
    if(host.isSiteLocalAddress())
    {
        InetAddress remote = this.client.getRemoteAddress();
        if(!remote.isSiteLocalAddress())
        {
            newHostname = remote.getHostAddress();
        }
    }
    return newHostname;
}
}
private OutputStream
    getBufferedOutputStream(OutputStream outputStream)
{
    if(__bufferSize > 0)
    {
        return new BufferedOutputStream(outputStream, __bufferSize);
    }
    return new BufferedOutputStream(outputStream);
}

private InputStream
    getBufferedInputStream(InputStream inputStream)
{
    if(__bufferSize > 0)
    {
        return new BufferedInputStream(inputStream, __bufferSize);
    }
    return new BufferedInputStream(inputStream);
}

private static class CSL
implements
    CopyStreamListener
{

private final FTPClient parent;
private final long idle;
private final int currentSoTimeout;

private long time = System.currentTimeMillis();
private int notAcked;

CSL(FTPClient parent, long idleTime, int maxWait)
throws SocketException
{
    this.idle = idleTime;
    this.parent = parent;
    this.currentSoTimeout = parent.getSoTimeout();
    parent.setSoTimeout(maxWait);
}

@Override
public void
    bytesTransferred(CopyStreamEvent event)
{
    bytesTransferred(event.getTotalBytesTransferred(), event.getBytesTransferred(), event.getStreamSize());
}

@Override
public void
    bytesTransferred(long totalBytesTransferred,
                     int bytesTransferred, long streamSize)
{
    long now = System.currentTimeMillis();
    if(now - time > idle)
    {
        try
        {
            parent.__noop();
        }
        catch(SocketTimeoutException e)
        {
            notAcked++;
        }
        catch(IOException e)
        {
        }
        time = now;
    }
}

void cleanUp()
throws IOException
{
    try
    {
        while(notAcked-- > 0)
        {
            parent.__getReplyNoReport();
        }
    }
    finally
    {
        parent.setSoTimeout(currentSoTimeout);
    }
}

}
private CopyStreamListener
    __mergeListeners(CopyStreamListener local)
{
    if(local == null)
    {
        return __copyStreamListener;
    }
    if(__copyStreamListener == null)
    {
        return local;
    }
    CopyStreamAdapter merged = new CopyStreamAdapter();
    merged.addCopyStreamListener(local);
    merged.addCopyStreamListener(__copyStreamListener);
    return merged;
}
public void
    setAutodetectUTF8(boolean autodetect)
{
    __autodetectEncoding = autodetect;
}
public boolean
    getAutodetectUTF8()
{
    return __autodetectEncoding;
}

FTPFileEntryParser getEntryParser()
{
    return __entryParser;
}
public InputParams
    getParams()
{
    return params;
}
/* (non-Javadoc)
 * @see java.lang.Object#hashCode()
 */
@Override
public int
    hashCode()
{
    final int prime = 31;
    int result = 1;
    result = prime * result + ((params == null) ? 0 : params.hashCode());
    return result;
}
/* (non-Javadoc)
 * @see java.lang.Object#equals(java.lang.Object)
 */
@Override
public boolean
    equals(Object obj)
{
    if(this == obj)
        return true;
    if(obj == null)
        return false;
    if(!(obj instanceof FTPClient))
        return false;
    FTPClient other = (FTPClient)obj;
    if(params == null)
    {
        if(other.params != null)
            return false;
    }
    else if(!params.equals(other.params))
        return false;
    return true;
}
}
