package com.atomiton.sff.imp.commons;

import java.io.IOException;
import java.net.*;

import javax.net.SocketFactory;

public class DefaultSocketFactory
extends
    SocketFactory
{
private final Proxy connProxy;
public DefaultSocketFactory()
{
    this(null);
}
public DefaultSocketFactory(Proxy proxy)
{
    connProxy = proxy;
}
@Override
public Socket
    createSocket()
    throws IOException
{
    if(connProxy != null)
    {
        return new Socket(connProxy);
    }
    return new Socket();
}
@Override
public Socket
    createSocket(String host, int port)
    throws UnknownHostException,
    IOException
{
    if(connProxy != null)
    {
        Socket s = new Socket(connProxy);
        s.connect(new InetSocketAddress(host, port));
        return s;
    }
    return new Socket(host, port);
}
@Override
public Socket
    createSocket(InetAddress address, int port)
    throws IOException
{
    if(connProxy != null)
    {
        Socket s = new Socket(connProxy);
        s.connect(new InetSocketAddress(address, port));
        return s;
    }
    return new Socket(address, port);
}
@Override
public Socket
    createSocket(String host, int port,
                 InetAddress localAddr, int localPort)
    throws UnknownHostException,
    IOException
{
    if(connProxy != null)
    {
        Socket s = new Socket(connProxy);
        s.bind(new InetSocketAddress(localAddr, localPort));
        s.connect(new InetSocketAddress(host, port));
        return s;
    }
    return new Socket(host, port, localAddr, localPort);
}
@Override
public Socket
    createSocket(InetAddress address, int port,
                 InetAddress localAddr, int localPort)
    throws IOException
{
    if(connProxy != null)
    {
        Socket s = new Socket(connProxy);
        s.bind(new InetSocketAddress(localAddr, localPort));
        s.connect(new InetSocketAddress(address, port));
        return s;
    }
    return new Socket(address, port, localAddr, localPort);
}
public ServerSocket
    createServerSocket(int port)
    throws IOException
{
    return new ServerSocket(port);
}
public ServerSocket
    createServerSocket(int port, int backlog)
    throws IOException
{
    return new ServerSocket(port, backlog);
}
public ServerSocket
    createServerSocket(int port, int backlog,
                       InetAddress bindAddr)
    throws IOException
{
    return new ServerSocket(port, backlog, bindAddr);
}
}
