package com.atomiton.sff.imp.tftp;

import java.net.DatagramPacket;
import java.net.InetAddress;

public abstract class TFTPRequestPacket
extends
    TFTPPacket
{
static final String[] _modeStrings =
{"netascii", "octet"};
private static final byte[] _modeBytes[] =
{
 {(byte)'n', (byte)'e', (byte)'t', (byte)'a', (byte)'s', (byte)'c',
  (byte)'i', (byte)'i', 0},
 {(byte)'o', (byte)'c', (byte)'t', (byte)'e', (byte)'t', 0}
};
private final int _mode;
private final String _filename;
TFTPRequestPacket(InetAddress destination, int port,
                  int type, String filename, int mode)
{
    super(type, destination, port);

    _filename = filename;
    _mode = mode;
}
TFTPRequestPacket(int type, DatagramPacket datagram)
throws TFTPPacketException
{
    super(type, datagram.getAddress(), datagram.getPort());

    byte[] data = datagram.getData();

    if(getType() != data[1])
    {
        throw new TFTPPacketException("TFTP operator code does not match type.");
    }

    StringBuilder buffer = new StringBuilder();

    int index = 2;
    int length = datagram.getLength();

    while(index < length && data[index] != 0)
    {
        buffer.append((char)data[index]);
        ++index;
    }

    _filename = buffer.toString();

    if(index >= length)
    {
        throw new TFTPPacketException("Bad filename and mode format.");
    }

    buffer.setLength(0);
    ++index;
    while(index < length && data[index] != 0)
    {
        buffer.append((char)data[index]);
        ++index;
    }

    String modeString = buffer.toString().toLowerCase(java.util.Locale.ENGLISH);
    length = _modeStrings.length;

    int mode = 0;
    for(index = 0; index < length; index++)
    {
        if(modeString.equals(_modeStrings[index]))
        {
            mode = index;
            break;
        }
    }

    _mode = mode;

    if(index >= length)
    {
        throw new TFTPPacketException("Unrecognized TFTP transfer mode: " + modeString);
    }
}
@Override
final DatagramPacket
    _newDatagram(DatagramPacket datagram, byte[] data)
{
    int fileLength, modeLength;

    fileLength = _filename.length();
    modeLength = _modeBytes[_mode].length;

    data[0] = 0;
    data[1] = (byte)_type;
    System.arraycopy(_filename.getBytes(), 0, data, 2, fileLength);
    data[fileLength + 2] = 0;
    System.arraycopy(_modeBytes[_mode], 0, data, fileLength + 3,
                     modeLength);

    datagram.setAddress(_address);
    datagram.setPort(_port);
    datagram.setData(data);
    datagram.setLength(fileLength + modeLength + 3);

    return datagram;
}
@Override
public final DatagramPacket
    newDatagram()
{
    int fileLength, modeLength;
    byte[] data;

    fileLength = _filename.length();
    modeLength = _modeBytes[_mode].length;

    data = new byte[fileLength + modeLength + 4];
    data[0] = 0;
    data[1] = (byte)_type;
    System.arraycopy(_filename.getBytes(), 0, data, 2, fileLength);
    data[fileLength + 2] = 0;
    System.arraycopy(_modeBytes[_mode], 0, data, fileLength + 3,
                     modeLength);

    return new DatagramPacket(data, data.length, _address, _port);
}
public final int
    getMode()
{
    return _mode;
}
public final String
    getFilename()
{
    return _filename;
}
}
