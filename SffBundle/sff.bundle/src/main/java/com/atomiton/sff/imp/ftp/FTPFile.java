package com.atomiton.sff.imp.ftp;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.TimeZone;

public class FTPFile
implements
    Serializable
{
private static final long serialVersionUID = 9010790363003271996L;
public static final int FILE_TYPE = 0;
public static final int DIRECTORY_TYPE = 1;
public static final int SYMBOLIC_LINK_TYPE = 2;
public static final int UNKNOWN_TYPE = 3;
public static final int USER_ACCESS = 0;
public static final int GROUP_ACCESS = 1;
public static final int WORLD_ACCESS = 2;
public static final int READ_PERMISSION = 0;
public static final int WRITE_PERMISSION = 1;
public static final int EXECUTE_PERMISSION = 2;
private int _type, _hardLinkCount;
private long _size;
private String _rawListing, _user, _group, _name, _link;
private Calendar _date;
private final boolean[] _permissions[];
public FTPFile()
{
    _permissions = new boolean[3][3];
    _type = UNKNOWN_TYPE;
    _hardLinkCount = 0;
    _size = -1;
    _user = "";
    _group = "";
    _date = null;
    _name = null;
}
FTPFile(String rawListing)
{
    _permissions = null;
    _rawListing = rawListing;
    _type = UNKNOWN_TYPE;
    _hardLinkCount = 0;
    _size = -1;
    _user = "";
    _group = "";
    _date = null;
    _name = null;
}
public void
    setRawListing(String rawListing)
{
    _rawListing = rawListing;
}
public String
    getRawListing()
{
    return _rawListing;
}
public boolean
    isDirectory()
{
    return(_type == DIRECTORY_TYPE);
}
public boolean
    isFile()
{
    return(_type == FILE_TYPE);
}
public boolean
    isSymbolicLink()
{
    return(_type == SYMBOLIC_LINK_TYPE);
}
public boolean
    isUnknown()
{
    return(_type == UNKNOWN_TYPE);
}
public boolean
    isValid()
{
    return(_permissions != null);
}
public void
    setType(int type)
{
    _type = type;
}
public int
    getType()
{
    return _type;
}
public void
    setName(String name)
{
    _name = name;
}
public String
    getName()
{
    return _name;
}
public void
    setSize(long size)
{
    _size = size;
}
public long
    getSize()
{
    return _size;
}
public void
    setHardLinkCount(int links)
{
    _hardLinkCount = links;
}
public int
    getHardLinkCount()
{
    return _hardLinkCount;
}
public void
    setGroup(String group)
{
    _group = group;
}
public String
    getGroup()
{
    return _group;
}
public void
    setUser(String user)
{
    _user = user;
}
public String
    getUser()
{
    return _user;
}
public void
    setLink(String link)
{
    _link = link;
}
public String
    getLink()
{
    return _link;
}
public void
    setTimestamp(Calendar date)
{
    _date = date;
}
public Calendar
    getTimestamp()
{
    return _date;
}
public void
    setPermission(int access, int permission, boolean value)
{
    _permissions[access][permission] = value;
}
public boolean
    hasPermission(int access, int permission)
{
    if(_permissions == null)
    {
        return false;
    }
    return _permissions[access][permission];
}
@Override
public String
    toString()
{
    return getRawListing();
}
public String
    toFormattedString()
{
    return toFormattedString(null);
}
public String
    toFormattedString(final String timezone)
{

    if(!isValid())
    {
        return "[Invalid: could not parse file entry]";
    }
    StringBuilder sb = new StringBuilder();
    Formatter fmt = new Formatter(sb);
    sb.append(formatType());
    sb.append(permissionToString(USER_ACCESS));
    sb.append(permissionToString(GROUP_ACCESS));
    sb.append(permissionToString(WORLD_ACCESS));
    fmt.format(" %4d", Integer.valueOf(getHardLinkCount()));
    fmt.format(" %-8s %-8s", getUser(), getGroup());
    fmt.format(" %8d", Long.valueOf(getSize()));
    Calendar timestamp = getTimestamp();
    if(timestamp != null)
    {
        if(timezone != null)
        {
            TimeZone newZone = TimeZone.getTimeZone(timezone);
            if(!newZone.equals(timestamp.getTimeZone()))
            {
                Date original = timestamp.getTime();
                Calendar newStamp = Calendar.getInstance(newZone);
                newStamp.setTime(original);
                timestamp = newStamp;
            }
        }
        fmt.format(" %1$tY-%1$tm-%1$td", timestamp);
        if(timestamp.isSet(Calendar.HOUR_OF_DAY))
        {
            fmt.format(" %1$tH", timestamp);
            if(timestamp.isSet(Calendar.MINUTE))
            {
                fmt.format(":%1$tM", timestamp);
                if(timestamp.isSet(Calendar.SECOND))
                {
                    fmt.format(":%1$tS", timestamp);
                    if(timestamp.isSet(Calendar.MILLISECOND))
                    {
                        fmt.format(".%1$tL", timestamp);
                    }
                }
            }
            fmt.format(" %1$tZ", timestamp);
        }
    }
    sb.append(' ');
    sb.append(getName());
    fmt.close();
    return sb.toString();
}

private char
    formatType()
{
    switch(_type)
    {
    case FILE_TYPE:
        return '-';
    case DIRECTORY_TYPE:
        return 'd';
    case SYMBOLIC_LINK_TYPE:
        return 'l';
    default:
        return '?';
    }
}
private String
    permissionToString(int access)
{
    StringBuilder sb = new StringBuilder();
    if(hasPermission(access, READ_PERMISSION))
    {
        sb.append('r');
    }
    else
    {
        sb.append('-');
    }
    if(hasPermission(access, WRITE_PERMISSION))
    {
        sb.append('w');
    }
    else
    {
        sb.append('-');
    }
    if(hasPermission(access, EXECUTE_PERMISSION))
    {
        sb.append('x');
    }
    else
    {
        sb.append('-');
    }
    return sb.toString();
}
}
