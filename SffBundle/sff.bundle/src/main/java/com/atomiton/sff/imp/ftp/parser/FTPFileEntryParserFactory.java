package com.atomiton.sff.imp.ftp.parser;

import com.atomiton.sff.imp.ftp.FTPClientConfig;
import com.atomiton.sff.imp.ftp.FTPFileEntryParser;

public interface FTPFileEntryParserFactory
{
public FTPFileEntryParser
    createFileEntryParser(String key)
    throws ParserInitializationException;
public FTPFileEntryParser
    createFileEntryParser(FTPClientConfig config)
    throws ParserInitializationException;
}
