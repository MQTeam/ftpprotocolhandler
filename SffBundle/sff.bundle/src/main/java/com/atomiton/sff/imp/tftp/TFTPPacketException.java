package com.atomiton.sff.imp.tftp;

public class TFTPPacketException
extends
    Exception
{

private static final long serialVersionUID = -8114699256840851439L;
public TFTPPacketException()
{
    super();
}
public TFTPPacketException(String message)
{
    super(message);
}
}
