/*************************************************************************
 * 
 * ATOMITON CONFIDENTIAL __________________
 * 
 * [2013] - [2016] ATOMITON Incorporated All Rights Reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of Atomiton Incorporated and its suppliers, if any. The intellectual and technical
 * concepts contained herein are proprietary to Atomiton Incorporated and its suppliers and may be covered by U.S. and Foreign Patents, patents in process, and
 * are protected by trade secret or copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from Atomiton Incorporated.
 */
package com.atomiton.sff.imp.netty.ftp;

import static com.atomiton.sff.api.SffKeywordApi.val_close;
import static com.atomiton.sff.imp.netty.NettyUtils.newNullChannel;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.*;
import org.xml.sax.SAXException;

import com.atomiton.sff.api.SffComponentApi;
import com.atomiton.sff.imp.base.SffDataMap;
import com.atomiton.sff.imp.base.SffStringWrapper;
import com.atomiton.sff.imp.base.SffTextMessage;
import com.atomiton.sff.imp.netty.NettyChannelHandler;
import com.atomiton.sff.imp.netty.NettyTransport.NettyPipeline;
import com.atomiton.sff.imp.netty.NettyUtils.NullChannel;

import oda.common.Obj;
import oda.lm.ListMap;

/*******************************************************************************
 * 
 * @author Rakesh Kaim
 */
public class SffFtpHandler
extends
    NettyChannelHandler
implements
    ChannelFutureListener
{
// ==============================================================================
public SffFtpHandler(ListMap args)
throws Exception
{
    super((SffComponentApi)args.get(tag_Component));
    lmArgs = args;
}
// ==============================================================================
protected ListMap lmArgs;
protected ChannelHandlerContext chCntx;
protected NullChannel ftpChannel;
// ==============================================================================
@Override
public void
    afterAdd(ChannelHandlerContext chc)
{
    System.out.println("FTP: Inside after add");
    chCntx = chc;
    NettyPipeline pipeline = nettyPipeline(chc);
    Channel channel = pipeline.getChannel();
    if(channel == null)
        channel = newNullChannel(pipeline);
    channel.getCloseFuture().addListener(this);
    ftpChannel = (NullChannel)channel;
    pipeline.getResolvedFacet().facetInst.put(tag_Context, val_close);
    connect(lmArgs);
    super.afterAdd(chc);
}
// ==============================================================================
protected String ClientID;
protected String HostName;
protected String Port;
protected String UserName;
protected String Password;
protected String RemotePath;
protected String LocalPath;
protected String CommandName;

protected boolean MetadataInvoke = false;
public static final String tag_ClientID = "ClientID";
public static final String tag_HostName = "HostName";
public static final String tag_Port = "Port";
public static final String tag_UserName = "UserName";
public static final String tag_Password = "Password";
public static final String tag_RemotePath = "RemotePath";
public static final String tag_LocalPath = "LocalPath";
public static final String tag_CommandName = "CommandName";

public static final String def_ClientID = null;
public static final String def_HostName = null;
public static final String def_RemotePath = null;
public static final String def_LocalPath = null;
public static final String def_Password = null;
public static final String def_UserName = null;
public static final String def_CommandName = null;
// ==============================================================================
public SffDataMap
    getConnArgument(String argName, String argType, String Default, String Required)
{
    SffDataMap ConnArgument = SffDataMap.newInstance();
    ConnArgument.add("Name", argName);
    ConnArgument.add("Type", argType);
    ConnArgument.add("Default", Default);
    ConnArgument.add("Required", Required);
    return ConnArgument;
}
// ==============================================================================
public SffDataMap
    getResponseArgument(String argName, String argType, String format)
{
    SffDataMap RespArgument = SffDataMap.newInstance();
    RespArgument.add("Name", argName);
    RespArgument.add("Type", argType);
    RespArgument.add("Format", format);
    return RespArgument;
}
// ==============================================================================
public SffDataMap
    getRequestArgument(String argName, String argType, String format)
{
    SffDataMap ReqArgument = SffDataMap.newInstance();
    ReqArgument.add("Name", argName);
    ReqArgument.add("Type", argType);
    ReqArgument.add("Format", format);
    return ReqArgument;
}
// ==============================================================================
public SffDataMap
    constructPDL()
{
    SffDataMap ret = SffDataMap.newInstance();
    ret.put("ShortName", "FTP");
    ret.put("Description", "FTP is a standard network protocol used for the transfer of"
                           + " computer files between a client and server on a computer network.");
    ret.put("GetURL", "ftp://");
    ret.put("PutURL", "ftp://");

    SffDataMap ProtocolHandlerParams = SffDataMap.newInstance();
    SffDataMap ConnStructure = SffDataMap.newInstance();

    ConnStructure.add("ConnArgument", getConnArgument(tag_ClientID, "String", null, "Yes"));
    ConnStructure.add("ConnArgument", getConnArgument(tag_HostName, "String", null, "Yes"));
    ConnStructure.add("ConnArgument", getConnArgument(tag_Port, "String", "", "Optional"));
    ConnStructure.add("ConnArgument", getConnArgument(tag_UserName, "String", null, "Yes"));
    ConnStructure.add("ConnArgument", getConnArgument(tag_Password, "String", null, "Yes"));
    ConnStructure.add("ConnArgument", getConnArgument(tag_RemotePath, "String", null, "Yes"));
    ConnStructure.add("ConnArgument", getConnArgument(tag_LocalPath, "String", null, "Yes"));
    ConnStructure.add("ConnArgument", getConnArgument(tag_CommandName, "String", null, "Yes"));
    ProtocolHandlerParams.put("ConnStructure", ConnStructure);

    SffDataMap OperationModes = SffDataMap.newInstance();

    SffDataMap Mode = SffDataMap.newInstance();

    Mode.add("ModeName", "Sensor");
    Mode.add("InvokeMethod", "Get");
    Mode.add("AutoTrigger", "Yes");
    Mode.add("RequestStructure", null);

    Mode.add("ResponseStructure", getResponseArgument("SubscriptionPayload", "String", "xml"));
    OperationModes.add("Mode", Mode);

    Mode = null;
    Mode = SffDataMap.newInstance();

    Mode.add("ModeName", "Device");
    Mode.add("InvokeMethod", "Put");
    Mode.add("AutoTrigger", "Yes");
    Mode.add("RequestStructure", getRequestArgument("Payload", "String", "none"));
    Mode.add("ResponseStructure", null);
    OperationModes.add("Mode", Mode);

    ProtocolHandlerParams.put("OperationModes", OperationModes);
    ProtocolHandlerParams.put("ProtocolHandlerParamTypes", null);
    ret.put("ProtocolHandlerParams", ProtocolHandlerParams);
    ret = messageArgs("ProtocolStructure", ret);
    return ret;
}
// ==============================================================================
public ListMap
    convertURlToListMap(String url)
{
    ListMap args = null;
    if(url.indexOf('?') < 0)
    {
        return args;
    }
    url = url.substring(url.indexOf('?') + 1);
    url = url.trim().replaceAll(" +", " ");
    if(url == null || url.equalsIgnoreCase(""))
    {
        return args;
    }
    try
    {
        args = oda.common.Xml.lmXml("");
    }
    catch(IOException e)
    {
        e.printStackTrace();
    }
    catch(SAXException e)
    {
        e.printStackTrace();
    }
    String[] arr = url.split("&");
    for(String attribute : arr)
    {
        String[] keyValue = attribute.split("=", 2);
        if(keyValue != null && keyValue.length >= 2)
            args.put(keyValue[0].trim(), keyValue[1].trim());
        else if(keyValue != null && keyValue.length == 1)
            args.put(keyValue[0].trim(), "");
    }

    try
    {
        throw new RuntimeException("URL " + url);
    }
    catch(Exception exc)
    {
        exc.printStackTrace();
    }

    return args;
}
// ==============================================================================
private void
    emptyCheck()
{
    if(HostName.equals(""))
        HostName = null;
    if(UserName.equals(""))
        UserName = null;
    if(Password.equals(""))
        Password = null;
    if(RemotePath.equals(""))
        RemotePath = null;
    if(LocalPath.equals(""))
        LocalPath = null;
    if(CommandName.equals(""))
        CommandName = null;
}
// ==============================================================================
private void
    printInputParameters()
{
    System.out.println("   FTP:   Input Paramaters"
                       + " ClintID = " + ClientID
                       + " HostName = " + HostName
                       + " Port = " + Port
                       + " UserName = " + UserName
                       + " Password = " + Password
                       + " RemotePath = " + RemotePath
                       + " LocalPath = " + LocalPath
                       + " CommandName = " + CommandName);
}
// ==============================================================================
private void
    setParameters(ListMap parameterMap)
{
    ClientID = strVal(parameterMap, tag_ClientID, "");
    HostName = strVal(parameterMap, tag_HostName, def_HostName);
    Port = strVal(parameterMap, tag_Port, "");
    UserName = strVal(parameterMap, tag_UserName, def_UserName);
    Password = strVal(parameterMap, tag_Password, def_Password);
    RemotePath = strVal(parameterMap, tag_RemotePath, def_RemotePath);
    LocalPath = strVal(parameterMap, tag_LocalPath, def_LocalPath);
    CommandName = strVal(parameterMap, tag_CommandName, def_CommandName);
}
// ==============================================================================
protected void
    connect(ListMap args)
{
    System.out.println("FTP : Inside connect");
    String url = null;

    if(args.containsKey("Get"))
    {
        url = (String)args.get("Get");
        System.out.println("FTP : Inside connect URL = " + url);
    }
    else if(args.containsKey("Put"))
    {
        url = (String)args.get("Put");
        System.out.println("FTP : Inside connect URL = " + url);
    }
    else
    {
        System.out.println("FTP Error: Invoke Type neither Get nor Put = " + url);
        return;
    }

    ListMap params = convertURlToListMap(url);
    setParameters(params);
    printInputParameters();
    emptyCheck();

    try
    {
        FTPClientImpl ftpImpl = FTPClientImpl.getInstance();

        boolean isRegisterSucess = ftpImpl.register(ClientID, HostName, Port,
                                                    UserName, Password, RemotePath, LocalPath);

        if(!isRegisterSucess)
            return;

        if(CommandName.equalsIgnoreCase("get"))
        {
            System.out.println("FTP : Inside Get Command");
            ftpImpl.ftpClientConfigure(ClientID);
            ftpImpl.ftpOpen(ClientID);
            ftpImpl.ftpLogin(ClientID);
            ftpImpl.ftpGet(ClientID);
            ftpImpl.ftpLogout(ClientID);
            ftpImpl.deregister(ClientID);
            
            return;
        }
        else if(CommandName.equalsIgnoreCase("put"))
        {
            System.out.println("FTP : Inside Put Command");
            ftpImpl.ftpClientConfigure(ClientID);
            ftpImpl.ftpOpen(ClientID);
            ftpImpl.ftpLogin(ClientID);
            ftpImpl.ftpPut(ClientID);
            ftpImpl.ftpLogout(ClientID);
            ftpImpl.deregister(ClientID);
            
            return;
        }
        else
        {
            System.out.println("FTP Error: Command Name is neither Get nor Put Or Some mandatory Parameters are found NULL");
            return;
        }

    }
    catch(Exception exc)
    {
        throw new RuntimeException(exc);
    }
}
// ==============================================================================
protected SffDataMap
    messageArgs(Object... args)
{
    SffDataMap res = SffDataMap.newInstance();
    SffDataMap msg = res.instanceGet(tag_Message);
    msg.put(tag_Type, val_xml, isAttribute_TRUE);
    SffDataMap val = msg.instanceGet(tag_Value);
    val.addPairs(args);
    return res;
}
// ==============================================================================
@Override
public void
    writeRequested(ChannelHandlerContext ctx, MessageEvent mev)
    throws Exception
{

    System.out.println("FTP Handler : Write Requested.");

    if(MetadataInvoke)
    {
        try
        {
            messageReceived(chCntx, new UpstreamMessageEvent(ftpChannel, constructPDL(), null));
            return;
        }
        catch(Exception exc)
        {
            throw new RuntimeException(exc);
        }
    }

    Object msg = mev.getMessage();
    if(msg instanceof String)
    {
        msg = (String)msg;
    }
    else if(msg instanceof SffStringWrapper)
    {
        msg = ((SffStringWrapper)msg).unwrap();
    }
    else if(msg instanceof ListMap)
    {
        ListMap lmsg = (ListMap)msg;
        if(lmsg.containsKey("Message"))
        {
            msg = ((ListMap)lmsg.get("Message")).get("Value");
        }
    }
    else
    {
        throw new IllegalArgumentException("Unsupported outbound message type: " + msg.getClass().getName());
    }
    Object writeResponse = new SffTextMessage((String)"Success", null, getDefaults(chCntx, true));
    try
    {
        messageReceived(chCntx, new UpstreamMessageEvent(ftpChannel, writeResponse, null));
    }
    catch(Exception exc)
    {
        throw new RuntimeException(exc);
    }
}
// ================================================================================
@Override
public void
    operationComplete(ChannelFuture arg0)
    throws Exception
{
    System.out.println("FTP Handler : Closing the client");
    lmArgs = null;
    if(!ftpChannel.isClosing())
        ftpChannel.close();
}
// ================================================================================
protected static final Map<String, Object> valConfig;
static
{
    Map<String, Object> val = new HashMap<>();
    valConfig = Collections.unmodifiableMap(val);
}
protected Map<String, Object> phidgitConfig = new HashMap<>();
private Charset defCharset;
// ===============================================================================
protected String
    valTag(ListMap args, String tag)
{
    Object val = args.getIgnoreCase(tag);
    if(!(val instanceof String))
        return null;
    phidgitConfig.put(tag, val);
    return (String)val;
}
// ==============================================================================
protected Object
    objVal(String arg, Object def)
{
    if(arg == null
       || arg.length() == 0)
        return def;
    Object val = valConfig.get(arg.toLowerCase());
    if(val == null)
        val = arg;
    return val;
}
// ==============================================================================
protected String
    strVal(String arg, Object def)
{
    return Obj.stringValue(objVal(arg, def));
}
// ==============================================================================
protected String
    strVal(ListMap args, String tag, Object def)
{
    return strVal(valTag(args, tag), def);
}
// ==============================================================================
protected int
    intVal(String arg, Object def)
{
    return Obj.intValue(objVal(arg, def));
}
// ==============================================================================
protected int
    intVal(ListMap args, String tag, Object def)
{
    return intVal(valTag(args, tag), def);
}
// ==============================================================================
public void
    connectionLost(Throwable arg0)
{

}
/* public void deliveryComplete(IMqttDeliveryToken arg0) { } */

@Override
public void
messageReceived(ChannelHandlerContext chc, MessageEvent mev)
throws Exception
{
    System.out.println("Message Received");
    Object msg = mev.getMessage();

    if(msg instanceof SffTextMessage)
    {
        msg = ((SffTextMessage)msg).toString();
        System.out.println("SffTextMessage is " + (String)msg);
    }
    else if(msg instanceof String)
    {
        System.out.println("String Received is " + (String)msg);
    }
    else if(msg instanceof ChannelBuffer)
    {
        msg = ((ChannelBuffer)msg).toString(defCharset);
        System.out.println("ChannelBuffer is " + msg);
    }
    else
    {
        super.messageReceived(chc, mev);
        return;
    }
    msg = messageArgs(msg);
    super.messageReceived(chc, new UpstreamMessageEvent(mev.getChannel(), msg, mev.getRemoteAddress()));
    System.out.println("Before receiving response message");
}
}// SffFtpHandler
 // ******************************************************************************
