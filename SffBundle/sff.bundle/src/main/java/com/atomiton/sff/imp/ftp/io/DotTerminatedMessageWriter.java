package com.atomiton.sff.imp.ftp.io;

import java.io.IOException;
import java.io.Writer;

public final class DotTerminatedMessageWriter
extends
    Writer
{
private static final int __NOTHING_SPECIAL_STATE = 0;
private static final int __LAST_WAS_CR_STATE = 1;
private static final int __LAST_WAS_NL_STATE = 2;

private int __state;
private Writer __output;
public DotTerminatedMessageWriter(Writer output)
{
    super(output);
    __output = output;
    __state = __NOTHING_SPECIAL_STATE;
}
@Override
public void
    write(int ch)
    throws IOException
{
    synchronized(lock)
    {
        switch(ch)
        {
        case '\r':
            __state = __LAST_WAS_CR_STATE;
            __output.write('\r');
            return;
        case '\n':
            if(__state != __LAST_WAS_CR_STATE)
            {
                __output.write('\r');
            }
            __output.write('\n');
            __state = __LAST_WAS_NL_STATE;
            return;
        case '.':
            if(__state == __LAST_WAS_NL_STATE)
            {
                __output.write('.');
            }
        default:
            __state = __NOTHING_SPECIAL_STATE;
            __output.write(ch);
            return;
        }
    }
}
@Override
public void
    write(char[] buffer, int offset, int length)
    throws IOException
{
    synchronized(lock)
    {
        while(length-- > 0)
        {
            write(buffer[offset++]);
        }
    }
}

@Override
public void
    write(char[] buffer)
    throws IOException
{
    write(buffer, 0, buffer.length);
}
@Override
public void
    write(String string)
    throws IOException
{
    write(string.toCharArray());
}
@Override
public void
    write(String string, int offset, int length)
    throws IOException
{
    write(string.toCharArray(), offset, length);
}
@Override
public void
    flush()
    throws IOException
{
    synchronized(lock)
    {
        __output.flush();
    }
}
@Override
public void
    close()
    throws IOException
{
    synchronized(lock)
    {
        if(__output == null)
        {
            return;
        }

        if(__state == __LAST_WAS_CR_STATE)
        {
            __output.write('\n');
        }
        else if(__state != __LAST_WAS_NL_STATE)
        {
            __output.write("\r\n");
        }

        __output.write(".\r\n");

        __output.flush();
        __output = null;
    }
}

}
