package com.atomiton.sff.imp.main;

import java.io.*;
import java.net.SocketException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atomiton.sff.imp.commons.PrintCommandListener;
import com.atomiton.sff.imp.ftp.FTP;
import com.atomiton.sff.imp.ftp.FTPClient;
import com.atomiton.sff.imp.ftp.FTPFile;

public class FTPClientDemo
{
private static final Logger logger = LoggerFactory.getLogger(FTPClientDemo.class);

private static FTPClient ftp;

public static void
    main(String[] args)
    throws SocketException,
    IOException
{
    connectGNUFtpOpenCloseDemo();
    connectGNUFtpOpenListDirectoryCloseDemo();
    connectGNUFtpOpenGetCloseDemo();
    connectLocalhostOpenListDirectoryCloseDemo();
    connectLocalhostOpenCloseDemo();
    connectLocalhostOpenGetCloseDemo();
    connectLocalhostOpenPutCloseDemo();
}

public static void
    connectLocalhostOpenGetCloseDemo()
    throws SocketException,
    IOException
{
    logger.info("Localhost FTP GET Demo");
    init();
    configure();
    open("localhost");
    login(new User("test", "Atomiton@tql"));
    get("testftp.txt", "/home/test/ftp_upload/out_box/ftp_get_test.txt");
    close();
    distroy();
}
public static void
    connectLocalhostOpenListDirectoryCloseDemo()
    throws SocketException,
    IOException
{
    logger.info("Localhost FTP Server List Directory Demo");
    init();
    configure();
    open("localhost");
    login(new User("test", "Atomiton@tql"));
    list("/");
    close();
    distroy();
}
public static void
    connectLocalhostOpenPutCloseDemo()
    throws SocketException,
    IOException
{
    logger.info("FTP PUT Demo");
    init();
    configure();
    open("localhost");
    login(new User("test", "Atomiton@tql"));
    put("testftp.txt", "/home/test/ftp_upload/in_box/ftp_put_test.txt");
    close();
    distroy();
}
public static void
    connectLocalhostOpenCloseDemo()
    throws SocketException,
    IOException
{
    logger.info("FTP Open Close Demo");
    init();
    configure();
    open("localhost");
    login(new User("test", "Atomiton@tql"));
    close();
    distroy();
}
public static void
    connectGNUFtpOpenCloseDemo()
    throws SocketException,
    IOException
{
    logger.info("GNU FTP Open Close Demo");
    init();
    configure();
    open("ftp.gnu.org");
    login(new User("anonymous", ""));
    close();
    distroy();
}
public static void
    connectGNUFtpOpenGetCloseDemo()
    throws SocketException,
    IOException
{
    logger.info("GNU FTP Server GET Demo");
    init();
    configure();
    open("ftp.gnu.org");
    login(new User("anonymous", ""));
    get("c-graph-2.0.tar.gz", "/gnu/c-graph/c-graph-2.0.tar.gz");
    close();
    distroy();
}

public static void
    connectGNUFtpOpenListDirectoryCloseDemo()
    throws SocketException,
    IOException
{
    logger.info("GNU FTP Server List Directory Demo");
    init();
    configure();
    open("ftp.gnu.org");
    login(new User("anonymous", ""));
    list("/");
    close();
    distroy();
}
public static void
    list(String remote)
    throws IOException
{
    for(FTPFile f : ftp.listFiles(remote))
        System.out.println(f.getRawListing());
}
public static void
    get(String local, String remote)
    throws IOException
{
    ftp.setFileType(FTP.ASCII_FILE_TYPE);
    OutputStream output;
    output = new FileOutputStream(local);
    ftp.retrieveFile(remote, output);
    output.close();
}
public static void
    put(String local, String remote)
    throws IOException
{
    ftp.setFileType(FTP.ASCII_FILE_TYPE);
    InputStream input;
    input = new FileInputStream(local);
    ftp.storeFile(remote, input);
    input.close();
}
public static void
    open(String hostName)
    throws SocketException,
    IOException
{
    ftp.connect(hostName);
}

public static void
    login(User user)
    throws IOException
{
    ftp.login(user.getName(), user.getPassword());
}

public static void
    close()
    throws IOException
{
    ftp.noop();
    ftp.logout();
    ftp.disconnect();
}

public static void
    configure()
{
    ftp.addProtocolCommandListener(new PrintCommandListener(System.out, true));
}

public static void
    init()
{
    ftp = new FTPClient();
}

public static void
    distroy()
{
    ftp = null;
}
}
