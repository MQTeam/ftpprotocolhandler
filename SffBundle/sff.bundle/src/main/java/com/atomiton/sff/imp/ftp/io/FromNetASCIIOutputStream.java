package com.atomiton.sff.imp.ftp.io;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public final class FromNetASCIIOutputStream
extends
    FilterOutputStream
{
private boolean __lastWasCR;
public FromNetASCIIOutputStream(OutputStream output)
{
    super(output);
    __lastWasCR = false;
}
private void
    __write(int ch)
    throws IOException
{
    switch(ch)
    {
    case '\r':
        __lastWasCR = true;
        break;
    case '\n':
        if(__lastWasCR)
        {
            out.write(FromNetASCIIInputStream._lineSeparatorBytes);
            __lastWasCR = false;
            break;
        }
        __lastWasCR = false;
        out.write('\n');
        break;
    default:
        if(__lastWasCR)
        {
            out.write('\r');
            __lastWasCR = false;
        }
        out.write(ch);
        break;
    }
}
@Override
public synchronized void
    write(int ch)
    throws IOException
{
    if(FromNetASCIIInputStream._noConversionRequired)
    {
        out.write(ch);
        return;
    }

    __write(ch);
}
@Override
public synchronized void
    write(byte buffer[])
    throws IOException
{
    write(buffer, 0, buffer.length);
}
@Override
public synchronized void
    write(byte buffer[], int offset, int length)
    throws IOException
{
    if(FromNetASCIIInputStream._noConversionRequired)
    {
        out.write(buffer, offset, length);
        return;
    }

    while(length-- > 0)
    {
        __write(buffer[offset++]);
    }
}

@Override
public synchronized void
    close()
    throws IOException
{
    if(FromNetASCIIInputStream._noConversionRequired)
    {
        super.close();
        return;
    }

    if(__lastWasCR)
    {
        out.write('\r');
    }
    super.close();
}
}
