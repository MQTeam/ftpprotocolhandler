package com.atomiton.sff.imp.main;

import com.atomiton.sff.imp.netty.ftp.FTPClientImpl;

public class FTPClientImplDemo
{
    private static String ClientID1 = "LocalClient";
    private static String ClientID2 = "GnuClient";
    public static void
        main(String[] args)
    {
        FTPClientImpl client = FTPClientImpl.getInstance();
        
        client.register(ClientID1, "localhost", "21", "test","Atomiton@tql", "/home/test/ftp_upload/out_box/ftp_get_test.txt", "ftp_get_test.txt");
        client.ftpClientConfigure(ClientID1);
        client.ftpOpen(ClientID1);
        client.ftpLogin(ClientID1);
        client.register(ClientID2, "ftp.gnu.org", "21", "anonymous", "", "/", "c-graph-2.0.tar.gz");
        //client.register(ClientID2, "ftp.gnu.org", "21", "anonymous", "", "/gnu/c-graph/c-graph-2.0.tar.gz", "c-graph-2.0.tar.gz");
        client.ftpClientConfigure(ClientID2);
        client.ftpOpen(ClientID2);
        client.ftpLogin(ClientID2);
        
        //client.ftpListFiles(ClientID2);
        
        //client.ftpGet(ClientID2);
        client.ftpGet(ClientID1);
        
        client.ftpListFiles(ClientID2);
        client.ftpLogout(ClientID1);
        client.ftpLogout(ClientID2);
        client.deregister(ClientID2);
        client.deregister(ClientID1);
    }
}
