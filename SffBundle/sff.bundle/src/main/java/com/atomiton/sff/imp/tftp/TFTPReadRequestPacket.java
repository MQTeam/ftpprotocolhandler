package com.atomiton.sff.imp.tftp;

import java.net.DatagramPacket;
import java.net.InetAddress;

public final class TFTPReadRequestPacket
extends
    TFTPRequestPacket
{
public TFTPReadRequestPacket(InetAddress destination, int port,
                             String filename, int mode)
{
    super(destination, port, TFTPPacket.READ_REQUEST, filename, mode);
}
TFTPReadRequestPacket(DatagramPacket datagram)
throws TFTPPacketException
{
    super(TFTPPacket.READ_REQUEST, datagram);
}
@Override
public String
    toString()
{
    return super.toString() + " RRQ " + getFilename() + " " + TFTP.getModeName(getMode());
}
}
