package com.atomiton.sff.imp.ftp.io;

import java.util.EventObject;

public class CopyStreamEvent
extends
    EventObject
{
private static final long serialVersionUID = -964927635655051867L;
public static final long UNKNOWN_STREAM_SIZE = -1;
private final int bytesTransferred;
private final long totalBytesTransferred;
private final long streamSize;
public CopyStreamEvent(Object source, long totalBytesTransferred,
                       int bytesTransferred, long streamSize)
{
    super(source);
    this.bytesTransferred = bytesTransferred;
    this.totalBytesTransferred = totalBytesTransferred;
    this.streamSize = streamSize;
}
public int
    getBytesTransferred()
{
    return bytesTransferred;
}
public long
    getTotalBytesTransferred()
{
    return totalBytesTransferred;
}
public long
    getStreamSize()
{
    return streamSize;
}
@Override
public String
    toString()
{
    return getClass().getName() + "[source=" + source
           + ", total=" + totalBytesTransferred
           + ", bytes=" + bytesTransferred
           + ", size=" + streamSize
           + "]";
}
}
