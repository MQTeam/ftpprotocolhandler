package com.atomiton.sff.imp.ftp.parser;

import java.text.ParseException;

import com.atomiton.sff.imp.ftp.FTPClientConfig;
import com.atomiton.sff.imp.ftp.FTPFile;

public class MacOsPeterFTPEntryParser
extends
    ConfigurableFTPFileEntryParserImpl
{
static final String DEFAULT_DATE_FORMAT = "MMM d yyyy";

static final String DEFAULT_RECENT_DATE_FORMAT = "MMM d HH:mm";
private static final String REGEX = "([bcdelfmpSs-])"
                                    + "(((r|-)(w|-)([xsStTL-]))((r|-)(w|-)([xsStTL-]))((r|-)(w|-)([xsStTL-])))\\+?\\s+"
                                    + "("
                                    + "(folder\\s+)"
                                    + "|"
                                    + "((\\d+)\\s+(\\d+)\\s+)"
                                    + ")"
                                    + "(\\d+)\\s+"

                                    + "((?:\\d+[-/]\\d+[-/]\\d+)|(?:\\S{3}\\s+\\d{1,2})|(?:\\d{1,2}\\s+\\S{3}))\\s+"

                                    + "(\\d+(?::\\d+)?)\\s+"

                                    + "(\\S*)(\\s*.*)";
public MacOsPeterFTPEntryParser()
{
    this(null);
}
public MacOsPeterFTPEntryParser(FTPClientConfig config)
{
    super(REGEX);
    configure(config);
}
@Override
public FTPFile
    parseFTPEntry(String entry)
{
    FTPFile file = new FTPFile();
    file.setRawListing(entry);
    int type;
    boolean isDevice = false;

    if(matches(entry))
    {
        String typeStr = group(1);
        String hardLinkCount = "0";
        String usr = null;
        String grp = null;
        String filesize = group(20);
        String datestr = group(21) + " " + group(22);
        String name = group(23);
        String endtoken = group(24);

        try
        {
            file.setTimestamp(super.parseTimestamp(datestr));
        }
        catch(ParseException e)
        {
        }
        switch(typeStr.charAt(0))
        {
        case 'd':
            type = FTPFile.DIRECTORY_TYPE;
            break;
        case 'e':
            type = FTPFile.SYMBOLIC_LINK_TYPE;
            break;
        case 'l':
            type = FTPFile.SYMBOLIC_LINK_TYPE;
            break;
        case 'b':
        case 'c':
            isDevice = true;
            type = FTPFile.FILE_TYPE;
            break;
        case 'f':
        case '-':
            type = FTPFile.FILE_TYPE;
            break;
        default:
            type = FTPFile.UNKNOWN_TYPE;
        }

        file.setType(type);

        int g = 4;
        for(int access = 0; access < 3; access++, g += 4)
        {

            file.setPermission(access, FTPFile.READ_PERMISSION,
                               (!group(g).equals("-")));
            file.setPermission(access, FTPFile.WRITE_PERMISSION,
                               (!group(g + 1).equals("-")));

            String execPerm = group(g + 2);
            if(!execPerm.equals("-") && !Character.isUpperCase(execPerm.charAt(0)))
            {
                file.setPermission(access, FTPFile.EXECUTE_PERMISSION, true);
            }
            else
            {
                file.setPermission(access, FTPFile.EXECUTE_PERMISSION, false);
            }
        }

        if(!isDevice)
        {
            try
            {
                file.setHardLinkCount(Integer.parseInt(hardLinkCount));
            }
            catch(NumberFormatException e)
            {
            }
        }

        file.setUser(usr);
        file.setGroup(grp);

        try
        {
            file.setSize(Long.parseLong(filesize));
        }
        catch(NumberFormatException e)
        {
        }

        if(null == endtoken)
        {
            file.setName(name);
        }
        else
        {
            name += endtoken;
            if(type == FTPFile.SYMBOLIC_LINK_TYPE)
            {

                int end = name.indexOf(" -> ");
                if(end == -1)
                {
                    file.setName(name);
                }
                else
                {
                    file.setName(name.substring(0, end));
                    file.setLink(name.substring(end + 4));
                }

            }
            else
            {
                file.setName(name);
            }
        }
        return file;
    }
    return null;
}
@Override
protected FTPClientConfig
    getDefaultConfiguration()
{
    return new FTPClientConfig(
                               FTPClientConfig.SYST_UNIX,
                               DEFAULT_DATE_FORMAT,
                               DEFAULT_RECENT_DATE_FORMAT);
}
}
