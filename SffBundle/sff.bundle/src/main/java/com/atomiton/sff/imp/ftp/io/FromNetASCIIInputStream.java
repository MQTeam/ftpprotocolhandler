package com.atomiton.sff.imp.ftp.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.io.UnsupportedEncodingException;

public final class FromNetASCIIInputStream
extends
    PushbackInputStream
{
static final boolean _noConversionRequired;
static final String _lineSeparator;
static final byte[] _lineSeparatorBytes;

static
{
    _lineSeparator = System.getProperty("line.separator");
    _noConversionRequired = _lineSeparator.equals("\r\n");
    try
    {
        _lineSeparatorBytes = _lineSeparator.getBytes("US-ASCII");
    }
    catch(UnsupportedEncodingException e)
    {
        throw new RuntimeException("Broken JVM - cannot find US-ASCII charset!", e);
    }
}

private int __length = 0;
public static final boolean
    isConversionRequired()
{
    return !_noConversionRequired;
}
public FromNetASCIIInputStream(InputStream input)
{
    super(input, _lineSeparatorBytes.length + 1);
}

private int
    __read()
    throws IOException
{
    int ch;

    ch = super.read();

    if(ch == '\r')
    {
        ch = super.read();
        if(ch == '\n')
        {
            unread(_lineSeparatorBytes);
            ch = super.read();
            --__length;
        }
        else
        {
            if(ch != -1)
            {
                unread(ch);
            }
            return '\r';
        }
    }

    return ch;
}

@Override
public int
    read()
    throws IOException
{
    if(_noConversionRequired)
    {
        return super.read();
    }

    return __read();
}
@Override
public int
    read(byte buffer[])
    throws IOException
{
    return read(buffer, 0, buffer.length);
}
@Override
public int
    read(byte buffer[], int offset, int length)
    throws IOException
{
    if(_noConversionRequired)
    {
        return super.read(buffer, offset, length);
    }

    if(length < 1)
    {
        return 0;
    }

    int ch, off;

    ch = available();

    __length = (length > ch ? ch : length);
    if(__length < 1)
    {
        __length = 1;
    }

    if((ch = __read()) == -1)
    {
        return -1;
    }

    off = offset;

    do
    {
        buffer[offset++] = (byte)ch;
    }
    while(--__length > 0 && (ch = __read()) != -1);

    return(offset - off);
}
@Override
public int
    available()
    throws IOException
{
    if(in == null)
    {
        throw new IOException("Stream closed");
    }
    return (buf.length - pos) + in.available();
}
}
