package com.atomiton.sff.imp.tftp;

import java.net.DatagramPacket;
import java.net.InetAddress;

public final class TFTPWriteRequestPacket
extends
    TFTPRequestPacket
{
public TFTPWriteRequestPacket(InetAddress destination, int port,
                              String filename, int mode)
{
    super(destination, port, TFTPPacket.WRITE_REQUEST, filename, mode);
}
TFTPWriteRequestPacket(DatagramPacket datagram)
throws TFTPPacketException
{
    super(TFTPPacket.WRITE_REQUEST, datagram);
}
@Override
public String
    toString()
{
    return super.toString() + " WRQ " + getFilename() + " " + TFTP.getModeName(getMode());
}
}
