/*******************************************************************************
 * 
 * ATOMITON CONFIDENTIAL __________________
 * 
 * [2013] - [2016] ATOMITON Incorporated All Rights Reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of Atomiton Incorporated and its suppliers, if any. The intellectual and technical
 * concepts contained herein are proprietary to Atomiton Incorporated and its suppliers and may be covered by U.S. and Foreign Patents, patents in process, and
 * are protected by trade secret or copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from Atomiton Incorporated.
 */

package com.atomiton.sff.imp.ftp;

import java.io.Serializable;

public class InputParams implements Serializable
{
private static final long serialVersionUID = 5919532396346622597L;
private String hostName;
private String port;
private String userName;
private String password;
private String remotePath;
private String localPath;
public String
    getHostName()
{
    return hostName;
}
public void
    setHostName(String hostName)
{
    this.hostName = hostName;
}
public String
    getUserName()
{
    return userName;
}
public void
    setUserName(String userName)
{
    this.userName = userName;
}
public String
    getPassword()
{
    return password;
}
public void
    setPassword(String password)
{
    this.password = password;
}
public String
    getRemotePath()
{
    return remotePath;
}
public void
    setRemotePath(String remotePath)
{
    this.remotePath = remotePath;
}
public String
    getLocalPath()
{
    return localPath;
}
public void
    setLocalPath(String localPath)
{
    this.localPath = localPath;
}
/**
 * @return the port
 */
public String
    getPort()
{
    return port;
}
/**
 * @param port the port to set
 */
public void
    setPort(String port)
{
    this.port = port;
}
/* (non-Javadoc)
 * @see java.lang.Object#hashCode()
 */
@Override
public int
    hashCode()
{
    final int prime = 31;
    int result = 1;
    result = prime * result + ((hostName == null) ? 0 : hostName.hashCode());
    result = prime * result + ((localPath == null) ? 0 : localPath.hashCode());
    result = prime * result + ((password == null) ? 0 : password.hashCode());
    result = prime * result + ((port == null) ? 0 : port.hashCode());
    result = prime * result + ((remotePath == null) ? 0 : remotePath.hashCode());
    result = prime * result + ((userName == null) ? 0 : userName.hashCode());
    return result;
}
/* (non-Javadoc)
 * @see java.lang.Object#equals(java.lang.Object)
 */
@Override
public boolean
    equals(Object obj)
{
    if(this == obj)
        return true;
    if(obj == null)
        return false;
    if(!(obj instanceof InputParams))
        return false;
    InputParams other = (InputParams)obj;
    if(hostName == null)
    {
        if(other.hostName != null)
            return false;
    }
    else if(!hostName.equals(other.hostName))
        return false;
    if(localPath == null)
    {
        if(other.localPath != null)
            return false;
    }
    else if(!localPath.equals(other.localPath))
        return false;
    if(password == null)
    {
        if(other.password != null)
            return false;
    }
    else if(!password.equals(other.password))
        return false;
    if(port == null)
    {
        if(other.port != null)
            return false;
    }
    else if(!port.equals(other.port))
        return false;
    if(remotePath == null)
    {
        if(other.remotePath != null)
            return false;
    }
    else if(!remotePath.equals(other.remotePath))
        return false;
    if(userName == null)
    {
        if(other.userName != null)
            return false;
    }
    else if(!userName.equals(other.userName))
        return false;
    return true;
}
/* (non-Javadoc)
 * @see java.lang.Object#toString()
 */
@Override
public String
    toString()
{
    return "InputParams [hostName=" + hostName + ", port=" + port + ", userName=" + userName + ", password=" + password + ", remotePath=" + remotePath
           + ", localPath=" + localPath + "]";
}

}
