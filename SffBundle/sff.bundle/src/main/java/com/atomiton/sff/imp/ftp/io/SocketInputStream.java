package com.atomiton.sff.imp.ftp.io;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

public class SocketInputStream
extends
    FilterInputStream
{
private final Socket __socket;
public SocketInputStream(Socket socket, InputStream stream)
{
    super(stream);
    __socket = socket;
}
@Override
public void
    close()
    throws IOException
{
    super.close();
    __socket.close();
}
}
