package com.atomiton.sff.imp.ftp.io;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class ToNetASCIIInputStream
extends
    FilterInputStream
{
private static final int __NOTHING_SPECIAL = 0;
private static final int __LAST_WAS_CR = 1;
private static final int __LAST_WAS_NL = 2;
private int __status;
public ToNetASCIIInputStream(InputStream input)
{
    super(input);
    __status = __NOTHING_SPECIAL;
}
@Override
public int
    read()
    throws IOException
{
    int ch;

    if(__status == __LAST_WAS_NL)
    {
        __status = __NOTHING_SPECIAL;
        return '\n';
    }

    ch = in.read();

    switch(ch)
    {
    case '\r':
        __status = __LAST_WAS_CR;
        return '\r';
    case '\n':
        if(__status != __LAST_WAS_CR)
        {
            __status = __LAST_WAS_NL;
            return '\r';
        }
    default:
        __status = __NOTHING_SPECIAL;
        return ch;
    }
}

@Override
public int
    read(byte buffer[])
    throws IOException
{
    return read(buffer, 0, buffer.length);
}
@Override
public int
    read(byte buffer[], int offset, int length)
    throws IOException
{
    int ch, off;

    if(length < 1)
    {
        return 0;
    }

    ch = available();

    if(length > ch)
    {
        length = ch;
    }

    if(length < 1)
    {
        length = 1;
    }

    if((ch = read()) == -1)
    {
        return -1;
    }

    off = offset;

    do
    {
        buffer[offset++] = (byte)ch;
    }
    while(--length > 0 && (ch = read()) != -1);

    return(offset - off);
}

@Override
public boolean
    markSupported()
{
    return false;
}

@Override
public int
    available()
    throws IOException
{
    int result;

    result = in.available();

    if(__status == __LAST_WAS_NL)
    {
        return(result + 1);
    }

    return result;
}
}
