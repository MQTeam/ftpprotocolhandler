package com.atomiton.sff.imp.ftp;

import java.io.*;
import java.net.Socket;

import javax.net.ssl.*;

import com.atomiton.sff.imp.ftp.util.Base64;
import com.atomiton.sff.imp.ftp.util.SSLContextUtils;
import com.atomiton.sff.imp.ftp.util.SSLSocketUtils;
import com.atomiton.sff.imp.ftp.util.TrustManagerUtils;

public class FTPSClient
extends
    FTPClient
{

public static final int DEFAULT_FTPS_DATA_PORT = 989;
public static final int DEFAULT_FTPS_PORT = 990;
private static final String[] PROT_COMMAND_VALUE =
{"C", "E", "S", "P"};
private static final String DEFAULT_PROT = "C";
private static final String DEFAULT_PROTOCOL = "TLS";
private static final String CMD_AUTH = "AUTH";
private static final String CMD_ADAT = "ADAT";
private static final String CMD_PROT = "PROT";
private static final String CMD_PBSZ = "PBSZ";
private static final String CMD_MIC = "MIC";
private static final String CMD_CONF = "CONF";
private static final String CMD_ENC = "ENC";
private static final String CMD_CCC = "CCC";
private final boolean isImplicit;
private final String protocol;
private String auth = DEFAULT_PROTOCOL;
private SSLContext context;
private Socket plainSocket;
private boolean isCreation = true;
private boolean isClientMode = true;
private boolean isNeedClientAuth = false;
private boolean isWantClientAuth = false;
private String[] suites = null;
private String[] protocols = null;
private TrustManager trustManager = TrustManagerUtils.getValidateServerCertificateTrustManager();
private KeyManager keyManager = null;
private HostnameVerifier hostnameVerifier = null;
private boolean tlsEndpointChecking;
public FTPSClient()
{
    this(DEFAULT_PROTOCOL, false);
}
public FTPSClient(boolean isImplicit)
{
    this(DEFAULT_PROTOCOL, isImplicit);
}
public FTPSClient(String protocol)
{
    this(protocol, false);
}
public FTPSClient(String protocol, boolean isImplicit)
{
    super();
    this.protocol = protocol;
    this.isImplicit = isImplicit;
    if(isImplicit)
    {
        setDefaultPort(DEFAULT_FTPS_PORT);
    }
}
public FTPSClient(boolean isImplicit, SSLContext context)
{
    this(DEFAULT_PROTOCOL, isImplicit);
    this.context = context;
}
public FTPSClient(SSLContext context)
{
    this(false, context);
}
public void
    setAuthValue(String auth)
{
    this.auth = auth;
}
public String
    getAuthValue()
{
    return this.auth;
}
@Override
protected void
    _connectAction_()
    throws IOException
{
    if(isImplicit)
    {
        sslNegotiation();
    }
    super._connectAction_();
    if(!isImplicit)
    {
        execAUTH();
        sslNegotiation();
    }
}
protected void
    execAUTH()
    throws SSLException,
    IOException
{
    int replyCode = sendCommand(CMD_AUTH, auth);
    if(FTPReply.SECURITY_MECHANISM_IS_OK == replyCode)
    {
    }
    else if(FTPReply.SECURITY_DATA_EXCHANGE_COMPLETE != replyCode)
    {
        throw new SSLException(getReplyString());
    }
}
private void
    initSslContext()
    throws IOException
{
    if(context == null)
    {
        context = SSLContextUtils.createSSLContext(protocol, getKeyManager(), getTrustManager());
    }
}
protected void
    sslNegotiation()
    throws IOException
{
    plainSocket = _socket_;
    initSslContext();

    SSLSocketFactory ssf = context.getSocketFactory();
    String host = (_hostname_ != null) ? _hostname_ : getRemoteAddress().getHostAddress();
    int port = _socket_.getPort();
    SSLSocket socket = (SSLSocket)ssf.createSocket(_socket_, host, port, false);
    socket.setEnableSessionCreation(isCreation);
    socket.setUseClientMode(isClientMode);
    if(isClientMode)
    {
        if(tlsEndpointChecking)
        {
            SSLSocketUtils.enableEndpointNameVerification(socket);
        }
    }
    else
    {
        socket.setNeedClientAuth(isNeedClientAuth);
        socket.setWantClientAuth(isWantClientAuth);
    }

    if(protocols != null)
    {
        socket.setEnabledProtocols(protocols);
    }
    if(suites != null)
    {
        socket.setEnabledCipherSuites(suites);
    }
    socket.startHandshake();
    _socket_ = socket;
    _controlInput_ = new BufferedReader(new InputStreamReader(
                                                              socket.getInputStream(), getControlEncoding()));
    _controlOutput_ = new BufferedWriter(new OutputStreamWriter(
                                                                socket.getOutputStream(), getControlEncoding()));

    if(isClientMode)
    {
        if(hostnameVerifier != null && !hostnameVerifier.verify(host, socket.getSession()))
        {
            throw new SSLHandshakeException("Hostname doesn't match certificate");
        }
    }
}
private KeyManager
    getKeyManager()
{
    return keyManager;
}
public void
    setKeyManager(KeyManager keyManager)
{
    this.keyManager = keyManager;
}
public void
    setEnabledSessionCreation(boolean isCreation)
{
    this.isCreation = isCreation;
}
public boolean
    getEnableSessionCreation()
{
    if(_socket_ instanceof SSLSocket)
    {
        return ((SSLSocket)_socket_).getEnableSessionCreation();
    }
    return false;
}
public void
    setNeedClientAuth(boolean isNeedClientAuth)
{
    this.isNeedClientAuth = isNeedClientAuth;
}
public boolean
    getNeedClientAuth()
{
    if(_socket_ instanceof SSLSocket)
    {
        return ((SSLSocket)_socket_).getNeedClientAuth();
    }
    return false;
}
public void
    setWantClientAuth(boolean isWantClientAuth)
{
    this.isWantClientAuth = isWantClientAuth;
}
public boolean
    getWantClientAuth()
{
    if(_socket_ instanceof SSLSocket)
    {
        return ((SSLSocket)_socket_).getWantClientAuth();
    }
    return false;
}
public void
    setUseClientMode(boolean isClientMode)
{
    this.isClientMode = isClientMode;
}
public boolean
    getUseClientMode()
{
    if(_socket_ instanceof SSLSocket)
    {
        return ((SSLSocket)_socket_).getUseClientMode();
    }
    return false;
}
public void
    setEnabledCipherSuites(String[] cipherSuites)
{
    suites = new String[cipherSuites.length];
    System.arraycopy(cipherSuites, 0, suites, 0, cipherSuites.length);
}
public String[]
    getEnabledCipherSuites()
{
    if(_socket_ instanceof SSLSocket)
    {
        return ((SSLSocket)_socket_).getEnabledCipherSuites();
    }
    return null;
}
public void
    setEnabledProtocols(String[] protocolVersions)
{
    protocols = new String[protocolVersions.length];
    System.arraycopy(protocolVersions, 0, protocols, 0, protocolVersions.length);
}
public String[]
    getEnabledProtocols()
{
    if(_socket_ instanceof SSLSocket)
    {
        return ((SSLSocket)_socket_).getEnabledProtocols();
    }
    return null;
}
public void
    execPBSZ(long pbsz)
    throws SSLException,
    IOException
{
    if(pbsz < 0 || 4294967295L < pbsz)
    {
        throw new IllegalArgumentException();
    }
    int status = sendCommand(CMD_PBSZ, String.valueOf(pbsz));
    if(FTPReply.COMMAND_OK != status)
    {
        throw new SSLException(getReplyString());
    }
}
public long
    parsePBSZ(long pbsz)
    throws SSLException,
    IOException
{
    execPBSZ(pbsz);
    long minvalue = pbsz;
    String remainder = extractPrefixedData("PBSZ=", getReplyString());
    if(remainder != null)
    {
        long replysz = Long.parseLong(remainder);
        if(replysz < minvalue)
        {
            minvalue = replysz;
        }
    }
    return minvalue;
}
public void
    execPROT(String prot)
    throws SSLException,
    IOException
{
    if(prot == null)
    {
        prot = DEFAULT_PROT;
    }
    if(!checkPROTValue(prot))
    {
        throw new IllegalArgumentException();
    }
    if(FTPReply.COMMAND_OK != sendCommand(CMD_PROT, prot))
    {
        throw new SSLException(getReplyString());
    }
    if(DEFAULT_PROT.equals(prot))
    {
        setSocketFactory(null);
        setServerSocketFactory(null);
    }
    else
    {
        setSocketFactory(new FTPSSocketFactory(context));
        setServerSocketFactory(new FTPSServerSocketFactory(context));
        initSslContext();
    }
}
private boolean
    checkPROTValue(String prot)
{
    for(String element : PROT_COMMAND_VALUE)
    {
        if(element.equals(prot))
        {
            return true;
        }
    }
    return false;
}
@Override
public int
    sendCommand(String command, String args)
    throws IOException
{
    int repCode = super.sendCommand(command, args);
    if(CMD_CCC.equals(command))
    {
        if(FTPReply.COMMAND_OK == repCode)
        {
            _socket_.close();
            _socket_ = plainSocket;
            _controlInput_ = new BufferedReader(
                                                new InputStreamReader(
                                                                      _socket_.getInputStream(), getControlEncoding()));
            _controlOutput_ = new BufferedWriter(
                                                 new OutputStreamWriter(
                                                                        _socket_.getOutputStream(), getControlEncoding()));
        }
        else
        {
            throw new SSLException(getReplyString());
        }
    }
    return repCode;
}
@Override
protected Socket
    _openDataConnection_(String command, String arg)
    throws IOException
{
    Socket socket = super._openDataConnection_(command, arg);
    _prepareDataSocket_(socket);
    if(socket instanceof SSLSocket)
    {
        SSLSocket sslSocket = (SSLSocket)socket;

        sslSocket.setUseClientMode(isClientMode);
        sslSocket.setEnableSessionCreation(isCreation);

        if(!isClientMode)
        {
            sslSocket.setNeedClientAuth(isNeedClientAuth);
            sslSocket.setWantClientAuth(isWantClientAuth);
        }
        if(suites != null)
        {
            sslSocket.setEnabledCipherSuites(suites);
        }
        if(protocols != null)
        {
            sslSocket.setEnabledProtocols(protocols);
        }
        sslSocket.startHandshake();
    }

    return socket;
}
protected void
    _prepareDataSocket_(Socket socket)
    throws IOException
{
}
public TrustManager
    getTrustManager()
{
    return trustManager;
}
public void
    setTrustManager(TrustManager trustManager)
{
    this.trustManager = trustManager;
}
public HostnameVerifier
    getHostnameVerifier()
{
    return hostnameVerifier;
}
public void
    setHostnameVerifier(HostnameVerifier newHostnameVerifier)
{
    hostnameVerifier = newHostnameVerifier;
}
public boolean
    isEndpointCheckingEnabled()
{
    return tlsEndpointChecking;
}
public void
    setEndpointCheckingEnabled(boolean enable)
{
    tlsEndpointChecking = enable;
}
@Override
public void
    disconnect()
    throws IOException
{
    super.disconnect();
    if(plainSocket != null)
    {
        plainSocket.close();
    }
    setSocketFactory(null);
    setServerSocketFactory(null);
}
public int
    execAUTH(String mechanism)
    throws IOException
{
    return sendCommand(CMD_AUTH, mechanism);
}
public int
    execADAT(byte[] data)
    throws IOException
{
    if(data != null)
    {
        return sendCommand(CMD_ADAT, Base64.encodeBase64StringUnChunked(data));
    }
    else
    {
        return sendCommand(CMD_ADAT);
    }
}
public int
    execCCC()
    throws IOException
{
    int repCode = sendCommand(CMD_CCC);
    return repCode;
}
public int
    execMIC(byte[] data)
    throws IOException
{
    if(data != null)
    {
        return sendCommand(CMD_MIC, Base64.encodeBase64StringUnChunked(data));
    }
    else
    {
        return sendCommand(CMD_MIC, "");
    }
}

public int
    execCONF(byte[] data)
    throws IOException
{
    if(data != null)
    {
        return sendCommand(CMD_CONF, Base64.encodeBase64StringUnChunked(data));
    }
    else
    {
        return sendCommand(CMD_CONF, "");
    }
}

public int
    execENC(byte[] data)
    throws IOException
{
    if(data != null)
    {
        return sendCommand(CMD_ENC, Base64.encodeBase64StringUnChunked(data));
    }
    else
    {
        return sendCommand(CMD_ENC, "");
    }
}

public byte[]
    parseADATReply(String reply)
{
    if(reply == null)
    {
        return null;
    }
    else
    {
        return Base64.decodeBase64(extractPrefixedData("ADAT=", reply));
    }
}
private String
    extractPrefixedData(String prefix, String reply)
{
    int idx = reply.indexOf(prefix);
    if(idx == -1)
    {
        return null;
    }
    return reply.substring(idx + prefix.length()).trim();
}
}
