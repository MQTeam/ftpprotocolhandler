package com.atomiton.sff.imp.ftp.io;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public final class ToNetASCIIOutputStream
extends
    FilterOutputStream
{
private boolean __lastWasCR;
public ToNetASCIIOutputStream(OutputStream output)
{
    super(output);
    __lastWasCR = false;
}
@Override
public synchronized void
    write(int ch)
    throws IOException
{
    switch(ch)
    {
    case '\r':
        __lastWasCR = true;
        out.write('\r');
        return;
    case '\n':
        if(!__lastWasCR)
        {
            out.write('\r');
        }
    default:
        __lastWasCR = false;
        out.write(ch);
        return;
    }
}
@Override
public synchronized void
    write(byte buffer[])
    throws IOException
{
    write(buffer, 0, buffer.length);
}
@Override
public synchronized void
    write(byte buffer[], int offset, int length)
    throws IOException
{
    while(length-- > 0)
    {
        write(buffer[offset++]);
    }
}

}
