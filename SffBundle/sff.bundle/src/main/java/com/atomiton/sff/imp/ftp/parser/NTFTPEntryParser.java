package com.atomiton.sff.imp.ftp.parser;

import java.text.ParseException;
import java.util.regex.Pattern;

import com.atomiton.sff.imp.ftp.Configurable;
import com.atomiton.sff.imp.ftp.FTPClientConfig;
import com.atomiton.sff.imp.ftp.FTPFile;

public class NTFTPEntryParser
extends
    ConfigurableFTPFileEntryParserImpl
{

private static final String DEFAULT_DATE_FORMAT = "MM-dd-yy hh:mma";

private static final String DEFAULT_DATE_FORMAT2 = "MM-dd-yy kk:mm";

private final FTPTimestampParser timestampParser;

private static final String REGEX = "(\\S+)\\s+(\\S+)\\s+"
                                    + "(?:(<DIR>)|([0-9]+))\\s+"
                                    + "(\\S.*)";

public NTFTPEntryParser()
{
    this(null);
}

public NTFTPEntryParser(FTPClientConfig config)
{
    super(REGEX, Pattern.DOTALL);
    configure(config);
    FTPClientConfig config2 = new FTPClientConfig(
                                                  FTPClientConfig.SYST_NT,
                                                  DEFAULT_DATE_FORMAT2,
                                                  null);
    config2.setDefaultDateFormatStr(DEFAULT_DATE_FORMAT2);
    this.timestampParser = new FTPTimestampParserImpl();
    ((Configurable)this.timestampParser).configure(config2);
}

@Override
public FTPFile
    parseFTPEntry(String entry)
{
    FTPFile f = new FTPFile();
    f.setRawListing(entry);

    if(matches(entry))
    {
        String datestr = group(1) + " " + group(2);
        String dirString = group(3);
        String size = group(4);
        String name = group(5);
        try
        {
            f.setTimestamp(super.parseTimestamp(datestr));
        }
        catch(ParseException e)
        {
            try
            {
                f.setTimestamp(timestampParser.parseTimestamp(datestr));
            }
            catch(ParseException e2)
            {
            }
        }

        if(null == name || name.equals(".") || name.equals(".."))
        {
            return(null);
        }
        f.setName(name);

        if("<DIR>".equals(dirString))
        {
            f.setType(FTPFile.DIRECTORY_TYPE);
            f.setSize(0);
        }
        else
        {
            f.setType(FTPFile.FILE_TYPE);
            if(null != size)
            {
                f.setSize(Long.parseLong(size));
            }
        }
        return(f);
    }
    return null;
}

@Override
public FTPClientConfig
    getDefaultConfiguration()
{
    return new FTPClientConfig(
                               FTPClientConfig.SYST_NT,
                               DEFAULT_DATE_FORMAT,
                               null);
}

}
