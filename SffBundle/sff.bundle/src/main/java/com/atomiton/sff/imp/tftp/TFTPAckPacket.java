package com.atomiton.sff.imp.tftp;

import java.net.DatagramPacket;
import java.net.InetAddress;

public final class TFTPAckPacket
extends
    TFTPPacket
{
int _blockNumber;
public TFTPAckPacket(InetAddress destination, int port, int blockNumber)
{
    super(TFTPPacket.ACKNOWLEDGEMENT, destination, port);
    _blockNumber = blockNumber;
}

TFTPAckPacket(DatagramPacket datagram)
throws TFTPPacketException
{
    super(TFTPPacket.ACKNOWLEDGEMENT, datagram.getAddress(),
          datagram.getPort());
    byte[] data;

    data = datagram.getData();

    if(getType() != data[1])
    {
        throw new TFTPPacketException("TFTP operator code does not match type.");
    }

    _blockNumber = (((data[2] & 0xff) << 8) | (data[3] & 0xff));
}

@Override
DatagramPacket _newDatagram(DatagramPacket datagram, byte[] data)
{
    data[0] = 0;
    data[1] = (byte)_type;
    data[2] = (byte)((_blockNumber & 0xffff) >> 8);
    data[3] = (byte)(_blockNumber & 0xff);

    datagram.setAddress(_address);
    datagram.setPort(_port);
    datagram.setData(data);
    datagram.setLength(4);

    return datagram;
}

@Override
public DatagramPacket
    newDatagram()
{
    byte[] data;

    data = new byte[4];
    data[0] = 0;
    data[1] = (byte)_type;
    data[2] = (byte)((_blockNumber & 0xffff) >> 8);
    data[3] = (byte)(_blockNumber & 0xff);

    return new DatagramPacket(data, data.length, _address, _port);
}

public int
    getBlockNumber()
{
    return _blockNumber;
}
public void
    setBlockNumber(int blockNumber)
{
    _blockNumber = blockNumber;
}
@Override
public String
    toString()
{
    return super.toString() + " ACK " + _blockNumber;
}
}
