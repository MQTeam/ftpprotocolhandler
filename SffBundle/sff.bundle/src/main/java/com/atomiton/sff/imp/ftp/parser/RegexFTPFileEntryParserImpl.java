package com.atomiton.sff.imp.ftp.parser;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import com.atomiton.sff.imp.ftp.FTPFileEntryParserImpl;

public abstract class RegexFTPFileEntryParserImpl
extends
    FTPFileEntryParserImpl
{

private Pattern pattern = null;

private MatchResult result = null;

protected Matcher _matcher_ = null;

public RegexFTPFileEntryParserImpl(String regex)
{
    super();
    compileRegex(regex, 0);
}

public RegexFTPFileEntryParserImpl(String regex, final int flags)
{
    super();
    compileRegex(regex, flags);
}

public boolean
    matches(String s)
{
    this.result = null;
    _matcher_ = pattern.matcher(s);
    if(_matcher_.matches())
    {
        this.result = _matcher_.toMatchResult();
    }
    return null != this.result;
}

public int
    getGroupCnt()
{
    if(this.result == null)
    {
        return 0;
    }
    return this.result.groupCount();
}

public String
    group(int matchnum)
{
    if(this.result == null)
    {
        return null;
    }
    return this.result.group(matchnum);
}

public String
    getGroupsAsString()
{
    StringBuilder b = new StringBuilder();
    for(int i = 1; i <= this.result.groupCount(); i++)
    {
        b.append(i).append(") ").append(this.result.group(i)).append(
                                                                     System.getProperty("line.separator"));
    }
    return b.toString();
}

public boolean
    setRegex(final String regex)
{
    compileRegex(regex, 0);
    return true;
}

public boolean
    setRegex(final String regex, final int flags)
{
    compileRegex(regex, flags);
    return true;
}

private void
    compileRegex(final String regex, final int flags)
{
    try
    {
        pattern = Pattern.compile(regex, flags);
    }
    catch(PatternSyntaxException pse)
    {
        throw new IllegalArgumentException("Unparseable regex supplied: " + regex);
    }
}
}
