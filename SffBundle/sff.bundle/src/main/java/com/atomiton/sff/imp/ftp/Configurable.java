package com.atomiton.sff.imp.ftp;

public interface Configurable
{
public void
    configure(FTPClientConfig config);
}
