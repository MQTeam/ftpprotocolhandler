package com.atomiton.sff.imp.ftp.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

public final class CRLFLineReader
extends
    BufferedReader
{
private static final char LF = '\n';
private static final char CR = '\r';
public CRLFLineReader(Reader reader)
{
    super(reader);
}
@Override
public String
    readLine()
    throws IOException
{
    StringBuilder sb = new StringBuilder();
    int intch;
    boolean prevWasCR = false;
    synchronized(lock)
    {
        while((intch = read()) != -1)
        {
            if(prevWasCR && intch == LF)
            {
                return sb.substring(0, sb.length() - 1);
            }
            if(intch == CR)
            {
                prevWasCR = true;
            }
            else
            {
                prevWasCR = false;
            }
            sb.append((char)intch);
        }
    }
    String string = sb.toString();
    if(string.length() == 0)
    {
        return null;
    }
    return string;
}
}
