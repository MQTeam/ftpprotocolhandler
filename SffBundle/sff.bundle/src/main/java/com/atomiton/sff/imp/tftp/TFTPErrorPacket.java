package com.atomiton.sff.imp.tftp;

import java.net.DatagramPacket;
import java.net.InetAddress;

public final class TFTPErrorPacket
extends
    TFTPPacket
{
public static final int UNDEFINED = 0;
public static final int FILE_NOT_FOUND = 1;
public static final int ACCESS_VIOLATION = 2;
public static final int OUT_OF_SPACE = 3;
public static final int ILLEGAL_OPERATION = 4;
public static final int UNKNOWN_TID = 5;
public static final int FILE_EXISTS = 6;
public static final int NO_SUCH_USER = 7;
int _error;
String _message;
public TFTPErrorPacket(InetAddress destination, int port,
                       int error, String message)
{
    super(TFTPPacket.ERROR, destination, port);

    _error = error;
    _message = message;
}
TFTPErrorPacket(DatagramPacket datagram)
throws TFTPPacketException
{
    super(TFTPPacket.ERROR, datagram.getAddress(), datagram.getPort());
    int index, length;
    byte[] data;
    StringBuilder buffer;

    data = datagram.getData();
    length = datagram.getLength();

    if(getType() != data[1])
    {
        throw new TFTPPacketException("TFTP operator code does not match type.");
    }

    _error = (((data[2] & 0xff) << 8) | (data[3] & 0xff));

    if(length < 5)
    {
        throw new TFTPPacketException("Bad error packet. No message.");
    }

    index = 4;
    buffer = new StringBuilder();

    while(index < length && data[index] != 0)
    {
        buffer.append((char)data[index]);
        ++index;
    }

    _message = buffer.toString();
}
@Override
DatagramPacket _newDatagram(DatagramPacket datagram, byte[] data)
{
    int length;

    length = _message.length();

    data[0] = 0;
    data[1] = (byte)_type;
    data[2] = (byte)((_error & 0xffff) >> 8);
    data[3] = (byte)(_error & 0xff);

    System.arraycopy(_message.getBytes(), 0, data, 4, length);

    data[length + 4] = 0;

    datagram.setAddress(_address);
    datagram.setPort(_port);
    datagram.setData(data);
    datagram.setLength(length + 4);

    return datagram;
}
@Override
public DatagramPacket
    newDatagram()
{
    byte[] data;
    int length;

    length = _message.length();

    data = new byte[length + 5];
    data[0] = 0;
    data[1] = (byte)_type;
    data[2] = (byte)((_error & 0xffff) >> 8);
    data[3] = (byte)(_error & 0xff);

    System.arraycopy(_message.getBytes(), 0, data, 4, length);

    data[length + 4] = 0;

    return new DatagramPacket(data, data.length, _address, _port);
}
public int
    getError()
{
    return _error;
}
public String
    getMessage()
{
    return _message;
}
@Override
public String
    toString()
{
    return super.toString() + " ERR " + _error + " " + _message;
}
}
