package com.atomiton.sff.imp.commons;

import java.io.Serializable;
import java.util.EventListener;

import com.atomiton.sff.imp.ftp.util.ListenerList;

public class ProtocolCommandSupport
implements
    Serializable
{
private static final long serialVersionUID = -8017692739988399978L;
private final Object __source;
private final ListenerList __listeners;
public ProtocolCommandSupport(Object source)
{
    __listeners = new ListenerList();
    __source = source;
}
public void
    fireCommandSent(String command, String message)
{
    ProtocolCommandEvent event;

    event = new ProtocolCommandEvent(__source, command, message);

    for(EventListener listener : __listeners)
    {
        ((ProtocolCommandListener)listener).protocolCommandSent(event);
    }
}
public void
    fireReplyReceived(int replyCode, String message)
{
    ProtocolCommandEvent event;
    event = new ProtocolCommandEvent(__source, replyCode, message);

    for(EventListener listener : __listeners)
    {
        ((ProtocolCommandListener)listener).protocolReplyReceived(event);
    }
}
public void
    addProtocolCommandListener(ProtocolCommandListener listener)
{
    __listeners.addListener(listener);
}
public void
    removeProtocolCommandListener(ProtocolCommandListener listener)
{
    __listeners.removeListener(listener);
}
public int
    getListenerCount()
{
    return __listeners.getListenerCount();
}
}
