package com.atomiton.sff.imp.commons;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class DefaultDatagramSocketFactory
implements
    DatagramSocketFactory
{
@Override
public DatagramSocket
    createDatagramSocket()
    throws SocketException
{
    return new DatagramSocket();
}
@Override
public DatagramSocket
    createDatagramSocket(int port)
    throws SocketException
{
    return new DatagramSocket(port);
}
@Override
public DatagramSocket
    createDatagramSocket(int port, InetAddress laddr)
    throws SocketException
{
    return new DatagramSocket(port, laddr);
}
}
