package com.atomiton.sff.imp.ftp.io;

import java.util.EventListener;

import com.atomiton.sff.imp.ftp.util.ListenerList;

public class CopyStreamAdapter
implements
    CopyStreamListener
{
private final ListenerList internalListeners;
public CopyStreamAdapter()
{
    internalListeners = new ListenerList();
}
@Override
public void
    bytesTransferred(CopyStreamEvent event)
{
    for(EventListener listener : internalListeners)
    {
        ((CopyStreamListener)(listener)).bytesTransferred(event);
    }
}
@Override
public void
    bytesTransferred(long totalBytesTransferred,
                     int bytesTransferred, long streamSize)
{
    for(EventListener listener : internalListeners)
    {
        ((CopyStreamListener)(listener)).bytesTransferred(
                                                          totalBytesTransferred, bytesTransferred, streamSize);
    }
}

public void
    addCopyStreamListener(CopyStreamListener listener)
{
    internalListeners.addListener(listener);
}

public void
    removeCopyStreamListener(CopyStreamListener listener)
{
    internalListeners.removeListener(listener);
}
}
