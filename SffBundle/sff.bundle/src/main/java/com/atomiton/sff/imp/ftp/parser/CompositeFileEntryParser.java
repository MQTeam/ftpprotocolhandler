package com.atomiton.sff.imp.ftp.parser;

import com.atomiton.sff.imp.ftp.FTPFile;
import com.atomiton.sff.imp.ftp.FTPFileEntryParser;
import com.atomiton.sff.imp.ftp.FTPFileEntryParserImpl;

public class CompositeFileEntryParser
extends
    FTPFileEntryParserImpl
{
private final FTPFileEntryParser[] ftpFileEntryParsers;
private FTPFileEntryParser cachedFtpFileEntryParser;

public CompositeFileEntryParser(FTPFileEntryParser[] ftpFileEntryParsers)
{
    super();

    this.cachedFtpFileEntryParser = null;
    this.ftpFileEntryParsers = ftpFileEntryParsers;
}

@Override
public FTPFile
    parseFTPEntry(String listEntry)
{
    if(cachedFtpFileEntryParser != null)
    {
        FTPFile matched = cachedFtpFileEntryParser.parseFTPEntry(listEntry);
        if(matched != null)
        {
            return matched;
        }
    }
    else
    {
        for(FTPFileEntryParser ftpFileEntryParser : ftpFileEntryParsers)
        {
            FTPFile matched = ftpFileEntryParser.parseFTPEntry(listEntry);
            if(matched != null)
            {
                cachedFtpFileEntryParser = ftpFileEntryParser;
                return matched;
            }
        }
    }
    return null;
}
}
