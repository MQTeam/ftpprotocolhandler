/*******************************************************************************
 * 
 * ATOMITON CONFIDENTIAL __________________
 * 
 * [2013] - [2016] ATOMITON Incorporated All Rights Reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of Atomiton Incorporated and its suppliers, if any. The intellectual and technical
 * concepts contained herein are proprietary to Atomiton Incorporated and its suppliers and may be covered by U.S. and Foreign Patents, patents in process, and
 * are protected by trade secret or copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from Atomiton Incorporated.
 */
package com.atomiton.sff.imp.netty.ftp;

import java.io.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.atomiton.sff.imp.commons.PrintCommandListener;
import com.atomiton.sff.imp.ftp.FTPClient;
import com.atomiton.sff.imp.ftp.FTPFile;
import com.atomiton.sff.imp.ftp.FTPReply;
import com.atomiton.sff.imp.ftp.parser.ParserInitializationException;

/*******************************************************************************
 * 
 * @author Rakesh Kaim
 */

public class FTPClientImpl
{
protected static volatile FTPClientImpl ftpImpl;

protected static volatile ConcurrentMap<String, FTPClient> ftpClients;
// ==============================================================================
private FTPClientImpl()
{
}
// ==============================================================================
public static FTPClientImpl
    getInstance()
{
    if(ftpImpl == null)
    {
        synchronized(FTPClientImpl.class)
        {
            if(ftpImpl == null)
                ftpImpl = new FTPClientImpl();
        }
    }
    return ftpImpl;
}
// ==============================================================================
public boolean
    register(String ClientID,
             String HostName, String Port, String UserName, String Password, String RemotePath, String LocalPath)
{
    boolean isRegisterSucess = false;
    if(ftpClients == null)
    {
        synchronized(FTPClientImpl.class)
        {
            if(ftpClients == null)
                ftpClients = new ConcurrentHashMap<>();
        }
    }

    if(!ftpClients.containsKey(ClientID))
    {
        FTPClient Client = null;
        System.out.println("ClientID is '" + ClientID + "'");
        System.out.println("HostName is '" + HostName + "' UserName is '" + UserName + "' "
                           + "Password is '" + Password);
        System.out.println("RemotePath is '" + RemotePath + "' LocalPath is '" + LocalPath + "'");
        if(HostName != null && UserName != null && Password != null && RemotePath != null && LocalPath != null)
        {
            Client = new FTPClient();
            if(Port.equals(""))
            {
                Port = String.valueOf(Client.getDefaultPort());
            }
            System.out.println("Port is '" + Port + "'");
            Client.getParams().setHostName(HostName);
            Client.getParams().setPort(Port);
            Client.getParams().setUserName(UserName);
            Client.getParams().setPassword(Password);
            Client.getParams().setRemotePath(RemotePath);
            Client.getParams().setLocalPath(LocalPath);
            synchronized(FTPClientImpl.class)
            {
                ftpClients.putIfAbsent(ClientID, Client);
            }
            isRegisterSucess = true;
        }
        else
        {
            if(HostName == null)
                System.out.printf("FTP Error:  HostName is mendatory Parameters and can not be null/empty");
            if(UserName == null)
                System.out.printf("FTP Error:  UserName is mendatory Parameters and can not be null/empty");
            if(Password == null)
                System.out.printf("FTP Error:  Password is mendatory Parameters and can not be null/empty");
            if(RemotePath == null)
                System.out.printf("FTP Error:  RemotePath is mendatory Parameters and can not be null/empty");
            if(LocalPath == null)
                System.out.printf("FTP Error:  LocalPath is mendatory Parameters and can not be null/empty");
        }
    }
    else
    {
        throw new RuntimeException("FTP Error: Connection With Client ID already Exists");
    }
    return isRegisterSucess;
}
// ==============================================================================
public void
    ftpClientConfigure(String ClientID)
{
    FTPClient Client = ftpClients.get(ClientID);
    Client.addProtocolCommandListener(new PrintCommandListener(System.out, true));
}
// ==============================================================================
public void
    ftpOpen(String ClientID)
{
    FTPClient Client = ftpClients.get(ClientID);
    String hostName = Client.getParams().getHostName();
    int port = Integer.parseInt(Client.getParams().getPort().trim());

    try
    {
        if(port > 0)
            Client.connect(hostName, port);
        else
            Client.connect(hostName);
        System.out.println("Connected to '" + hostName + "' on port '" + (port > 0 ? port : Client.getDefaultPort()) + "'");
        int reply = Client.getReplyCode();
        if(!FTPReply.isPositiveCompletion(reply))
        {
            if(Client.isConnected())
                Client.disconnect();
            System.out.println("FTP Error: FTP server refused connection.");
        }
    }
    catch(IOException ioException)
    {
        if(Client.isConnected())
        {
            try
            {
                Client.disconnect();
            }
            catch(IOException f)
            {
            }
        }
        System.out.println("FTP Error: Could not connect to server.");
        ioException.printStackTrace();
    }
}
// ==============================================================================
public void
    ftpLogin(String ClientID)
{
    FTPClient Client = ftpClients.get(ClientID);
    String userName = Client.getParams().getUserName();
    String password = Client.getParams().getPassword();
    try
    {
        boolean isLogin = Client.login(userName, password);
        if(isLogin)
            System.out.println("User " + userName + " logged in sucessfully");
        else
            System.out.println("Server refused " + userName + " logged in. Please check username or password. Try again");
    }
    catch(IOException ioException)
    {
        System.out.println("FTP Error: Could not logged in to server. Internal Error Please contact Admin.");
        ioException.printStackTrace();
    }
}
// ==============================================================================
public void
    ftpGet(String ClientID)
{
    OutputStream output = null;
    FTPClient Client = ftpClients.get(ClientID);
    String localPath = Client.getParams().getLocalPath();
    String remotePath = Client.getParams().getRemotePath();
    try
    {
        output = new FileOutputStream(localPath);
        Client.retrieveFile(remotePath, output);
        output.close();
    }
    catch(IOException ioException)
    {
        System.out.println("FTP Error: Error in getting remote file :" + remotePath);
        ioException.printStackTrace();
    }
    finally
    {
        try
        {
            System.out.println("Inside FTP ftpGet: " + output);
            // logger.debug("Inside FTP ftpGet: " + output );
            if(null != output)
                output.close();
        }
        catch(IOException exc)
        {
            exc.printStackTrace();
        }
    }
}
// ==============================================================================
public void
    ftpPut(String ClientID)
{
    InputStream input = null;
    FTPClient Client = ftpClients.get(ClientID);
    String localPath = Client.getParams().getLocalPath();
    String remotePath = Client.getParams().getRemotePath();
    try
    {
        input = new FileInputStream(localPath);
        Client.storeFile(remotePath, input);
        input.close();
    }
    catch(IOException ioException)
    {
        System.out.println("FTP Error: Error in uploading remote file :" + remotePath);
        ioException.printStackTrace();
    }
    finally
    {
        try
        {
            input.close();
        }
        catch(IOException exc)
        {
            exc.printStackTrace();
        }
    }
}
// ==============================================================================
public void
    ftpListFiles(String ClientID)
{
    FTPClient Client = ftpClients.get(ClientID);
    String remotePath = Client.getParams().getRemotePath();
    try
    {
        for(FTPFile f : Client.listFiles(remotePath))
            System.out.println(f.getRawListing());
    }
    catch(IOException ioException)
    {
        System.out.println("FTP Error: Error in listing directory remote file :" + remotePath);
        ioException.printStackTrace();
    }
    catch(ParserInitializationException parsingException)
    {
        System.out.println("FTP Error: Parsing Error:" + remotePath);
        parsingException.printStackTrace();
    }
}
public void
    ftpLogout(String ClientID)
{
    FTPClient Client = ftpClients.get(ClientID);
    String userName = Client.getParams().getUserName();
    try
    {
        Client.noop();
        boolean isLogout = Client.logout();
        if(isLogout)
            System.out.println("User " + userName + " log out sucessfully");
        else
            System.out.println("User " + userName + " could not log out. Please see the Engine Log");
    }
    catch(IOException ioException)
    {
        System.out.println("FTP Error: Could not log out from server. Internal Error Please contact Admin.");
        ioException.printStackTrace();
    }
}
// ==============================================================================
public void
    deregister(String ClientID)
{
    System.out.println("FTP inside deregister ------------------------------------->");
    FTPClient Client = ftpClients.get(ClientID);
    try
    {
        if(Client.isConnected())
            Client.disconnect();
        System.out.println("Cleint " + ClientID + " disconnected sucessfully");
    }
    catch(IOException ioException)
    {
        if(Client.isConnected())
        {
            try
            {
                System.out.println("Cleint " + ClientID + " disconnected sucessfully");
                Client.disconnect();
            }
            catch(IOException f)
            {
            }
        }
        ioException.printStackTrace();
    }
    finally
    {
        synchronized(FTPClientImpl.class)
        {
            System.out.println("Cleint " + ClientID + " deregister sucessfully");
            ftpClients.remove(ClientID);
            System.out.println("FTP After clientID removal ------------------------------------->");
        }
    }
}
// ==============================================================================
}// FTPClientImpl
 // ******************************************************************************
