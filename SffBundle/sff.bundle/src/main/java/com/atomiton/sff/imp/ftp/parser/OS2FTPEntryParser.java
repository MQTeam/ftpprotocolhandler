package com.atomiton.sff.imp.ftp.parser;

import java.text.ParseException;

import com.atomiton.sff.imp.ftp.FTPClientConfig;
import com.atomiton.sff.imp.ftp.FTPFile;

public class OS2FTPEntryParser
extends
    ConfigurableFTPFileEntryParserImpl

{

private static final String DEFAULT_DATE_FORMAT = "MM-dd-yy HH:mm";
private static final String REGEX = "\\s*([0-9]+)\\s*"
                                    + "(\\s+|[A-Z]+)\\s*"
                                    + "(DIR|\\s+)\\s*"
                                    + "(\\S+)\\s+(\\S+)\\s+"
                                    + "(\\S.*)";
public OS2FTPEntryParser()
{
    this(null);
}
public OS2FTPEntryParser(FTPClientConfig config)
{
    super(REGEX);
    configure(config);
}
@Override
public FTPFile
    parseFTPEntry(String entry)
{

    FTPFile f = new FTPFile();
    if(matches(entry))
    {
        String size = group(1);
        String attrib = group(2);
        String dirString = group(3);
        String datestr = group(4) + " " + group(5);
        String name = group(6);
        try
        {
            f.setTimestamp(super.parseTimestamp(datestr));
        }
        catch(ParseException e)
        {
        }
        if(dirString.trim().equals("DIR") || attrib.trim().equals("DIR"))
        {
            f.setType(FTPFile.DIRECTORY_TYPE);
        }
        else
        {
            f.setType(FTPFile.FILE_TYPE);
        }
        f.setName(name.trim());
        f.setSize(Long.parseLong(size.trim()));

        return(f);
    }
    return null;

}
@Override
protected FTPClientConfig
    getDefaultConfiguration()
{
    return new FTPClientConfig(
                               FTPClientConfig.SYST_OS2,
                               DEFAULT_DATE_FORMAT,
                               null);
}

}
