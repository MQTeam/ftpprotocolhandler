package com.atomiton.sff.imp.ftp.util;

import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

public class SSLContextUtils
{

private SSLContextUtils()
{
}

public static SSLContext
    createSSLContext(String protocol, KeyManager keyManager, TrustManager trustManager)
    throws IOException
{
    return createSSLContext(protocol,
                            keyManager == null ? null : new KeyManager[]
                            {keyManager},
                            trustManager == null ? null : new TrustManager[]
                            {trustManager});
}
public static SSLContext
    createSSLContext(String protocol, KeyManager[] keyManagers, TrustManager[] trustManagers)
    throws IOException
{
    SSLContext ctx;
    try
    {
        ctx = SSLContext.getInstance(protocol);
        ctx.init(keyManagers, trustManagers, null);
    }
    catch(GeneralSecurityException e)
    {
        IOException ioe = new IOException("Could not initialize SSL context");
        ioe.initCause(e);
        throw ioe;
    }
    return ctx;
}
}
