
package com.atomiton.sff.imp.ftp.io;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class SocketOutputStream
extends
    FilterOutputStream
{
private final Socket __socket;

public SocketOutputStream(Socket socket, OutputStream stream)
{
    super(stream);
    __socket = socket;
}

@Override
public void
    write(byte buffer[], int offset, int length)
    throws IOException
{
    out.write(buffer, offset, length);
}

@Override
public void
    close()
    throws IOException
{
    super.close();
    __socket.close();
}
}
