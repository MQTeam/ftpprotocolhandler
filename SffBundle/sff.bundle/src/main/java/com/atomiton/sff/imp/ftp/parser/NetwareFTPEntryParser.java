package com.atomiton.sff.imp.ftp.parser;

import java.text.ParseException;

import com.atomiton.sff.imp.ftp.FTPClientConfig;
import com.atomiton.sff.imp.ftp.FTPFile;

public class NetwareFTPEntryParser
extends
    ConfigurableFTPFileEntryParserImpl
{

private static final String DEFAULT_DATE_FORMAT = "MMM dd yyyy";

private static final String DEFAULT_RECENT_DATE_FORMAT = "MMM dd HH:mm";

private static final String REGEX = "(d|-){1}\\s+"
                                    + "\\[([-A-Z]+)\\]\\s+"
                                    + "(\\S+)\\s+" + "(\\d+)\\s+"
                                    + "(\\S+\\s+\\S+\\s+((\\d+:\\d+)|(\\d{4})))"
                                    + "\\s+(.*)";

public NetwareFTPEntryParser()
{
    this(null);
}

public NetwareFTPEntryParser(FTPClientConfig config)
{
    super(REGEX);
    configure(config);
}

@Override
public FTPFile
    parseFTPEntry(String entry)
{

    FTPFile f = new FTPFile();
    if(matches(entry))
    {
        String dirString = group(1);
        String attrib = group(2);
        String user = group(3);
        String size = group(4);
        String datestr = group(5);
        String name = group(9);

        try
        {
            f.setTimestamp(super.parseTimestamp(datestr));
        }
        catch(ParseException e)
        {
        }
        if(dirString.trim().equals("d"))
        {
            f.setType(FTPFile.DIRECTORY_TYPE);
        }
        else
        {
            f.setType(FTPFile.FILE_TYPE);
        }

        f.setUser(user);

        f.setName(name.trim());

        f.setSize(Long.parseLong(size.trim()));

        if(attrib.indexOf("R") != -1)
        {
            f.setPermission(FTPFile.USER_ACCESS, FTPFile.READ_PERMISSION,
                            true);
        }
        if(attrib.indexOf("W") != -1)
        {
            f.setPermission(FTPFile.USER_ACCESS, FTPFile.WRITE_PERMISSION,
                            true);
        }

        return(f);
    }
    return null;

}

@Override
protected FTPClientConfig
    getDefaultConfiguration()
{
    return new FTPClientConfig(FTPClientConfig.SYST_NETWARE,
                               DEFAULT_DATE_FORMAT, DEFAULT_RECENT_DATE_FORMAT);
}

}
