package com.atomiton.sff.imp.commons;

import java.io.IOException;

public class MalformedServerReplyException
extends
    IOException
{

private static final long serialVersionUID = 6006765264250543945L;
public MalformedServerReplyException()
{
    super();
}
public MalformedServerReplyException(String message)
{
    super(message);
}

}
