package com.atomiton.sff.imp.ftp.parser;

import java.util.Calendar;

import com.atomiton.sff.imp.ftp.FTPFile;

public class EnterpriseUnixFTPEntryParser
extends
    RegexFTPFileEntryParserImpl
{
private static final String MONTHS = "(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)";
private static final String REGEX = "(([\\-]|[A-Z])([\\-]|[A-Z])([\\-]|[A-Z])([\\-]|[A-Z])([\\-]|[A-Z])"
                                    + "([\\-]|[A-Z])([\\-]|[A-Z])([\\-]|[A-Z])([\\-]|[A-Z])([\\-]|[A-Z]))"
                                    + "(\\S*)\\s*"
                                    + "(\\S+)\\s*"
                                    + "(\\S*)\\s*"
                                    + "(\\d*)\\s*"
                                    + "(\\d*)\\s*"
                                    + MONTHS
                                    + "\\s*"
                                    + "((?:[012]\\d*)|(?:3[01]))\\s*"
                                    + "((\\d\\d\\d\\d)|((?:[01]\\d)|(?:2[0123])):([012345]\\d))\\s"
                                    + "(\\S*)(\\s*.*)";

public EnterpriseUnixFTPEntryParser()
{
    super(REGEX);
}

@Override
public FTPFile
    parseFTPEntry(String entry)
{

    FTPFile file = new FTPFile();
    file.setRawListing(entry);

    if(matches(entry))
    {
        String usr = group(14);
        String grp = group(15);
        String filesize = group(16);
        String mo = group(17);
        String da = group(18);
        String yr = group(20);
        String hr = group(21);
        String min = group(22);
        String name = group(23);

        file.setType(FTPFile.FILE_TYPE);
        file.setUser(usr);
        file.setGroup(grp);
        try
        {
            file.setSize(Long.parseLong(filesize));
        }
        catch(NumberFormatException e)
        {
        }

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);

        int pos = MONTHS.indexOf(mo);
        int month = pos / 4;
        final int missingUnit;
        try
        {

            if(yr != null)
            {
                cal.set(Calendar.YEAR, Integer.parseInt(yr));
                missingUnit = Calendar.HOUR_OF_DAY;
            }
            else
            {
                missingUnit = Calendar.SECOND;
                int year = cal.get(Calendar.YEAR);
                if(cal.get(Calendar.MONTH) < month)
                {
                    year--;
                }
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hr));
                cal.set(Calendar.MINUTE, Integer.parseInt(min));
            }
            cal.set(Calendar.MONTH, month);
            cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(da));
            cal.clear(missingUnit);
            file.setTimestamp(cal);
        }
        catch(NumberFormatException e)
        {
        }
        file.setName(name);

        return file;
    }
    return null;
}
}
