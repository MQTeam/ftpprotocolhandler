package com.atomiton.sff.imp.commons;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.*;
import java.nio.charset.Charset;

import javax.net.ServerSocketFactory;
import javax.net.SocketFactory;

public abstract class SocketClient
{
public static final String NETASCII_EOL = "\r\n";
private static final SocketFactory __DEFAULT_SOCKET_FACTORY = SocketFactory.getDefault();
private static final ServerSocketFactory __DEFAULT_SERVER_SOCKET_FACTORY = ServerSocketFactory.getDefault();
private ProtocolCommandSupport __commandSupport;
protected int _timeout_;
protected Socket _socket_;
protected String _hostname_;
protected int _defaultPort_;
protected InputStream _input_;
protected OutputStream _output_;
protected SocketFactory _socketFactory_;
protected ServerSocketFactory _serverSocketFactory_;
private static final int DEFAULT_CONNECT_TIMEOUT = 0;
protected int connectTimeout = DEFAULT_CONNECT_TIMEOUT;
private int receiveBufferSize = -1;
private int sendBufferSize = -1;
private Proxy connProxy;
private Charset charset = Charset.defaultCharset();
public SocketClient()
{
    _socket_ = null;
    _hostname_ = null;
    _input_ = null;
    _output_ = null;
    _timeout_ = 0;
    _defaultPort_ = 0;
    _socketFactory_ = __DEFAULT_SOCKET_FACTORY;
    _serverSocketFactory_ = __DEFAULT_SERVER_SOCKET_FACTORY;
}
protected void
    _connectAction_()
    throws IOException
{
    _socket_.setSoTimeout(_timeout_);
    _input_ = _socket_.getInputStream();
    _output_ = _socket_.getOutputStream();
}
public void
    connect(InetAddress host, int port)
    throws SocketException,
    IOException
{
    _hostname_ = null;
    _connect(host, port, null, -1);
}
public void
    connect(String hostname, int port)
    throws SocketException,
    IOException
{
    _hostname_ = hostname;
    _connect(InetAddress.getByName(hostname), port, null, -1);
}
public void
    connect(InetAddress host, int port,
            InetAddress localAddr, int localPort)
    throws SocketException,
    IOException
{
    _hostname_ = null;
    _connect(host, port, localAddr, localPort);
}
private void
    _connect(InetAddress host, int port, InetAddress localAddr, int localPort)
    throws SocketException,
    IOException
{
    _socket_ = _socketFactory_.createSocket();
    if(receiveBufferSize != -1)
    {
        _socket_.setReceiveBufferSize(receiveBufferSize);
    }
    if(sendBufferSize != -1)
    {
        _socket_.setSendBufferSize(sendBufferSize);
    }
    if(localAddr != null)
    {
        _socket_.bind(new InetSocketAddress(localAddr, localPort));
    }
    _socket_.connect(new InetSocketAddress(host, port), connectTimeout);
    _connectAction_();
}
public void
    connect(String hostname, int port,
            InetAddress localAddr, int localPort)
    throws SocketException,
    IOException
{
    _hostname_ = hostname;
    _connect(InetAddress.getByName(hostname), port, localAddr, localPort);
}
public void
    connect(InetAddress host)
    throws SocketException,
    IOException
{
    _hostname_ = null;
    connect(host, _defaultPort_);
}
public void
    connect(String hostname)
    throws SocketException,
    IOException
{
    connect(hostname, _defaultPort_);
}
public void
    disconnect()
    throws IOException
{
    closeQuietly(_socket_);
    closeQuietly(_input_);
    closeQuietly(_output_);
    _socket_ = null;
    _hostname_ = null;
    _input_ = null;
    _output_ = null;
}
private void
    closeQuietly(Socket socket)
{
    if(socket != null)
    {
        try
        {
            socket.close();
        }
        catch(IOException e)
        {
        }
    }
}
private void
    closeQuietly(Closeable close)
{
    if(close != null)
    {
        try
        {
            close.close();
        }
        catch(IOException e)
        {
        }
    }
}
public boolean
    isConnected()
{
    if(_socket_ == null)
    {
        return false;
    }

    return _socket_.isConnected();
}
public boolean
    isAvailable()
{
    if(isConnected())
    {
        try
        {
            if(_socket_.getInetAddress() == null)
            {
                return false;
            }
            if(_socket_.getPort() == 0)
            {
                return false;
            }
            if(_socket_.getRemoteSocketAddress() == null)
            {
                return false;
            }
            if(_socket_.isClosed())
            {
                return false;
            }
            if(_socket_.isInputShutdown())
            {
                return false;
            }
            if(_socket_.isOutputShutdown())
            {
                return false;
            }
            _socket_.getInputStream();
            _socket_.getOutputStream();
        }
        catch(IOException ioex)
        {
            return false;
        }
        return true;
    }
    else
    {
        return false;
    }
}
public void
    setDefaultPort(int port)
{
    _defaultPort_ = port;
}
public int
    getDefaultPort()
{
    return _defaultPort_;
}
public void
    setDefaultTimeout(int timeout)
{
    _timeout_ = timeout;
}
public int
    getDefaultTimeout()
{
    return _timeout_;
}
public void
    setSoTimeout(int timeout)
    throws SocketException
{
    _socket_.setSoTimeout(timeout);
}
public void
    setSendBufferSize(int size)
    throws SocketException
{
    sendBufferSize = size;
}
protected int
    getSendBufferSize()
{
    return sendBufferSize;
}
public void
    setReceiveBufferSize(int size)
    throws SocketException
{
    receiveBufferSize = size;
}
protected int
    getReceiveBufferSize()
{
    return receiveBufferSize;
}
public int
    getSoTimeout()
    throws SocketException
{
    return _socket_.getSoTimeout();
}
public void
    setTcpNoDelay(boolean on)
    throws SocketException
{
    _socket_.setTcpNoDelay(on);
}
public boolean
    getTcpNoDelay()
    throws SocketException
{
    return _socket_.getTcpNoDelay();
}
public void
    setKeepAlive(boolean keepAlive)
    throws SocketException
{
    _socket_.setKeepAlive(keepAlive);
}
public boolean
    getKeepAlive()
    throws SocketException
{
    return _socket_.getKeepAlive();
}
public void
    setSoLinger(boolean on, int val)
    throws SocketException
{
    _socket_.setSoLinger(on, val);
}
public int
    getSoLinger()
    throws SocketException
{
    return _socket_.getSoLinger();
}
public int
    getLocalPort()
{
    return _socket_.getLocalPort();
}
public InetAddress
    getLocalAddress()
{
    return _socket_.getLocalAddress();
}
public int
    getRemotePort()
{
    return _socket_.getPort();
}
public InetAddress
    getRemoteAddress()
{
    return _socket_.getInetAddress();
}
public boolean
    verifyRemote(Socket socket)
{
    InetAddress host1, host2;

    host1 = socket.getInetAddress();
    host2 = getRemoteAddress();

    return host1.equals(host2);
}
public void
    setSocketFactory(SocketFactory factory)
{
    if(factory == null)
    {
        _socketFactory_ = __DEFAULT_SOCKET_FACTORY;
    }
    else
    {
        _socketFactory_ = factory;
    }
    connProxy = null;
}
public void
    setServerSocketFactory(ServerSocketFactory factory)
{
    if(factory == null)
    {
        _serverSocketFactory_ = __DEFAULT_SERVER_SOCKET_FACTORY;
    }
    else
    {
        _serverSocketFactory_ = factory;
    }
}
public void
    setConnectTimeout(int connectTimeout)
{
    this.connectTimeout = connectTimeout;
}
public int
    getConnectTimeout()
{
    return connectTimeout;
}
public ServerSocketFactory
    getServerSocketFactory()
{
    return _serverSocketFactory_;
}
public void
    addProtocolCommandListener(ProtocolCommandListener listener)
{
    getCommandSupport().addProtocolCommandListener(listener);
}
public void
    removeProtocolCommandListener(ProtocolCommandListener listener)
{
    getCommandSupport().removeProtocolCommandListener(listener);
}
protected void
    fireReplyReceived(int replyCode, String reply)
{
    if(getCommandSupport().getListenerCount() > 0)
    {
        getCommandSupport().fireReplyReceived(replyCode, reply);
    }
}
protected void
    fireCommandSent(String command, String message)
{
    if(getCommandSupport().getListenerCount() > 0)
    {
        getCommandSupport().fireCommandSent(command, message);
    }
}
protected void
    createCommandSupport()
{
    __commandSupport = new ProtocolCommandSupport(this);
}
protected ProtocolCommandSupport
    getCommandSupport()
{
    return __commandSupport;
}
public void
    setProxy(Proxy proxy)
{
    setSocketFactory(new DefaultSocketFactory(proxy));
    connProxy = proxy;
}
public Proxy
    getProxy()
{
    return connProxy;
}
public Charset
    getCharset()
{
    return charset;
}
public void
    setCharset(Charset charset)
{
    this.charset = charset;
}
}
