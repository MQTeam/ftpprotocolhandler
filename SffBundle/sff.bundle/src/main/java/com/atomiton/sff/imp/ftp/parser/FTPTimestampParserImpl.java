package com.atomiton.sff.imp.ftp.parser;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.atomiton.sff.imp.ftp.Configurable;
import com.atomiton.sff.imp.ftp.FTPClientConfig;

public class FTPTimestampParserImpl
implements
    FTPTimestampParser,
    Configurable
{
private SimpleDateFormat defaultDateFormat;
private int defaultDateSmallestUnitIndex;
private SimpleDateFormat recentDateFormat;
private int recentDateSmallestUnitIndex;
private boolean lenientFutureDates = false;
private static final int[] CALENDAR_UNITS =
{
 Calendar.MILLISECOND,
 Calendar.SECOND,
 Calendar.MINUTE,
 Calendar.HOUR_OF_DAY,
 Calendar.DAY_OF_MONTH,
 Calendar.MONTH,
 Calendar.YEAR};
private static int
    getEntry(SimpleDateFormat dateFormat)
{
    if(dateFormat == null)
    {
        return 0;
    }
    final String FORMAT_CHARS = "SsmHdM";
    final String pattern = dateFormat.toPattern();
    for(char ch : FORMAT_CHARS.toCharArray())
    {
        if(pattern.indexOf(ch) != -1)
        {
            switch(ch)
            {
            case 'S':
                return indexOf(Calendar.MILLISECOND);
            case 's':
                return indexOf(Calendar.SECOND);
            case 'm':
                return indexOf(Calendar.MINUTE);
            case 'H':
                return indexOf(Calendar.HOUR_OF_DAY);
            case 'd':
                return indexOf(Calendar.DAY_OF_MONTH);
            case 'M':
                return indexOf(Calendar.MONTH);
            }
        }
    }
    return 0;
}
private static int
    indexOf(int calendarUnit)
{
    int i;
    for(i = 0; i < CALENDAR_UNITS.length; i++)
    {
        if(calendarUnit == CALENDAR_UNITS[i])
        {
            return i;
        }
    }
    return 0;
}
private static void
    setPrecision(int index, Calendar working)
{
    if(index <= 0)
    {
        return;
    }
    final int field = CALENDAR_UNITS[index - 1];
    final int value = working.get(field);
    if(value != 0)
    {
    }
    else
    {
        working.clear(field);
    }
}
public FTPTimestampParserImpl()
{
    setDefaultDateFormat(DEFAULT_SDF, null);
    setRecentDateFormat(DEFAULT_RECENT_SDF, null);
}
@Override
public Calendar
    parseTimestamp(String timestampStr)
    throws ParseException
{
    Calendar now = Calendar.getInstance();
    return parseTimestamp(timestampStr, now);
}
public Calendar
    parseTimestamp(String timestampStr, Calendar serverTime)
    throws ParseException
{
    Calendar working = (Calendar)serverTime.clone();
    working.setTimeZone(getServerTimeZone());

    Date parsed = null;

    if(recentDateFormat != null)
    {
        Calendar now = (Calendar)serverTime.clone();
        now.setTimeZone(this.getServerTimeZone());
        if(lenientFutureDates)
        {
            now.add(Calendar.DAY_OF_MONTH, 1);
        }
        String year = Integer.toString(now.get(Calendar.YEAR));
        String timeStampStrPlusYear = timestampStr + " " + year;
        SimpleDateFormat hackFormatter = new SimpleDateFormat(recentDateFormat.toPattern() + " yyyy",
                                                              recentDateFormat.getDateFormatSymbols());
        hackFormatter.setLenient(false);
        hackFormatter.setTimeZone(recentDateFormat.getTimeZone());
        ParsePosition pp = new ParsePosition(0);
        parsed = hackFormatter.parse(timeStampStrPlusYear, pp);
        if(parsed != null && pp.getIndex() == timeStampStrPlusYear.length())
        {
            working.setTime(parsed);
            if(working.after(now))
            {
                working.add(Calendar.YEAR, -1);
            }
            setPrecision(recentDateSmallestUnitIndex, working);
            return working;
        }
    }

    ParsePosition pp = new ParsePosition(0);
    parsed = defaultDateFormat.parse(timestampStr, pp);
    if(parsed != null && pp.getIndex() == timestampStr.length())
    {
        working.setTime(parsed);
    }
    else
    {
        throw new ParseException(
                                 "Timestamp '" + timestampStr + "' could not be parsed using a server time of "
                                 + serverTime.getTime().toString(),
                                 pp.getErrorIndex());
    }
    setPrecision(defaultDateSmallestUnitIndex, working);
    return working;
}
public SimpleDateFormat
    getDefaultDateFormat()
{
    return defaultDateFormat;
}
public String
    getDefaultDateFormatString()
{
    return defaultDateFormat.toPattern();
}
private void
    setDefaultDateFormat(String format, DateFormatSymbols dfs)
{
    if(format != null)
    {
        if(dfs != null)
        {
            this.defaultDateFormat = new SimpleDateFormat(format, dfs);
        }
        else
        {
            this.defaultDateFormat = new SimpleDateFormat(format);
        }
        this.defaultDateFormat.setLenient(false);
    }
    else
    {
        this.defaultDateFormat = null;
    }
    this.defaultDateSmallestUnitIndex = getEntry(this.defaultDateFormat);
}
public SimpleDateFormat
    getRecentDateFormat()
{
    return recentDateFormat;
}
public String
    getRecentDateFormatString()
{
    return recentDateFormat.toPattern();
}
private void
    setRecentDateFormat(String format, DateFormatSymbols dfs)
{
    if(format != null)
    {
        if(dfs != null)
        {
            this.recentDateFormat = new SimpleDateFormat(format, dfs);
        }
        else
        {
            this.recentDateFormat = new SimpleDateFormat(format);
        }
        this.recentDateFormat.setLenient(false);
    }
    else
    {
        this.recentDateFormat = null;
    }
    this.recentDateSmallestUnitIndex = getEntry(this.recentDateFormat);
}

public String[]
    getShortMonths()
{
    return defaultDateFormat.getDateFormatSymbols().getShortMonths();
}
public TimeZone
    getServerTimeZone()
{
    return this.defaultDateFormat.getTimeZone();
}
private void
    setServerTimeZone(String serverTimeZoneId)
{
    TimeZone serverTimeZone = TimeZone.getDefault();
    if(serverTimeZoneId != null)
    {
        serverTimeZone = TimeZone.getTimeZone(serverTimeZoneId);
    }
    this.defaultDateFormat.setTimeZone(serverTimeZone);
    if(this.recentDateFormat != null)
    {
        this.recentDateFormat.setTimeZone(serverTimeZone);
    }
}
@Override
public void
    configure(FTPClientConfig config)
{
    DateFormatSymbols dfs = null;

    String languageCode = config.getServerLanguageCode();
    String shortmonths = config.getShortMonthNames();
    if(shortmonths != null)
    {
        dfs = FTPClientConfig.getDateFormatSymbols(shortmonths);
    }
    else if(languageCode != null)
    {
        dfs = FTPClientConfig.lookupDateFormatSymbols(languageCode);
    }
    else
    {
        dfs = FTPClientConfig.lookupDateFormatSymbols("en");
    }

    String recentFormatString = config.getRecentDateFormatStr();
    setRecentDateFormat(recentFormatString, dfs);

    String defaultFormatString = config.getDefaultDateFormatStr();
    if(defaultFormatString == null)
    {
        throw new IllegalArgumentException("defaultFormatString cannot be null");
    }
    setDefaultDateFormat(defaultFormatString, dfs);

    setServerTimeZone(config.getServerTimeZoneId());

    this.lenientFutureDates = config.isLenientFutureDates();
}
boolean isLenientFutureDates()
{
    return lenientFutureDates;
}
void setLenientFutureDates(boolean lenientFutureDates)
{
    this.lenientFutureDates = lenientFutureDates;
}
}
