package com.atomiton.sff.imp.commons;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public interface DatagramSocketFactory
{
public DatagramSocket
    createDatagramSocket()
    throws SocketException;
public DatagramSocket
    createDatagramSocket(int port)
    throws SocketException;
public DatagramSocket
    createDatagramSocket(int port, InetAddress laddr)
    throws SocketException;
}
