package com.atomiton.sff.imp.tftp;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.DatagramPacket;
import java.net.SocketException;

import com.atomiton.sff.imp.commons.DatagramSocketClient;

public class TFTP
extends
    DatagramSocketClient
{
public static final int ASCII_MODE = 0;
public static final int NETASCII_MODE = 0;
public static final int BINARY_MODE = 1;
public static final int IMAGE_MODE = 1;
public static final int OCTET_MODE = 1;
public static final int DEFAULT_TIMEOUT = 5000;
public static final int DEFAULT_PORT = 69;
static final int PACKET_SIZE = TFTPPacket.SEGMENT_SIZE + 4;
private byte[] __receiveBuffer;
private DatagramPacket __receiveDatagram;
private DatagramPacket __sendDatagram;
byte[] _sendBuffer;
public static final String
    getModeName(int mode)
{
    return TFTPRequestPacket._modeStrings[mode];
}
public TFTP()
{
    setDefaultTimeout(DEFAULT_TIMEOUT);
    __receiveBuffer = null;
    __receiveDatagram = null;
}
public final void
    discardPackets()
    throws IOException
{
    int to;
    DatagramPacket datagram;

    datagram = new DatagramPacket(new byte[PACKET_SIZE], PACKET_SIZE);

    to = getSoTimeout();
    setSoTimeout(1);

    try
    {
        while(true)
        {
            _socket_.receive(datagram);
        }
    }
    catch(SocketException e)
    {
    }
    catch(InterruptedIOException e)
    {
    }
    setSoTimeout(to);
}
public final TFTPPacket
    bufferedReceive()
    throws IOException,
    InterruptedIOException,
    SocketException,
    TFTPPacketException
{
    __receiveDatagram.setData(__receiveBuffer);
    __receiveDatagram.setLength(__receiveBuffer.length);
    _socket_.receive(__receiveDatagram);

    TFTPPacket newTFTPPacket = TFTPPacket.newTFTPPacket(__receiveDatagram);
    trace("<", newTFTPPacket);
    return newTFTPPacket;
}
public final void
    bufferedSend(TFTPPacket packet)
    throws IOException
{
    trace(">", packet);
    _socket_.send(packet._newDatagram(__sendDatagram, _sendBuffer));
}
public final void
    beginBufferedOps()
{
    __receiveBuffer = new byte[PACKET_SIZE];
    __receiveDatagram = new DatagramPacket(__receiveBuffer, __receiveBuffer.length);
    _sendBuffer = new byte[PACKET_SIZE];
    __sendDatagram = new DatagramPacket(_sendBuffer, _sendBuffer.length);
}
public final void
    endBufferedOps()
{
    __receiveBuffer = null;
    __receiveDatagram = null;
    _sendBuffer = null;
    __sendDatagram = null;
}
public final void
    send(TFTPPacket packet)
    throws IOException
{
    trace(">", packet);
    _socket_.send(packet.newDatagram());
}
public final TFTPPacket
    receive()
    throws IOException,
    InterruptedIOException,
    SocketException,
    TFTPPacketException
{
    DatagramPacket packet;

    packet = new DatagramPacket(new byte[PACKET_SIZE], PACKET_SIZE);

    _socket_.receive(packet);

    TFTPPacket newTFTPPacket = TFTPPacket.newTFTPPacket(packet);
    trace("<", newTFTPPacket);
    return newTFTPPacket;
}
protected void
    trace(String direction, TFTPPacket packet)
{
}
}
