package com.atomiton.sff.imp.ftp.io;

import java.io.*;
import java.net.Socket;

public final class Util
{
public static final int DEFAULT_COPY_BUFFER_SIZE = 1024;

private Util()
{
}
public static final long
    copyStream(InputStream source, OutputStream dest,
               int bufferSize, long streamSize,
               CopyStreamListener listener,
               boolean flush)
    throws CopyStreamException
{
    int numBytes;
    long total = 0;
    byte[] buffer = new byte[bufferSize > 0 ? bufferSize : DEFAULT_COPY_BUFFER_SIZE];

    try
    {
        while((numBytes = source.read(buffer)) != -1)
        {

            if(numBytes == 0)
            {
                int singleByte = source.read();
                if(singleByte < 0)
                {
                    break;
                }
                dest.write(singleByte);
                if(flush)
                {
                    dest.flush();
                }
                ++total;
                if(listener != null)
                {
                    listener.bytesTransferred(total, 1, streamSize);
                }
                continue;
            }

            dest.write(buffer, 0, numBytes);
            if(flush)
            {
                dest.flush();
            }
            total += numBytes;
            if(listener != null)
            {
                listener.bytesTransferred(total, numBytes, streamSize);
            }
        }
    }
    catch(IOException e)
    {
        throw new CopyStreamException("IOException caught while copying.",
                                      total, e);
    }

    return total;
}
public static final long
    copyStream(InputStream source, OutputStream dest,
               int bufferSize, long streamSize,
               CopyStreamListener listener)
    throws CopyStreamException
{
    return copyStream(source, dest, bufferSize, streamSize, listener,
                      true);
}
public static final long
    copyStream(InputStream source, OutputStream dest,
               int bufferSize)
    throws CopyStreamException
{
    return copyStream(source, dest, bufferSize,
                      CopyStreamEvent.UNKNOWN_STREAM_SIZE, null);
}
public static final long
    copyStream(InputStream source, OutputStream dest)
    throws CopyStreamException
{
    return copyStream(source, dest, DEFAULT_COPY_BUFFER_SIZE);
}
public static final long
    copyReader(Reader source, Writer dest,
               int bufferSize, long streamSize,
               CopyStreamListener listener)
    throws CopyStreamException
{
    int numChars;
    long total = 0;
    char[] buffer = new char[bufferSize > 0 ? bufferSize : DEFAULT_COPY_BUFFER_SIZE];

    try
    {
        while((numChars = source.read(buffer)) != -1)
        {
            if(numChars == 0)
            {
                int singleChar = source.read();
                if(singleChar < 0)
                {
                    break;
                }
                dest.write(singleChar);
                dest.flush();
                ++total;
                if(listener != null)
                {
                    listener.bytesTransferred(total, 1, streamSize);
                }
                continue;
            }

            dest.write(buffer, 0, numChars);
            dest.flush();
            total += numChars;
            if(listener != null)
            {
                listener.bytesTransferred(total, numChars, streamSize);
            }
        }
    }
    catch(IOException e)
    {
        throw new CopyStreamException("IOException caught while copying.",
                                      total, e);
    }

    return total;
}
public static final long
    copyReader(Reader source, Writer dest,
               int bufferSize)
    throws CopyStreamException
{
    return copyReader(source, dest, bufferSize,
                      CopyStreamEvent.UNKNOWN_STREAM_SIZE, null);
}
public static final long
    copyReader(Reader source, Writer dest)
    throws CopyStreamException
{
    return copyReader(source, dest, DEFAULT_COPY_BUFFER_SIZE);
}
public static void
    closeQuietly(Closeable closeable)
{
    if(closeable != null)
    {
        try
        {
            closeable.close();
        }
        catch(IOException e)
        {
        }
    }
}
public static void
    closeQuietly(Socket socket)
{
    if(socket != null)
    {
        try
        {
            socket.close();
        }
        catch(IOException e)
        {
        }
    }
}
}
