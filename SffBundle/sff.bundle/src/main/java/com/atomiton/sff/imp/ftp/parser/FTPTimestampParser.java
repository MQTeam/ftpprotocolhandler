package com.atomiton.sff.imp.ftp.parser;

import java.text.ParseException;
import java.util.Calendar;

public interface FTPTimestampParser
{
public static final String DEFAULT_SDF = UnixFTPEntryParser.DEFAULT_DATE_FORMAT;
public static final String DEFAULT_RECENT_SDF = UnixFTPEntryParser.DEFAULT_RECENT_DATE_FORMAT;
public Calendar
    parseTimestamp(String timestampStr)
    throws ParseException;
}
