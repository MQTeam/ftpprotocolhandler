package com.atomiton.sff.imp.ftp.io;

import java.util.EventListener;

public interface CopyStreamListener
extends
    EventListener
{
public void
    bytesTransferred(CopyStreamEvent event);
public void
    bytesTransferred(long totalBytesTransferred,
                     int bytesTransferred,
                     long streamSize);
}
