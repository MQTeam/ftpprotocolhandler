package com.atomiton.sff.imp.ftp.parser;

import java.text.ParseException;
import java.util.List;

import com.atomiton.sff.imp.ftp.FTPClientConfig;
import com.atomiton.sff.imp.ftp.FTPFile;

public class MVSFTPEntryParser
extends
    ConfigurableFTPFileEntryParserImpl
{

static final int UNKNOWN_LIST_TYPE = -1;
static final int FILE_LIST_TYPE = 0;
static final int MEMBER_LIST_TYPE = 1;
static final int UNIX_LIST_TYPE = 2;
static final int JES_LEVEL_1_LIST_TYPE = 3;
static final int JES_LEVEL_2_LIST_TYPE = 4;

private int isType = UNKNOWN_LIST_TYPE;

private UnixFTPEntryParser unixFTPEntryParser;

static final String DEFAULT_DATE_FORMAT = "yyyy/MM/dd HH:mm";

static final String FILE_LIST_REGEX = "\\S+\\s+" +
                                      "\\S+\\s+" +
                                      "\\S+\\s+" +
                                      "\\S+\\s+" +
                                      "\\S+\\s+" +
                                      "[FV]\\S*\\s+" +
                                      "\\S+\\s+" +
                                      "\\S+\\s+" +
                                      "(PS|PO|PO-E)\\s+" +
                                      "(\\S+)\\s*";

static final String MEMBER_LIST_REGEX = "(\\S+)\\s+" +
                                        "\\S+\\s+" +
                                        "\\S+\\s+" +
                                        "(\\S+)\\s+" +
                                        "(\\S+)\\s+" +
                                        "\\S+\\s+" +
                                        "\\S+\\s+" +
                                        "\\S+\\s+" +
                                        "\\S+\\s*";

static final String JES_LEVEL_1_LIST_REGEX = "(\\S+)\\s+" +
                                             "(\\S+)\\s+" +
                                             "(\\S+)\\s+" +
                                             "(\\S+)\\s+" +
                                             "(\\S+)\\s+" +
                                             "(\\S+)\\s*";

static final String JES_LEVEL_2_LIST_REGEX = "(\\S+)\\s+" +
                                             "(\\S+)\\s+" +
                                             "(\\S+)\\s+" +
                                             "(\\S+)\\s+" +
                                             "(\\S+)\\s+" +
                                             "(\\S+).*";

public MVSFTPEntryParser()
{
    super("");
    super.configure(null);
}

@Override
public FTPFile
    parseFTPEntry(String entry)
{
    boolean isParsed = false;
    FTPFile f = new FTPFile();

    if(isType == FILE_LIST_TYPE)
    {
        isParsed = parseFileList(f, entry);
    }
    else if(isType == MEMBER_LIST_TYPE)
    {
        isParsed = parseMemberList(f, entry);
        if(!isParsed)
        {
            isParsed = parseSimpleEntry(f, entry);
        }
    }
    else if(isType == UNIX_LIST_TYPE)
    {
        isParsed = parseUnixList(f, entry);
    }
    else if(isType == JES_LEVEL_1_LIST_TYPE)
    {
        isParsed = parseJeslevel1List(f, entry);
    }
    else if(isType == JES_LEVEL_2_LIST_TYPE)
    {
        isParsed = parseJeslevel2List(f, entry);
    }

    if(!isParsed)
    {
        f = null;
    }

    return f;
}

private boolean
    parseFileList(FTPFile file, String entry)
{
    if(matches(entry))
    {
        file.setRawListing(entry);
        String name = group(2);
        String dsorg = group(1);
        file.setName(name);

        if("PS".equals(dsorg))
        {
            file.setType(FTPFile.FILE_TYPE);
        }
        else if("PO".equals(dsorg) || "PO-E".equals(dsorg))
        {
            file.setType(FTPFile.DIRECTORY_TYPE);
        }
        else
        {
            return false;
        }

        return true;
    }

    return false;
}

private boolean
    parseMemberList(FTPFile file, String entry)
{
    if(matches(entry))
    {
        file.setRawListing(entry);
        String name = group(1);
        String datestr = group(2) + " " + group(3);
        file.setName(name);
        file.setType(FTPFile.FILE_TYPE);
        try
        {
            file.setTimestamp(super.parseTimestamp(datestr));
        }
        catch(ParseException e)
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    return false;
}

private boolean
    parseSimpleEntry(FTPFile file, String entry)
{
    if(entry != null && entry.trim().length() > 0)
    {
        file.setRawListing(entry);
        String name = entry.split(" ")[0];
        file.setName(name);
        file.setType(FTPFile.FILE_TYPE);
        return true;
    }
    return false;
}

private boolean
    parseUnixList(FTPFile file, String entry)
{
    file = unixFTPEntryParser.parseFTPEntry(entry);
    if(file == null)
    {
        return false;
    }
    return true;
}

private boolean
    parseJeslevel1List(FTPFile file, String entry)
{
    if(matches(entry))
    {
        if(group(3).equalsIgnoreCase("OUTPUT"))
        {
            file.setRawListing(entry);
            String name = group(2);
            file.setName(name);
            file.setType(FTPFile.FILE_TYPE);
            return true;
        }
    }

    return false;
}

private boolean
    parseJeslevel2List(FTPFile file, String entry)
{
    if(matches(entry))
    {
        if(group(4).equalsIgnoreCase("OUTPUT"))
        {
            file.setRawListing(entry);
            String name = group(2);
            file.setName(name);
            file.setType(FTPFile.FILE_TYPE);
            return true;
        }
    }

    return false;
}

@Override
public List<String>
    preParse(List<String> orig)
{
    if(orig != null && orig.size() > 0)
    {
        String header = orig.get(0);
        if(header.indexOf("Volume") >= 0 && header.indexOf("Dsname") >= 0)
        {
            setType(FILE_LIST_TYPE);
            super.setRegex(FILE_LIST_REGEX);
        }
        else if(header.indexOf("Name") >= 0 && header.indexOf("Id") >= 0)
        {
            setType(MEMBER_LIST_TYPE);
            super.setRegex(MEMBER_LIST_REGEX);
        }
        else if(header.indexOf("total") == 0)
        {
            setType(UNIX_LIST_TYPE);
            unixFTPEntryParser = new UnixFTPEntryParser();
        }
        else if(header.indexOf("Spool Files") >= 30)
        {
            setType(JES_LEVEL_1_LIST_TYPE);
            super.setRegex(JES_LEVEL_1_LIST_REGEX);
        }
        else if(header.indexOf("JOBNAME") == 0
                && header.indexOf("JOBID") > 8)
        {
            setType(JES_LEVEL_2_LIST_TYPE);
            super.setRegex(JES_LEVEL_2_LIST_REGEX);
        }
        else
        {
            setType(UNKNOWN_LIST_TYPE);
        }

        if(isType != JES_LEVEL_1_LIST_TYPE)
        {
            orig.remove(0);
        }
    }

    return orig;
}

void setType(int type)
{
    isType = type;
}

@Override
protected FTPClientConfig
    getDefaultConfiguration()
{
    return new FTPClientConfig(FTPClientConfig.SYST_MVS,
                               DEFAULT_DATE_FORMAT, null);
}

}
