package com.atomiton.sff.imp.ftp;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

public interface FTPFileEntryParser
{
FTPFile parseFTPEntry(String listEntry);
String readNextEntry(BufferedReader reader)
throws IOException;
List<String> preParse(List<String> original);
}
