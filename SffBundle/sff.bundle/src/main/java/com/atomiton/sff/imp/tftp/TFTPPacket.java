package com.atomiton.sff.imp.tftp;

import java.net.DatagramPacket;
import java.net.InetAddress;

public abstract class TFTPPacket
{
static final int MIN_PACKET_SIZE = 4;
public static final int READ_REQUEST = 1;
public static final int WRITE_REQUEST = 2;
public static final int DATA = 3;
public static final int ACKNOWLEDGEMENT = 4;
public static final int ERROR = 5;
public static final int SEGMENT_SIZE = 512;
int _type;
int _port;
InetAddress _address;

public static final TFTPPacket
    newTFTPPacket(DatagramPacket datagram)
    throws TFTPPacketException
{
    byte[] data;
    TFTPPacket packet = null;

    if(datagram.getLength() < MIN_PACKET_SIZE)
    {
        throw new TFTPPacketException(
                                      "Bad packet. Datagram data length is too short.");
    }

    data = datagram.getData();

    switch(data[1])
    {
    case READ_REQUEST:
        packet = new TFTPReadRequestPacket(datagram);
        break;
    case WRITE_REQUEST:
        packet = new TFTPWriteRequestPacket(datagram);
        break;
    case DATA:
        packet = new TFTPDataPacket(datagram);
        break;
    case ACKNOWLEDGEMENT:
        packet = new TFTPAckPacket(datagram);
        break;
    case ERROR:
        packet = new TFTPErrorPacket(datagram);
        break;
    default:
        throw new TFTPPacketException(
                                      "Bad packet.  Invalid TFTP operator code.");
    }

    return packet;
}
TFTPPacket(int type, InetAddress address, int port)
{
    _type = type;
    _address = address;
    _port = port;
}
abstract DatagramPacket
    _newDatagram(DatagramPacket datagram, byte[] data);
public abstract DatagramPacket
    newDatagram();
public final int
    getType()
{
    return _type;
}
public final InetAddress
    getAddress()
{
    return _address;
}
public final int
    getPort()
{
    return _port;
}
public final void
    setPort(int port)
{
    _port = port;
}
public final void
    setAddress(InetAddress address)
{
    _address = address;
}
@Override
public String
    toString()
{
    return _address + " " + _port + " " + _type;
}
}
