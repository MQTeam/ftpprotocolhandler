package com.atomiton.sff.imp.ftp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

import com.atomiton.sff.imp.ftp.util.Charsets;

public class FTPListParseEngine
{
private List<String> entries = new LinkedList<String>();
private ListIterator<String> _internalIterator = entries.listIterator();
private final FTPFileEntryParser parser;
private final boolean saveUnparseableEntries;
public FTPListParseEngine(FTPFileEntryParser parser)
{
    this(parser, null);
}
FTPListParseEngine(FTPFileEntryParser parser, FTPClientConfig configuration)
{
    this.parser = parser;
    if(configuration != null)
    {
        this.saveUnparseableEntries = configuration.getUnparseableEntries();
    }
    else
    {
        this.saveUnparseableEntries = false;
    }
}
public void
    readServerList(InputStream stream, String encoding)
    throws IOException
{
    this.entries = new LinkedList();
    readStream(stream, encoding);
    this.parser.preParse(this.entries);
    resetIterator();
}
private void
    readStream(InputStream stream, String encoding)
    throws IOException
{
    BufferedReader reader = new BufferedReader(
                                               new InputStreamReader(stream, Charsets.toCharset(encoding)));

    String line = this.parser.readNextEntry(reader);

    while(line != null)
    {
        this.entries.add(line);
        line = this.parser.readNextEntry(reader);
    }
    reader.close();
}
public FTPFile[]
    getNext(int quantityRequested)
{
    List<FTPFile> tmpResults = new LinkedList<FTPFile>();
    int count = quantityRequested;
    while(count > 0 && this._internalIterator.hasNext())
    {
        String entry = this._internalIterator.next();
        FTPFile temp = this.parser.parseFTPEntry(entry);
        if(temp == null && saveUnparseableEntries)
        {
            temp = new FTPFile(entry);
        }
        tmpResults.add(temp);
        count--;
    }
    return tmpResults.toArray(new FTPFile[tmpResults.size()]);

}
public FTPFile[]
    getPrevious(int quantityRequested)
{
    List<FTPFile> tmpResults = new LinkedList<FTPFile>();
    int count = quantityRequested;
    while(count > 0 && this._internalIterator.hasPrevious())
    {
        String entry = this._internalIterator.previous();
        FTPFile temp = this.parser.parseFTPEntry(entry);
        if(temp == null && saveUnparseableEntries)
        {
            temp = new FTPFile(entry);
        }
        tmpResults.add(0, temp);
        count--;
    }
    return tmpResults.toArray(new FTPFile[tmpResults.size()]);
}
public FTPFile[]
    getFiles()
    throws IOException
{
    return getFiles(FTPFileFilters.NON_NULL);
}
public FTPFile[]
    getFiles(FTPFileFilter filter)
    throws IOException
{
    List<FTPFile> tmpResults = new ArrayList<FTPFile>();
    Iterator<String> iter = this.entries.iterator();
    while(iter.hasNext())
    {
        String entry = iter.next();
        FTPFile temp = this.parser.parseFTPEntry(entry);
        if(temp == null && saveUnparseableEntries)
        {
            temp = new FTPFile(entry);
        }
        if(filter.accept(temp))
        {
            tmpResults.add(temp);
        }
    }
    return tmpResults.toArray(new FTPFile[tmpResults.size()]);

}
public boolean
    hasNext()
{
    return _internalIterator.hasNext();
}
public boolean
    hasPrevious()
{
    return _internalIterator.hasPrevious();
}
public void
    resetIterator()
{
    this._internalIterator = this.entries.listIterator();
}
}
