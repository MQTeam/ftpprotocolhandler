package com.atomiton.sff.imp.tftp;

import java.net.DatagramPacket;
import java.net.InetAddress;

public final class TFTPDataPacket
extends
    TFTPPacket
{
public static final int MAX_DATA_LENGTH = 512;
public static final int MIN_DATA_LENGTH = 0;
int _blockNumber;
int _length;
int _offset;
byte[] _data;
public TFTPDataPacket(InetAddress destination, int port, int blockNumber,
                      byte[] data, int offset, int length)
{
    super(TFTPPacket.DATA, destination, port);

    _blockNumber = blockNumber;
    _data = data;
    _offset = offset;

    if(length > MAX_DATA_LENGTH)
    {
        _length = MAX_DATA_LENGTH;
    }
    else
    {
        _length = length;
    }
}

public TFTPDataPacket(InetAddress destination, int port, int blockNumber,
                      byte[] data)
{
    this(destination, port, blockNumber, data, 0, data.length);
}
TFTPDataPacket(DatagramPacket datagram)
throws TFTPPacketException
{
    super(TFTPPacket.DATA, datagram.getAddress(), datagram.getPort());

    _data = datagram.getData();
    _offset = 4;

    if(getType() != _data[1])
    {
        throw new TFTPPacketException("TFTP operator code does not match type.");
    }

    _blockNumber = (((_data[2] & 0xff) << 8) | (_data[3] & 0xff));

    _length = datagram.getLength() - 4;

    if(_length > MAX_DATA_LENGTH)
    {
        _length = MAX_DATA_LENGTH;
    }
}
@Override
DatagramPacket _newDatagram(DatagramPacket datagram, byte[] data)
{
    data[0] = 0;
    data[1] = (byte)_type;
    data[2] = (byte)((_blockNumber & 0xffff) >> 8);
    data[3] = (byte)(_blockNumber & 0xff);

    if(data != _data)
    {
        System.arraycopy(_data, _offset, data, 4, _length);
    }

    datagram.setAddress(_address);
    datagram.setPort(_port);
    datagram.setData(data);
    datagram.setLength(_length + 4);

    return datagram;
}
@Override
public DatagramPacket
    newDatagram()
{
    byte[] data;

    data = new byte[_length + 4];
    data[0] = 0;
    data[1] = (byte)_type;
    data[2] = (byte)((_blockNumber & 0xffff) >> 8);
    data[3] = (byte)(_blockNumber & 0xff);

    System.arraycopy(_data, _offset, data, 4, _length);

    return new DatagramPacket(data, _length + 4, _address, _port);
}

public int
    getBlockNumber()
{
    return _blockNumber;
}
public void
    setBlockNumber(int blockNumber)
{
    _blockNumber = blockNumber;
}
public void
    setData(byte[] data, int offset, int length)
{
    _data = data;
    _offset = offset;
    _length = length;

    if(length > MAX_DATA_LENGTH)
    {
        _length = MAX_DATA_LENGTH;
    }
    else
    {
        _length = length;
    }
}
public int
    getDataLength()
{
    return _length;
}
public int
    getDataOffset()
{
    return _offset;
}
public byte[]
    getData()
{
    return _data;
}
@Override
public String
    toString()
{
    return super.toString() + " DATA " + _blockNumber + " " + _length;
}
}
