/*******************************************************************************
 * 
 * ATOMITON CONFIDENTIAL __________________
 * 
 * [2013] - [2016] ATOMITON Incorporated All Rights Reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of Atomiton Incorporated and its suppliers, if any. The intellectual and technical
 * concepts contained herein are proprietary to Atomiton Incorporated and its suppliers and may be covered by U.S. and Foreign Patents, patents in process, and
 * are protected by trade secret or copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from Atomiton Incorporated.
 */
package com.atomiton.sff.imp.netty.ftp;

import static com.atomiton.sff.api.nio.SffNioApi.val_global;
import static com.atomiton.sff.api.nio.SffNioApi.val_local;
import static com.atomiton.sff.api.nio.SffNioContextApi.Constants.AddLast_arg;
import static com.atomiton.sff.api.nio.SffNioContextApi.Constants.ExecutionHandler_obj;
import static com.atomiton.sff.api.nio.SffNioContextApi.Constants.Hl7Codec_obj;
import static com.atomiton.sff.api.nio.SffNioContextApi.Constants.SffMessageValidator_ob;
import static com.atomiton.sff.api.nio.SffNioContextApi.Constants.ThisHandler_obj;
import static com.atomiton.sff.api.nio.SffNioFrameworkSvc.sffClientPipelineArgs;
import static com.atomiton.sff.api.nio.SffNioFrameworkSvc.sffObjectName;
import static com.atomiton.sff.api.nio.SffNioFrameworkSvc.sffObjectNameMap;
import static com.atomiton.sff.api.nio.SffNioFrameworkSvc.sffProtocolClass;
import static com.atomiton.sff.api.nio.SffNioFrameworkSvc.sffProtocolName;
import static com.atomiton.sff.api.nio.SffNioFrameworkSvc.sffProtocolScope;
import static com.atomiton.sff.api.nio.SffNioFrameworkSvc.sffProtocolTransport;
import static com.atomiton.sff.api.nio.SffNioFrameworkSvc.sffServerPipelineArgs;
import static com.atomiton.sff.imp.base.SffUtils.newStruct;

import org.osgi.service.component.annotations.Component;

import com.atomiton.sff.api.SffTransportApi.Transport;
import com.atomiton.sff.api.nio.SffObjectFactorySvc;

import oda.lm.ListMap;

/*******************************************************************************
 * OSGi Ftp factory class.<br>
 * This factory registers and creates instances of FtpHandler.
 * @author Venkata Pokkuluri
 */
@Component(service = SffObjectFactorySvc.class)
public class SffFtpFactory
implements
    SffObjectFactorySvc<SffFtpHandler>
{
// ******************************************************************************
public static final String FTP = "ftp";                                      // protocol name
public static final String FTP_obj = SffFtpHandler.class.getSimpleName();  // object name
public static final Object[] FtpServerExtensionArgs = new Object[]
{
 AddLast_arg, FTP_obj,
 AddLast_arg, ExecutionHandler_obj,
 AddLast_arg, SffMessageValidator_ob,
 AddLast_arg, ThisHandler_obj,
};
public static final Object[] FtpClientExtensionArgs = FtpServerExtensionArgs; // same as server
// ------------------------------------------------------------------------------
public static final Object[] FtpHl7ServerExtensionArgs = new Object[]
{
 AddLast_arg, FTP_obj,
 AddLast_arg, ExecutionHandler_obj,
 AddLast_arg, SffMessageValidator_ob,
 AddLast_arg, Hl7Codec_obj,
 AddLast_arg, ThisHandler_obj,
};
public static final Object[] FtpHl7ClientExtensionArgs = FtpHl7ServerExtensionArgs;// same as server
protected static final ListMap codecInfo;
static
{
    ListMap info = ListMap.newInstance();
    info.put(sffObjectName, FTP_obj);
    info.put(sffProtocolName, FTP);
    info.put(sffProtocolClass, val_local);                                      // network protocol
    info.put(sffProtocolScope, val_global);                                     // pipelines should be shared globally
    info.put(sffProtocolTransport, Transport.TCP);
    info.put(sffServerPipelineArgs, FtpServerExtensionArgs);
    info.put(sffClientPipelineArgs, FtpClientExtensionArgs);
    info.put(sffObjectNameMap,
             newStruct("FtpServerExtensionArgs", FtpServerExtensionArgs,
                       "FtpClientExtensionArgs", FtpClientExtensionArgs,
                       "FtpHl7ServerExtensionArgs", FtpHl7ServerExtensionArgs,
                       "FtpHl7ClientExtensionArgs", FtpHl7ClientExtensionArgs));
    info.deepSetReadOnly(true);
    codecInfo = info;
}
// ------------------------------------------------------------------------------
@Override
public ListMap
    getInfo()
{
    return codecInfo;
}
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@Override
public SffFtpHandler
    newInstance(ListMap args)
    throws Exception
{
    return new SffFtpHandler(args);
}

}
