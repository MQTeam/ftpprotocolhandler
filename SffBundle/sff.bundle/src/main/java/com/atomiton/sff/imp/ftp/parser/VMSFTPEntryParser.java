package com.atomiton.sff.imp.ftp.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.StringTokenizer;

import com.atomiton.sff.imp.ftp.FTPClientConfig;
import com.atomiton.sff.imp.ftp.FTPFile;

public class VMSFTPEntryParser
extends
    ConfigurableFTPFileEntryParserImpl
{

private static final String DEFAULT_DATE_FORMAT = "d-MMM-yyyy HH:mm:ss";

private static final String REGEX = "(.*?;[0-9]+)\\s*"
                                    + "(\\d+)/\\d+\\s*"
                                    + "(\\S+)\\s+(\\S+)\\s+"
                                    + "\\[(([0-9$A-Za-z_]+)|([0-9$A-Za-z_]+),([0-9$a-zA-Z_]+))\\]?\\s*"
                                    + "\\([a-zA-Z]*,([a-zA-Z]*),([a-zA-Z]*),([a-zA-Z]*)\\)";
public VMSFTPEntryParser()
{
    this(null);
}
public VMSFTPEntryParser(FTPClientConfig config)
{
    super(REGEX);
    configure(config);
}
@Override
public FTPFile
    parseFTPEntry(String entry)
{
    long longBlock = 512;

    if(matches(entry))
    {
        FTPFile f = new FTPFile();
        f.setRawListing(entry);
        String name = group(1);
        String size = group(2);
        String datestr = group(3) + " " + group(4);
        String owner = group(5);
        String permissions[] = new String[3];
        permissions[0] = group(9);
        permissions[1] = group(10);
        permissions[2] = group(11);
        try
        {
            f.setTimestamp(super.parseTimestamp(datestr));
        }
        catch(ParseException e)
        {
        }

        String grp;
        String user;
        StringTokenizer t = new StringTokenizer(owner, ",");
        switch(t.countTokens())
        {
        case 1:
            grp = null;
            user = t.nextToken();
            break;
        case 2:
            grp = t.nextToken();
            user = t.nextToken();
            break;
        default:
            grp = null;
            user = null;
        }

        if(name.lastIndexOf(".DIR") != -1)
        {
            f.setType(FTPFile.DIRECTORY_TYPE);
        }
        else
        {
            f.setType(FTPFile.FILE_TYPE);
        }
        if(isVersioning())
        {
            f.setName(name);
        }
        else
        {
            name = name.substring(0, name.lastIndexOf(";"));
            f.setName(name);
        }
        long sizeInBytes = Long.parseLong(size) * longBlock;
        f.setSize(sizeInBytes);

        f.setGroup(grp);
        f.setUser(user);
        for(int access = 0; access < 3; access++)
        {
            String permission = permissions[access];

            f.setPermission(access, FTPFile.READ_PERMISSION, permission.indexOf('R') >= 0);
            f.setPermission(access, FTPFile.WRITE_PERMISSION, permission.indexOf('W') >= 0);
            f.setPermission(access, FTPFile.EXECUTE_PERMISSION, permission.indexOf('E') >= 0);
        }

        return f;
    }
    return null;
}
@Override
public String
    readNextEntry(BufferedReader reader)
    throws IOException
{
    String line = reader.readLine();
    StringBuilder entry = new StringBuilder();
    while(line != null)
    {
        if(line.startsWith("Directory") || line.startsWith("Total"))
        {
            line = reader.readLine();
            continue;
        }

        entry.append(line);
        if(line.trim().endsWith(")"))
        {
            break;
        }
        line = reader.readLine();
    }
    return(entry.length() == 0 ? null : entry.toString());
}

protected boolean
    isVersioning()
{
    return false;
}
@Override
protected FTPClientConfig
    getDefaultConfiguration()
{
    return new FTPClientConfig(
                               FTPClientConfig.SYST_VMS,
                               DEFAULT_DATE_FORMAT,
                               null);
}
}
