package com.atomiton.sff.imp.ftp;

public interface FTPFileFilter
{
public boolean
    accept(FTPFile file);
}
