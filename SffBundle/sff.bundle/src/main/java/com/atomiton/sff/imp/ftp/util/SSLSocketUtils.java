package com.atomiton.sff.imp.ftp.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.net.ssl.SSLSocket;

public class SSLSocketUtils
{
private SSLSocketUtils()
{
}
public static boolean
    enableEndpointNameVerification(SSLSocket socket)
{
    try
    {
        Class<?> cls = Class.forName("javax.net.ssl.SSLParameters");
        Method setEndpointIdentificationAlgorithm = cls.getDeclaredMethod("setEndpointIdentificationAlgorithm", String.class);
        Method getSSLParameters = SSLSocket.class.getDeclaredMethod("getSSLParameters");
        Method setSSLParameters = SSLSocket.class.getDeclaredMethod("setSSLParameters", cls);
        if(setEndpointIdentificationAlgorithm != null && getSSLParameters != null && setSSLParameters != null)
        {
            Object sslParams = getSSLParameters.invoke(socket);
            if(sslParams != null)
            {
                setEndpointIdentificationAlgorithm.invoke(sslParams, "HTTPS");
                setSSLParameters.invoke(socket, sslParams);
                return true;
            }
        }
    }
    catch(SecurityException e)
    {
    }
    catch(ClassNotFoundException e)
    {
    }
    catch(NoSuchMethodException e)
    {
    }
    catch(IllegalArgumentException e)
    {
    }
    catch(IllegalAccessException e)
    {
    }
    catch(InvocationTargetException e)
    {
    }
    return false;
}
}
