package com.atomiton.sff.imp.ftp;

import java.io.IOException;

public class FTPConnectionClosedException
extends
    IOException
{

private static final long serialVersionUID = 3500547241659379952L;
public FTPConnectionClosedException()
{
    super();
}
public FTPConnectionClosedException(String message)
{
    super(message);
}

}
