package com.atomiton.sff.imp.commons;

import java.io.PrintStream;
import java.io.PrintWriter;

public class PrintCommandListener
implements
    ProtocolCommandListener
{
private final PrintWriter __writer;
private final boolean __nologin;
private final char __eolMarker;
private final boolean __directionMarker;
public PrintCommandListener(PrintStream stream)
{
    this(new PrintWriter(stream));
}
public PrintCommandListener(PrintStream stream, boolean suppressLogin)
{
    this(new PrintWriter(stream), suppressLogin);
}
public PrintCommandListener(PrintStream stream, boolean suppressLogin, char eolMarker)
{
    this(new PrintWriter(stream), suppressLogin, eolMarker);
}
public PrintCommandListener(PrintStream stream, boolean suppressLogin, char eolMarker, boolean showDirection)
{
    this(new PrintWriter(stream), suppressLogin, eolMarker, showDirection);
}
public PrintCommandListener(PrintWriter writer)
{
    this(writer, false);
}
public PrintCommandListener(PrintWriter writer, boolean suppressLogin)
{
    this(writer, suppressLogin, (char)0);
}
public PrintCommandListener(PrintWriter writer, boolean suppressLogin, char eolMarker)
{
    this(writer, suppressLogin, eolMarker, false);
}
public PrintCommandListener(PrintWriter writer, boolean suppressLogin, char eolMarker, boolean showDirection)
{
    __writer = writer;
    __nologin = suppressLogin;
    __eolMarker = eolMarker;
    __directionMarker = showDirection;
}
@Override
public void
    protocolCommandSent(ProtocolCommandEvent event)
{
    if(__directionMarker)
    {
        __writer.print("> ");
    }
    if(__nologin)
    {
        String cmd = event.getCommand();
        if("PASS".equalsIgnoreCase(cmd) || "USER".equalsIgnoreCase(cmd))
        {
            __writer.print(cmd);
            __writer.println(" *******");
        }
        else
        {
            final String IMAP_LOGIN = "LOGIN";
            if(IMAP_LOGIN.equalsIgnoreCase(cmd))
            {
                String msg = event.getMessage();
                msg = msg.substring(0, msg.indexOf(IMAP_LOGIN) + IMAP_LOGIN.length());
                __writer.print(msg);
                __writer.println(" *******");
            }
            else
            {
                __writer.print(getPrintableString(event.getMessage()));
            }
        }
    }
    else
    {
        __writer.print(getPrintableString(event.getMessage()));
    }
    __writer.flush();
}

private String
    getPrintableString(String msg)
{
    if(__eolMarker == 0)
    {
        return msg;
    }
    int pos = msg.indexOf(SocketClient.NETASCII_EOL);
    if(pos > 0)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(msg.substring(0, pos));
        sb.append(__eolMarker);
        sb.append(msg.substring(pos));
        return sb.toString();
    }
    return msg;
}
@Override
public void
    protocolReplyReceived(ProtocolCommandEvent event)
{
    if(__directionMarker)
    {
        __writer.print("< ");
    }
    __writer.print(event.getMessage());
    __writer.flush();
}
}
