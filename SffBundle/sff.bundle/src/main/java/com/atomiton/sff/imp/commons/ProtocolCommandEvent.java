package com.atomiton.sff.imp.commons;

import java.util.EventObject;

public class ProtocolCommandEvent
extends
    EventObject
{
private static final long serialVersionUID = 403743538418947240L;
private final int __replyCode;
private final boolean __isCommand;
private final String __message, __command;

public ProtocolCommandEvent(Object source, String command, String message)
{
    super(source);
    __replyCode = 0;
    __message = message;
    __isCommand = true;
    __command = command;
}
public ProtocolCommandEvent(Object source, int replyCode, String message)
{
    super(source);
    __replyCode = replyCode;
    __message = message;
    __isCommand = false;
    __command = null;
}
public String
    getCommand()
{
    return __command;
}
public int
    getReplyCode()
{
    return __replyCode;
}
public boolean
    isCommand()
{
    return __isCommand;
}
public boolean
    isReply()
{
    return !isCommand();
}
public String
    getMessage()
{
    return __message;
}
}
