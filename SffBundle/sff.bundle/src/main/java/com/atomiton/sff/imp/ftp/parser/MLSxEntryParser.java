package com.atomiton.sff.imp.ftp.parser;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.*;

import com.atomiton.sff.imp.ftp.FTPFile;
import com.atomiton.sff.imp.ftp.FTPFileEntryParserImpl;

public class MLSxEntryParser
extends
    FTPFileEntryParserImpl
{

private static final MLSxEntryParser PARSER = new MLSxEntryParser();

private static final HashMap<String, Integer> TYPE_TO_INT = new HashMap<String, Integer>();
static
{
    TYPE_TO_INT.put("file", Integer.valueOf(FTPFile.FILE_TYPE));
    TYPE_TO_INT.put("cdir", Integer.valueOf(FTPFile.DIRECTORY_TYPE));
    TYPE_TO_INT.put("pdir", Integer.valueOf(FTPFile.DIRECTORY_TYPE));
    TYPE_TO_INT.put("dir", Integer.valueOf(FTPFile.DIRECTORY_TYPE));
}

private static int UNIX_GROUPS[] =
{
 FTPFile.USER_ACCESS,
 FTPFile.GROUP_ACCESS,
 FTPFile.WORLD_ACCESS,
};

private static int UNIX_PERMS[][] =
{
 {},
 {FTPFile.EXECUTE_PERMISSION},
 {FTPFile.WRITE_PERMISSION},
 {FTPFile.EXECUTE_PERMISSION, FTPFile.WRITE_PERMISSION},
 {FTPFile.READ_PERMISSION},
 {FTPFile.READ_PERMISSION, FTPFile.EXECUTE_PERMISSION},
 {FTPFile.READ_PERMISSION, FTPFile.WRITE_PERMISSION},
 {FTPFile.READ_PERMISSION, FTPFile.WRITE_PERMISSION, FTPFile.EXECUTE_PERMISSION},
};

public MLSxEntryParser()
{
    super();
}

@Override
public FTPFile
    parseFTPEntry(String entry)
{
    if(entry.startsWith(" "))
    {
        if(entry.length() > 1)
        {
            FTPFile file = new FTPFile();
            file.setRawListing(entry);
            file.setName(entry.substring(1));
            return file;
        }
        else
        {
            return null;
        }

    }
    String parts[] = entry.split(" ", 2);
    if(parts.length != 2 || parts[1].length() == 0)
    {
        return null;
    }
    final String factList = parts[0];
    if(!factList.endsWith(";"))
    {
        return null;
    }
    FTPFile file = new FTPFile();
    file.setRawListing(entry);
    file.setName(parts[1]);
    String[] facts = factList.split(";");
    boolean hasUnixMode = parts[0].toLowerCase(Locale.ENGLISH).contains("unix.mode=");
    for(String fact : facts)
    {
        String[] factparts = fact.split("=", -1);
        if(factparts.length != 2)
        {
            return null;
        }
        String factname = factparts[0].toLowerCase(Locale.ENGLISH);
        String factvalue = factparts[1];
        if(factvalue.length() == 0)
        {
            continue;
        }
        String valueLowerCase = factvalue.toLowerCase(Locale.ENGLISH);
        if("size".equals(factname))
        {
            file.setSize(Long.parseLong(factvalue));
        }
        else if("sizd".equals(factname))
        {
            file.setSize(Long.parseLong(factvalue));
        }
        else if("modify".equals(factname))
        {
            final Calendar parsed = parseGMTdateTime(factvalue);
            if(parsed == null)
            {
                return null;
            }
            file.setTimestamp(parsed);
        }
        else if("type".equals(factname))
        {
            Integer intType = TYPE_TO_INT.get(valueLowerCase);
            if(intType == null)
            {
                file.setType(FTPFile.UNKNOWN_TYPE);
            }
            else
            {
                file.setType(intType.intValue());
            }
        }
        else if(factname.startsWith("unix."))
        {
            String unixfact = factname.substring("unix.".length()).toLowerCase(Locale.ENGLISH);
            if("group".equals(unixfact))
            {
                file.setGroup(factvalue);
            }
            else if("owner".equals(unixfact))
            {
                file.setUser(factvalue);
            }
            else if("mode".equals(unixfact))
            {
                int off = factvalue.length() - 3;
                for(int i = 0; i < 3; i++)
                {
                    int ch = factvalue.charAt(off + i) - '0';
                    if(ch >= 0 && ch <= 7)
                    {
                        for(int p : UNIX_PERMS[ch])
                        {
                            file.setPermission(UNIX_GROUPS[i], p, true);
                        }
                    }
                    else
                    {
                    }
                }
            }
        }
        else if(!hasUnixMode && "perm".equals(factname))
        {
            doUnixPerms(file, valueLowerCase);
        }
    }
    return file;
}
public static Calendar
    parseGMTdateTime(String timestamp)
{
    final SimpleDateFormat sdf;
    final boolean hasMillis;
    if(timestamp.contains("."))
    {
        sdf = new SimpleDateFormat("yyyyMMddHHmmss.SSS");
        hasMillis = true;
    }
    else
    {
        sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        hasMillis = false;
    }
    TimeZone GMT = TimeZone.getTimeZone("GMT");
    sdf.setTimeZone(GMT);
    GregorianCalendar gc = new GregorianCalendar(GMT);
    ParsePosition pos = new ParsePosition(0);
    sdf.setLenient(false);
    final Date parsed = sdf.parse(timestamp, pos);
    if(pos.getIndex() != timestamp.length())
    {
        return null;
    }
    gc.setTime(parsed);
    if(!hasMillis)
    {
        gc.clear(Calendar.MILLISECOND);
    }
    return gc;
}

private void
    doUnixPerms(FTPFile file, String valueLowerCase)
{
    for(char c : valueLowerCase.toCharArray())
    {
        switch(c)
        {
        case 'a':
            file.setPermission(FTPFile.USER_ACCESS, FTPFile.WRITE_PERMISSION, true);
            break;
        case 'c':
            file.setPermission(FTPFile.USER_ACCESS, FTPFile.WRITE_PERMISSION, true);
            break;
        case 'd':
            file.setPermission(FTPFile.USER_ACCESS, FTPFile.WRITE_PERMISSION, true);
            break;
        case 'e':
            file.setPermission(FTPFile.USER_ACCESS, FTPFile.READ_PERMISSION, true);
            break;
        case 'f':
            break;
        case 'l':
            file.setPermission(FTPFile.USER_ACCESS, FTPFile.EXECUTE_PERMISSION, true);
            break;
        case 'm':
            file.setPermission(FTPFile.USER_ACCESS, FTPFile.WRITE_PERMISSION, true);
            break;
        case 'p':
            file.setPermission(FTPFile.USER_ACCESS, FTPFile.WRITE_PERMISSION, true);
            break;
        case 'r':
            file.setPermission(FTPFile.USER_ACCESS, FTPFile.READ_PERMISSION, true);
            break;
        case 'w':
            file.setPermission(FTPFile.USER_ACCESS, FTPFile.WRITE_PERMISSION, true);
            break;
        default:
            break;

        }
    }
}

public static FTPFile
    parseEntry(String entry)
{
    return PARSER.parseFTPEntry(entry);
}

public static MLSxEntryParser
    getInstance()
{
    return PARSER;
}
}
