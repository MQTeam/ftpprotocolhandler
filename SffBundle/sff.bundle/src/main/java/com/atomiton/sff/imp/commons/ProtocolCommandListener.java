package com.atomiton.sff.imp.commons;

import java.util.EventListener;

public interface ProtocolCommandListener
extends
    EventListener
{
public void
    protocolCommandSent(ProtocolCommandEvent event);
public void
    protocolReplyReceived(ProtocolCommandEvent event);
}
